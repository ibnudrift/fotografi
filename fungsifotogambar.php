<?php
//================================================================================sumber gambar
function kotakkan($pathfoto, $ukuranfoto)
{	$info		= getimagesize($pathfoto);
	$lebar		= $info[0];
	$tinggi		= $info[1];
	$tipe		= $info["mime"];

	if($tipe == "image/jpeg")
	{	$gambar = imagecreatefromjpeg($pathfoto);
	}
	else if($tipe == "image/png")
	{	$gambar = imagecreatefrompng($pathfoto);
	}

	if($lebar > $tinggi) //==================jika gambar melebar
	{	$x1 = floor(($lebar - $tinggi) / 2);	$x2 = $tinggi;
		$y1 = 0;				$y2 = $tinggi;
	}
	else if($tinggi > $lebar) //=============jika gambar meninggi
	{	$x1 = 0;				$x2 = $lebar;
		$y1 = floor(($tinggi - $lebar) / 2);	$y2 = $lebar;
	}
	else //==================================jika gambar kotak
	{	$x1 = 0;				$x2 = $lebar;
		$y1 = 0;				$y2 = $tinggi;
	}



	//================================================================================hasil gambar
	if($ukuranfoto == "besar")
	{	$lebarhasil	= "300";
		$tinggihasil	= "300";
	}
	else if($ukuranfoto == "kecil")
	{	$lebarhasil	= "100";
		$tinggihasil	= "100";
	}
	$gambarhasil	= imagecreatetruecolor($lebarhasil, $tinggihasil);



	//================================================================================pembuatan gambar
	imagecopyresampled($gambarhasil, $gambar, 0, 0, $x1, $y1, $lebarhasil, $tinggihasil, $x2, $y2);
	var_dump(imagejpeg($gambarhasil, $pathfoto));
}



function kecilkan($pathfoto, $ukuranfoto)
{	$info		= getimagesize($pathfoto);
	$lebar		= $info[0];
	$tinggi		= $info[1];
	$tipe		= $info["mime"];

	if($tipe == "image/jpeg")
	{	$gambar = imagecreatefromjpeg($pathfoto);
	}
	else if($tipe == "image/png")
	{	$gambar = imagecreatefrompng($pathfoto);
	}



	$luashasil	= 307200;
	$lebarhasil	= round(sqrt($lebar * $luashasil / $tinggi));
	$tinggihasil	= round(sqrt($tinggi * $luashasil / $lebar));

	$gambarhasil	= imagecreatetruecolor($lebarhasil, $tinggihasil);

	//================================================================================pembuatan gambar
	imagecopyresampled($gambarhasil, $gambar, 0, 0, 0, 0, $lebarhasil, $tinggihasil, $lebar, $tinggi);
	var_dump(imagejpeg($gambarhasil, $pathfoto));
}
?>
