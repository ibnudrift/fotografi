<?php	//=======================cek apakah ada variabel post foto
	session_start();
	include("../liby/fungsikoneksi.php");
	include("../liby/fungsitransformasipin.php");
	include("../liby/fungsifotogambar.php");

	if(isset($_FILES["fotofigur"]))	//===============================jika ada variabel post, maka pakai foto baru itu
	{	if($_FILES["fotofigur"]["error"] == 0) //============================================jika upload berjalan dengan baik
		{	$ekstensifoto = ".".strtolower(end(explode(".", $_FILES["fotofigur"]["name"])));
			$ekstensidiperbolehkan = array(".jpg", ".jpe", ".jpeg", ".jfif", ".png");
			$tipediperbolehkan = array("image/jpeg", "image/png");

			if(in_array($ekstensifoto, $ekstensidiperbolehkan) && $_FILES["fotofigur"]["size"] > 0
				&& in_array($_FILES["fotofigur"]["type"], $tipediperbolehkan)) //=================================cek kebenaran foto
			{	$queryfoto = mysql_query
				("	SELECT pinfoto FROM foto
					ORDER BY pinfoto DESC LIMIT 0, 1
				");
				$jumlah = mysql_num_rows($queryfoto);
				if($jumlah != 0)
				{	$array = mysql_fetch_assoc($queryfoto);
					$pinfoto = $array["pinfoto"];
					$nomorfoto = semuadigit($pinfoto);
					$nomorfoto++;
					$pinfoto = tujuhdigit($nomorfoto);
				}
				else
				{	$pinfoto = "1000000";
				}

				$jenisfoto = "utamafigur";
				$direktorifotobesar = "figure/fotobesar/";				$direktorifotokecil = "figure/fotokecil/";
				$pathfotobesar = "../".$direktorifotobesar.$pinfoto.$ekstensifoto;	$pathfotokecil = "../".$direktorifotokecil.$pinfoto.$ekstensifoto;
				move_uploaded_file($_FILES["fotofigur"]["tmp_name"], $pathfotobesar);
				kotakkan($pathfotobesar, "besar");
				echo("<script type='text/javascript'>$('pre.xdebug-var-dump').remove();</script>");

				copy($pathfotobesar, $pathfotokecil);
				kotakkan($pathfotokecil, "kecil");
				echo("<script type='text/javascript'>$('pre.xdebug-var-dump').remove();</script>");

				$pinfigur = $_SESSION["pinfigur"];

				//============================ubah status foto sebelumnya (jika ada), kemudian memasukkan foto baru dengan status aktif
				$queryfoto = mysql_query
				("	SELECT pinfoto FROM foto
					WHERE pinpembuatfoto = '$pinfigur' && statusfoto = 'aktif'
					ORDER BY pinfoto DESC LIMIT 0, 1
				");
				$jumlahhasil = mysql_num_rows($queryfoto);
				if($jumlahhasil == 1)
				{	$array = mysql_fetch_assoc($queryfoto);
					$pinfotolama = $array["pinfoto"];
					$queryfoto = mysql_query
					("	UPDATE foto SET statusfoto = 'deaktif'
						WHERE pinfoto = '$pinfotolama'
					");
				}
				$queryfoto = mysql_query
				("	INSERT INTO foto VALUES
					('$pinfoto', '$jenisfoto', 'aktif', '$pinfigur', CURRENT_TIMESTAMP, NULL, '$direktorifotobesar', '$ekstensifoto')
				");

				//===============================ubah sesi foto menjadi foto yang baru saja diupload
				$_SESSION["fotofigur"] = $pinfoto.$ekstensifoto;

				echo("sukses||".$_SESSION["fotofigur"]);
			}
			else
			{	echo("gagal||Foto yang Anda unggah tidak benar. Coba ulangi lagi.");
			}
		}
		else  //==========================================jika upload berjalan dengan tidak baik
		{	echo("gagal||Unggah tidak berjalan dengan baik. Coba ulangi lagi.");
		}
	}
	else				//================================jika tidak ada variable post, maka pakai foto yang sebelumnya dipakai oleh pengguna
	{	echo("gagal||Unggah tidak berjalan dengan baik. Coba ulangi lagi.");
	}
?>
