<?php

class EducationController extends Controller
{

	public function actionIndex()
	{
		$this->pageTitle = '3D Surgery Animation - Education - '.$this->pageTitle;

		$this->render('index', array(
		));
	}

	public function actionSub()
	{
		if (! isset($_GET['name'])) {
			$this->redirect(array('index'));
		}

		$strTitle = '';
		$names = $_GET['name'];
		if ($names == 'face') {
			$strTitle = 'Face';
		}elseif ($names == 'breast') {
			$strTitle = 'Breast';
		}else{
			$strTitle = 'Body';
		}

		$this->pageTitle = $strTitle.' Surgery Animation - Education - '.$this->pageTitle;

		$this->render('subpage', array(
			'names' => $names,
		));
	}

	public function actionFaq()
	{
		$this->pageTitle = 'Faq - Education - '.$this->pageTitle;

		$criteria = new CDbCriteria;
		$criteria->with = array('description');

		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;

		if ($_GET['topic'] != '') {
			$criteria->addCondition('topic = :topic');
			$criteria->params[':topic'] = $_GET['topic'];
		}

		$faq = new CActiveDataProvider('Faq', array(
			'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>10,
		    ),
		));

		$this->render('faq', array(
			'faq'=>$faq,
		));
	}
}

