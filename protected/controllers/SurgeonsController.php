<?php

class SurgeonsController extends Controller
{

	public function actionIndex()
	{
		$criteria = new CDbCriteria;
		$criteria->with = array('clinics', 'specialication', 'clinic');

		$criteria->addCondition('t.status = 1');
		// Mengatur Order
		if ($_GET['order'] == 'alphabetic') {
			$criteria->order = 't.name ASC';
		} elseif($_GET['order'] == 'most_review') {
			$criteria->order = 'n_review DESC';
		} elseif($_GET['order'] == 'date_join') {
			$criteria->order = 'date_input DESC';
		} else {
			$criteria->order = 't.name ASC';
		}
		$criteria->group = 't.id';

		if ($_GET['name'] != '') {
			$criteria->addCondition('t.name LIKE :name');
			$criteria->params[':name'] = '%'.$_GET['name'].'%';
		}

		if ($_GET['specialisation'] != '') {
			$criteria->addInCondition('specialication.specialitation_id', array($_GET['specialisation']));
			// $criteria->params[':specialisation'] = $_GET['specialisation'];
		}

		// SELECT `id`, `name`, `phone`, `latitude`, `longitude`,
		//    111.111 *
		//     DEGREES(ACOS(COS(RADIANS(`latitude`))
		//          * COS(RADIANS(-33.88114166))
		//          * COS(RADIANS(`longitude` - 151.21963501))
		//          + SIN(RADIANS(`latitude`))
		//          * SIN(RADIANS(-33.88114166)))) AS distance_in_km
		//   FROM doctor_clinic HAVING distance_in_km <= 10
		// ORDER BY distance_in_km ASC
		if ($_GET['suburb'] != '') {

			$criteria->addCondition('clinic.suburb LIKE :suburb');
			$criteria->params[':suburb'] = '%'.$_GET['suburb'].'%';
		}

		$dataDoctor = new CActiveDataProvider('Doctor', array(
			'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>6,
		    ),
		));
		// echo(count($dataDoctor->getData()));
		// exit;

		$this->pageTitle = 'Surgeons - ' . $this->pageTitle;
		$this->render('index', array(
			'dataDoctor'=>$dataDoctor,
		));
	}

	public function actionlist()
	{

		$criteria = new CDbCriteria;
		$criteria->with = array('specialication', 'clinics', 'clinic');

		$criteria->addCondition('t.status = 1');

		// Mengatur Order
		if ($_GET['order'] == 'alphabetic') {
			$criteria->order = 't.name ASC';
		} elseif($_GET['order'] == 'most_review') {
			$criteria->order = 'n_review DESC';
		} elseif($_GET['order'] == 'date_join') {
			$criteria->order = 'date_input DESC';
		} else {
			$criteria->order = 't.name ASC';
		}
		$criteria->group = 't.id';

		if ($_GET['name'] != '') {
			$criteria->addCondition('t.name LIKE :name');
			$criteria->params[':name'] = '%'.$_GET['name'].'%';
		}
		if ($_GET['q'] != '') {
			$criteria->addCondition('t.name LIKE :name');
			$criteria->params[':name'] = '%'.$_GET['q'].'%';
		}

		if ($_GET['specialisation'] != '') {
			$dataCategory = ViewCategory::model()->findAll('language_id = :language_id AND type = :type AND name LIKE :name', array(':language_id'=>$this->languageID, ':type'=>'spesialis', ':name'=>'%'.$_GET['specialisation'].'%'));
			$dataArray = array();
			foreach ($dataCategory as $key => $value) {
				array_push($dataArray, $value->id);
				array_push($dataArray, $value->parent_id);
			}
			// print_r($dataArray);
			// exit;
			$criteria->addInCondition('specialication.specialitation_id', $dataArray);
		}
		if ($_GET['suburb'] != '') {
			$api = new PlaceGoogle;
			if (($location = $api->getLatLong($_GET['suburb'])) != null) {
				$criteria->select = $criteria->select.', 111.111 *
			    DEGREES(ACOS(COS(RADIANS(`clinic`.`latitude`))
			         * COS(RADIANS('.$location['lat'].'))
			         * COS(RADIANS(`clinic`.`longitude` - '.$location['lng'].'))
			         + SIN(RADIANS(`clinic`.`latitude`))
			         * SIN(RADIANS('.$location['lat'].')))) AS distance_in_km';
				$criteria->having = 'distance_in_km <= 5';
			    $criteria->order = 'distance_in_km ASC';
			}
			// $criteria->addCondition('clinic.suburb LIKE :suburb');
			// $criteria->params[':suburb'] = '%'.$_GET['suburb'].'%';
		}

		$dataDoctor = new CActiveDataProvider('Doctor', array(
			'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>10,
		    ),
		));

		$this->pageTitle = 'Surgeons - ' . $this->pageTitle;
		$this->render('list', array(
			'dataDoctor'=>$dataDoctor,
		));
	}

	public function actionDetail($id)
	{
		$dataDoctor = Doctor::model()->with(array('clinics', 'spec', 'videos'))->findByPk($id);

		if($dataDoctor===null)
			throw new CHttpException(404,'The requested page does not exist.');

		$modelReview = new DoctorReview;
		
		$modelMessage = new DoctorMessage;

		if(isset($_POST['DoctorReview']))
		{
			$modelReview->attributes = $_POST['DoctorReview'];

			$session = new CHttpSession;
			$session->open();

			$modelReview->doctor_id = $dataDoctor->id;
			$modelReview->member_id = $session['login_member']['id'];
			$modelReview->name = $session['login_member']['first_name'];
			$modelReview->address = $session['login_member']['billing_suburb'];
			$modelReview->date_input = date('Y-m-d H:i:s');

			if ($modelReview->validate()) {
				$modelReview->save();
				foreach ($_POST['Rating'] as $key => $value) {
					$modelRating = new DoctorCategoryReview;
					$modelRating->doctor_id = $dataDoctor->id;
					$modelRating->review_category_id = $key;
					$modelRating->review_id = $modelReview->id;
					$modelRating->value = $value;
					$modelRating->save();
				}
				Yii::app()->user->setFlash('success','You have successfully posted your review. Your review is pending now and it will be displayed after we verify.');
				$this->redirect(array('/memberreview/index'));
			}
		}
		if(isset($_POST['DoctorMessage']))
		{
			$modelMessage->attributes = $_POST['DoctorMessage'];
			if ($modelMessage->validate()) {
				$session = new CHttpSession;
				$session->open();
				$modelMessage->doctor_id = $dataDoctor->id;
				$modelMessage->date_input = date('Y-m-d H:i:s');
				if ( ! is_null($session['login_member'])) {
					$modelMessage->member_id = $session['login_member']['id'];
				}
				$modelMessage->save();
				Yii::app()->user->setFlash('success2','You have successfully posted your message. We will reply your message as soon as possible.');
				$this->refresh();
			}
		}
		$this->pageTitle = 'Surgeons Detail - ' . $this->pageTitle;

		$session = new CHttpSession;
		$session->open();
		if ( ! is_null($session['login_member'])) {
			$modelMessage->first_name = $session['login_member']['first_name'];
			$modelMessage->last_name = $session['login_member']['last_name'];
			$modelMessage->email = $session['login_member']['email'];
			$modelMessage->telp = $session['login_member']['phone'];
		}

		$modelLogin = new MeMember;
		$modelLogin->scenario = 'loginMember';

		$this->render('detail', array(
			'dataDoctor'=>$dataDoctor,
			'modelReview'=>$modelReview,
			'modelMessage'=>$modelMessage,
			'modelLogin'=>$modelLogin,
		));
	}
	public function actionClaim($id)
	{
		$session = new CHttpSession;
		$session->open();

		if (! isset($session['login_member'])){
			$this->redirect(array('/member/index', 'ret'=>urlencode( CHtml::normalizeUrl(array('claim', 'id'=>$id)) )));
		}

		$dataDoctor = Doctor::model()->with(array('clinics', 'spec', 'videos'))->findByPk($id);

		if($dataDoctor===null)
			throw new CHttpException(404,'The requested page does not exist.');

		$dataClaim = DoctorClaim::model()->find('member_id = :member_id', array(':member_id'=>$session['login_member']['id']));
		if($dataClaim!==null){
			Yii::app()->user->setFlash('danger2','You have already claimed this profile');
			$this->redirect(array('/surgeons/detail', 'id'=>$id));
		}

		$modelClaim = new DoctorClaim;
		$modelClaim->doctor_id = $dataDoctor->id;
		$modelClaim->member_id = $session['login_member']['id'];
		$modelClaim->date_input = date('Y-m-d H:i:s');
		$modelClaim->status = 0;
		$modelClaim->save(false);

		$messaged = $this->renderPartial('//mail/claimed',array(
			'model'=>$session['login_member'],
			'dataDoctor'=>$dataDoctor,
		),TRUE);

		$config = array(
			'to'=>array($this->setting['email']),
			'subject'=>'Hi, '.$session['login_member']['email'].' claimed Trusted Surgeons Profile',
			'message'=>$messaged,
		);
		// print_r($config); exit;
		// kirim email
		Common::mail($config);

		Yii::app()->user->setFlash('success','You\'ve claimed '.$dataDoctor->name.'\'s profile. Your request will be confirmed as soon as possible.');
		$this->redirect(array('/member/index'));
	}

}