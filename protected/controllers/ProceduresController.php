<?php

class ProceduresController extends Controller
{

	// public function actionIndex()
	// {
	// 	$this->render('index', array(
	// 		// 'procedure'=>$procedure,
	// 	));
	// }

	public function actionDetail($id)
	{
		$criteria = new CDbCriteria;
		$criteria->with = array('description');

		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;

		$criteria->addCondition('t.id = :id');
		$criteria->params[':id'] = $id;

		$data = Listprocedure::model()->find($criteria);

		$this->render('detail', array(
			'data'=>$data,
			// 'procedure'=>$procedure,
		));
	}

	public function actionIndex()
	{
		// $id = 0;
		// if ($_GET['name'] != '') {
		// 	if ($_GET['name']== 'face') {
		// 		$id = 3;
		// 	}elseif ($_GET['name'] == 'body') {
		// 		$id = 2;
		// 	}elseif ($_GET['name'] == 'breast') {
		// 		$id = 1;
		// 	} else {
		// 		$id = 0;
		// 	}
		// }

		$this->pageTitle = 'Procedures - ' . $this->pageTitle;

		// $procedure = array();
		// if ($id != 0 AND $id != '') {

			$this->render('index', array(
				'procedure'=>$procedure,
			));
		// }else{
		// 	$this->render('landing', array(
		// 	));
		// }
		// End Procedure
	}
}