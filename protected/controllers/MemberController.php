<?php

class MemberController extends Controller
{

	public function actionIndex()
	{
		$session = new CHttpSession;
		$session->open();
		if (isset($session['login_member'])) { // sudah login
			$model = MeMember::model()->findByPk($session['login_member']['id']);
			if ($model == null){
				unset($session['login_member']);
				$this->refresh();
			}
				

			$model2 = MeMember::model()->findByPk($session['login_member']['id']);
			$model->scenario = 'editMember';
			$model2->scenario = 'updatePass';

			if(isset($_POST['MeMember']))
			{
				$model->attributes = $_POST['MeMember'];

				if ($model->validate()) {
					$model->save();
					Yii::app()->user->setFlash('success','Edit data success');
					$this->redirect(array('index'));
				}
			}

			if(isset($_POST['MeMember2']))
			{
				$model2->attributes = $_POST['MeMember2'];

				$model2->pass = sha1($model2->pass);
				$model2->pass2 = sha1($model2->pass2);

				if(sha1($model2->passold) != $model->pass)
					$model2->addError('passold','Incorrect password.');

				if ((!$model2->hasErrors()) && $model2->validate()) {
					$model2->save();
					Yii::app()->user->setFlash('success','Change password success');
					$this->redirect(array('index'));
				}
			}

			$model2->pass = '';
			$model2->pass2 = '';
			$model2->passold = '';
			$this->render('index2', array(
				'model'=>$model,
				'model2'=>$model2,
			));	
		}else{ // belum login

			// if(isset($_POST['LoginForm2']))
			// {
			// 	$modelLogin->attributes=$_POST['LoginForm2'];
			// 	// validate user input and redirect to the previous page if valid
			// 	$data = MeMember::model()->find('no_member = :no_member', array(':no_member'=>$modelLogin->username));
			// 	if($data->pass != sha1($modelLogin->password))
			// 		$modelLogin->addError('password','Incorrect No Member or password.');

			// 	if ((!$modelLogin->hasErrors()) && $modelLogin->validate()) {
			// 		$session['login_member'] = $data->attributes;
			// 	    if ($_GET['ret']) {
			// 			$this->redirect(urldecode($_GET['ret']));
			// 	    }else{
			// 			$this->redirect(array('index'));
			// 	    }
			// 	}
			// }

			$model = new MeMember;
			$model->scenario = 'loginMember';

			$model2 = new MeMember;
			$model2->scenario = 'loginMember';
			
			if(isset($_POST['MeMember']))
			{
				$model->attributes = $_POST['MeMember'];

				$data = MeMember::model()->find('email = :email AND type = 0', array(':email'=>$model->email));
				if($data->pass != sha1($model->pass))
					$model->addError('pass','Incorrect Email or password.');

				if ((!$model->hasErrors()) && $model->validate()) {
					$session['login_member'] = $data->attributes;
					$data->login_terakhir = date('Y-m-d H:i:s');
					$data->save(false);
					Yii::app()->user->setFlash('success','login successful.');
				    if ($_GET['ret']) {
						$this->redirect(urldecode($_GET['ret']));
				    }else{
						$this->redirect(array('index'));
				    }

				}
			}
			if(isset($_POST['MeMember2']))
			{
				$model2->attributes = $_POST['MeMember2'];

				$data = MeMember::model()->find('email = :email AND type = 1', array(':email'=>$model2->email));
				if($data->pass != sha1($model2->pass))
					$model2->addError('pass','Incorrect Email or password.');

				if ((!$model2->hasErrors()) && $model2->validate()) {
					$session['login_member'] = $data->attributes;
					$data->login_terakhir = date('Y-m-d H:i:s');
					$data->save(false);
				    if ($_GET['ret']) {
						$this->redirect(urldecode($_GET['ret']));
				    }else{
						$this->redirect(array('index'));
				    }
				}
			}

			// $this->pageTitle = 'Login & Register - '.$this->pageTitle;
			// $this->layout='//layouts/home';
			$this->render('index', array(
				'model'=>$model,
				'model2'=>$model2,
				// 'modelDelivery'=>$modelDelivery,
			));	
		}
	}
	public function actionSignup()
	{
		$session = new CHttpSession;
		$session->open();

		$model = new MeMember;
		$model->scenario = 'createMember';

		$model2 = new MeMember;
		$model2->scenario = 'createMember';
		
		if(isset($_POST['MeMember']))
		{
			$model->attributes = $_POST['MeMember'];

			if ($model->validate()) {
				$model->pass = sha1($model->pass);
				$model->type = 0;
				$model->save();
				$session['login_member'] = $model->attributes;
				
				Yii::app()->user->setFlash('success_register','You have successfully sign up.');
			    if ($_GET['ret']) {
					$this->redirect(urldecode($_GET['ret']));
			    }else{
					$this->redirect(array('index'));
			    }

			}
		}
		if(isset($_POST['MeMember2']))
		{
			$model2->attributes = $_POST['MeMember2'];

			if ($model2->validate()) {
				$model2->pass = sha1($model2->pass);
				$model2->type = 1;
				$model2->save();
				$session['login_member'] = $model2->attributes;
			    if ($_GET['ret']) {
					$this->redirect(urldecode($_GET['ret']));
			    }else{
					$this->redirect(array('index'));
			    }
			}
		}
		$this->render('signup', array(
			'model'=>$model,
			'model2'=>$model2,
		));	
	}
	public function actionEdit()
	{
		$session = new CHttpSession;
		$session->open();
		if ( ! isset($session['login_member']))
			$this->redirect(array('index'));

		$model = MeMember::model()->findByPk($session['login_member']['id']);
		if(isset($_POST['MeMember'])) {
			$pass = $model->pass;
			$model->attributes = $_POST['MeMember'];
			if ($_POST['MeMember']['passold'] != '') {
				$model->scenario = 'updatePass';
				$model->pass = sha1($model->pass);
				$model->pass2 = sha1($model->pass2);
			}else{
				$model->scenario = 'update';
				$model->pass = $pass;
			}
			if ($model->validate()) {
				if ($_POST['MeMember']['passold'] != '') {
					if (sha1($model->passold) != $pass) {
						$model->addError('passold','Password lama tidak valid');
					}
				}
				if(!$model->hasErrors())
				{
					$model->save();
					$this->redirect(array('index'));
				}
			}
		}

		$model->pass = '';
		$model->pass2 = '';
		$model->passold = '';

		$this->render('edit', array(
			'model'=>$model,
		));	

	}

	public function actionAddpemeriksaan($id)
	{
		$session = new CHttpSession;
		$session->open();
		if ( ! isset($session['login_member']))
			$this->redirect(array('index'));

		$model = new Pemeriksaan;
		if(isset($_POST['Pemeriksaan'])) {
			$model->attributes = $_POST['Pemeriksaan'];

			$pdf = CUploadedFile::getInstance($model,'pdf');
			$model->pdf = substr(md5(time()),0,5).'-'.$pdf->name;

			if ($model->validate()) {
				$model->member_id = $id;
				$pdf->saveAs(Yii::getPathOfAlias('webroot').'/images/pdf/'.$model->pdf);
				$model->save();
				$this->refresh();
			}
		}
		$model->date = date('Y-m-d');

		$criteria=new CDbCriteria;

		$criteria->order = 'date DESC';
		if ($_GET['jenis'] != '') {
			$criteria->addCondition('t.jenis = :jenis');
			$criteria->params[':jenis'] = $_GET['jenis'];
		}

		if ($_GET['pagesize'] != '') {
			$pageSize = $_GET['pagesize'];
		} else {
			$pageSize = 15;
		}
		$list = new CActiveDataProvider('Pemeriksaan', array(
			'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>$pageSize,
		    ),
		));

		$this->render('addpemeriksaan', array(
			'model'=>$model,
			'list'=>$list,
		));	

	}

	public function actionLogout()
	{
		$session = new CHttpSession;
		$session->open();
		unset($session['login_member']);
		$this->redirect(array('index'));
	}

	public function actionOrder()
	{
		$session = new CHttpSession;
		$session->open();

		if (empty($session['login_member']))
			$this->redirect(array('index', 'ret'=>urlencode(CHtml::normalizeUrl(array('/member/order')))));

		$criteria=new CDbCriteria;

		// echo $session['login_member']['email'];
		// exit;

		$criteria->addCondition('email = :email');
		$criteria->params[':email'] = $session['login_member']['email'];
		$criteria->order = 'date_add DESC';

		$data = new CActiveDataProvider('OrOrder', array(
			'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>$pageSize,
		    ),
		));

		$this->render('order', array(
			'data' => $data,
		));
	}
	public function actionVieworder($nota)
	{
		$session = new CHttpSession;
		$session->open();
		$modelOrder = OrOrder::model()->find('CONCAT(`invoice_prefix`, "-", RIGHT(CONCAT("0000",`invoice_no`), 4)) LIKE :search', array(':search'=>$nota));

		if (is_null($modelOrder))
			throw new CHttpException(404,'The requested page does not exist.');
		
		if ($_REQUEST['token'] != '' AND $_GET['action'] == 'success') {
	        $e=new ExpressCheckout;
	
			$modelOrder = OrOrder::model()->find('token = :token AND CONCAT(`invoice_prefix`, "-", RIGHT(CONCAT("0000",`invoice_no`), 4)) LIKE :search', array(':token'=>$_REQUEST['token'], ':search'=>$nota));
			if (is_null($modelOrder))
				throw new CHttpException(404,'The requested page does not exist.');

	        $paymentDetails=$e->getPaymentDetails($_REQUEST['token']); //1.get payment details by using the given token
	 		// print_r($paymentDetails);
	        if($paymentDetails['ACK']=="Success")
	        {
	        	if ($paymentDetails['CHECKOUTSTATUS'] == 'PaymentActionNotInitiated') {
		        	$ack=$e->doPayment($paymentDetails);  //2.Do payment

		        	$modelOrder->order_status_id = 17;
		        	$modelOrder->save(false);

					// save history
					// $modelHistory = new OrOrderHistory;
					// $modelHistory->member_id = $modelOrder->customer_id;
					// $modelHistory->order_id = $modelOrder->id;
					// $modelHistory->order_status_id = 17;
					// $modelHistory->notify = '';
					// $modelHistory->comment =  'Payment by paypal no '.$modelOrder->invoice_prefix.'-'.$modelOrder->invoice_no.' successfully';
					// $modelHistory->date_add = date("Y-m-d H:i:s");
					// $modelHistory->save(false);

				    $order = OrOrderProduct::model()->findAll('order_id = :order_id', array(':order_id'=>$modelOrder->id));
					
					$mail = $this->renderPartial('//mail/cart-paypal', array(
						'model'=>$modelOrder,
						'order'=>$order,
					), true);
					// echo $mail;
					// exit;

					$config = array(
						'to'=>array($modelOrder->email, $this->setting['email']),
						// 'to'=>array($model->email),
						'subject'=>'Trusted Surgeons Pay With Paypal Successfully',
						'message'=>$mail,
					);
					// kirim email
					Common::mail($config);


	        	}
	        }
		}


		$data = OrOrderProduct::model()->findAll('order_id = :order_id', array(':order_id'=>$modelOrder->id));

		$this->pageTitle = 'View Order - '.$this->pageTitle;
		$this->render('vieworder', array(
			'data' => $data,
			'modelOrder' => $modelOrder,
		));
	}

	public function actionForgot()
	{
		$modelLogin = new LoginForm2;

		if(isset($_POST['LoginForm2']))
		{
			$modelLogin->attributes=$_POST['LoginForm2'];
			// validate user input and redirect to the previous page if valid
			if ($modelLogin->username != '') {
				$hash = urlencode(base64_encode($modelLogin->username.'|'.rand(1000000,10000000)));
				$mail = $this->renderPartial('//mail/forgotpass', array(
					'hash'=>$hash,
					'email'=>$modelLogin->username,
				), true);
				// echo $mail;
				// exit;

				$config = array(
					'to'=>array($modelLogin->username),
					// 'to'=>array($model->email),
					'subject'=>'Trusted Surgeons Forgot Password',
					'message'=>$mail,
				);
				// kirim email
				Common::mail($config);
				Yii::app()->user->setFlash('success','Please access your email to reset your password');
				$this->redirect(array('index'));
			}
		}

		// $this->pageTitle = 'Login & Register - '.$this->pageTitle;
		// $this->layout='//layouts/home';
		$this->render('forgot', array(
			'modelLogin'=>$modelLogin,
			// 'modelDelivery'=>$modelDelivery,
		));	
	}
	public function actionChangepass($hash)
	{
		$email = explode('|', base64_decode(urldecode($hash)));
		$email = $email[0];
		
		$model = MeMember::model()->find('email = :email', array(':email'=>$email));

		if(isset($_POST['MeMember'])) {
			$pass = $model->pass;
			$model->attributes = $_POST['MeMember'];
			if ($_POST['MeMember']['pass'] != '') {
				$model->scenario = 'changePass';
				$model->pass = sha1($model->pass);
				$model->pass2 = sha1($model->pass2);
			}
			if ($model->validate()) {
				if(!$model->hasErrors())
				{
					$model->save();
					Yii::app()->user->setFlash('success','Your password has been changed, please login');
					$this->redirect(array('index'));
				}
			}
		}
		$model->pass = '';
		$model->pass2 = '';

		// $this->pageTitle = 'Login & Register - '.$this->pageTitle;
		// $this->layout='//layouts/home';
		$this->render('changepass', array(
			'model'=>$model,
			// 'modelDelivery'=>$modelDelivery,
		));	
	}

}
