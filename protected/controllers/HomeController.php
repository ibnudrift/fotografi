<?php

class HomeController extends Controller
{

	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}	

	public function actionDummy()
	{
		Dummy::createDummyProduct();
		echo '<META http-equiv="refresh" content="0;URL=http://localhost/dv-computers/home/dummy">';
	}

	public function actionImageupdate()
	{
		$data = SettingDescription::model()->findAll();
		foreach ($data as $key => $value) {
			$value->value = str_replace('/{images}/', '/images/', $value->value);
			$value->save(false);
		}
		echo "success"; die();
	}

	public function actionCroncategory()
	{
		$data = PrdProduct::model()->findAll();
		foreach ($data as $key => $value) {
			$tag = PrdCategory::model()->getBreadcrump($value->category_id, $this->languageID);
			$dataTag = array();
			foreach ($tag as $k => $v) {
				$dataTag[] = $k;
			}
			$value->tag = implode(', ', $dataTag);
			$value->save();
		}
	}

	public function actionTemp()
	{
		$data = Temp::model()->findAll();
		foreach ($data as $key => $value) {
			$value->f = trim($value->f);
			$value->g = trim($value->g);
			$value->g = str_replace('P: ', '', $value->g);
			$value->h = str_replace('F: ', '', $value->h);
			$value->save(false);
			print_r($value->attributes);
		}
	}
	public function actionInput()
	{
		$data = Temp::model()->findAll();
		foreach ($data as $key => $value) {
			// cek doctor exist
			$dataDoctor = Doctor::model()->find('name = :name', array(':name'=>$value->a));
			// create doctor login
			if ($dataDoctor == null) {
				$modelDoctor = new Doctor;
				$modelDoctor->name = $value->a;
				$modelDoctor->certification = $value->b;
				$modelDoctor->save(false);
				$dataDoctor = $modelDoctor;
			}

			// $dataSurgeons = DoctorClinic::model()->find('address_1 = :address_1', array(':address_1'=>$value->c));
			// if ($dataSurgeons == null) {
				$modelDoctor = new DoctorClinic;
				$modelDoctor->doctor_id = $dataDoctor->id;
				$modelDoctor->address_1 = $value->c;
				$modelDoctor->address_2 = $value->d;
				$modelDoctor->name = $value->e;
				$modelDoctor->suburb = $value->f;
				$modelDoctor->phone = $value->g;
				$modelDoctor->fax = $value->h;
				$modelDoctor->website = $value->i;
				$modelDoctor->save(false);
			// }


			// loop
			// create clinic
		}
	}
	public function actionInputspec()
	{
		$data = Doctor::model()->findAll();
		$dataSpec = array(
			8,
			10,
			11,
			12,
			13,
			14,
			15,
			16,
			17,
			18,
			25,
			26,
			27,
			28,
			20,
			21,
			22,
			23,
		);
		foreach ($data as $key => $value) {
			foreach ($dataSpec as $k => $v) {
				$modelSpec = new DoctorSpecialication;
				$modelSpec->doctor_id = $value->id;
				$modelSpec->specialitation_id = $v;
				$modelSpec->save(false);
			}
		}
	}

	public function actionGeocode()
	{
		$dataClinic = DoctorClinic::model()->findAll('latitude = 0');
		foreach ($dataClinic as $key => $value) {
			$params = array(
				'address' => $value->address_2.', '.$value->suburb,
				'key' => 'AIzaSyCnVYV9PU2hDS4GMEJ_TZ2Hy-zy1iXfQX0',
			);
			$params_string = http_build_query($params);

			$url = 'https://maps.googleapis.com/maps/api/geocode/json?'.$params_string;


			$ch = curl_init();
			 
			curl_setopt($ch, CURLOPT_URL, $url);
			// curl_setopt($ch, CURLOPT_POST, count($params));
			// curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			 
			//execute post
			$request = curl_exec($ch);
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			if ( $request === false ) {
				echo curl_error($ch);
			}else if($httpCode == 200) {
				$result = json_decode($request);
				if (count($result->results) > 0) {
					$dataGeometry = $result->results[0]->geometry;
					$value->latitude = $dataGeometry->location->lat;
					$value->longitude = $dataGeometry->location->lng;
					$value->save(false);
				// print_r($dataGeometry);
				}
				// exit;
				// echo "<pre>";
				// print_r($result);
				// echo "</pre>";
			}else{
				print_r(json_decode($request));

			}
			// exit;
		}

	}

	public function actionIndex()
	{
		// $criteria = new CDbCriteria;
		// $criteria->addCondition('active = "1"');
		// $criteria->addCondition('language_id = :language_id');
		// $criteria->params[':language_id'] = $this->languageID;
		// if ($_GET['category'] != '') {
		// 	$criteria->addCondition('t.topik_id = :category');
		// 	$criteria->params[':category'] = $_GET['category'];
		// }
		// $criteria->order = 'date_input DESC';
		// $gallery = new CActiveDataProvider('ViewGalleryok', array(
		// 	'criteria'=>$criteria,
		//     'pagination'=>array(
		//         'pageSize'=>6,
		//     ),
		// ));
		
		$this->set_parallax = true;

		$this->layout='//layouts/column1';

		$this->render('index_new', array(
			// 'gallery'=>$gallery,
		));
	}

	public function actionSpecialisationList()
	{
		$dataCategory = (PrdCategory::model()->categoryTree('spesialis', $this->languageID));
		// $data = array();
		$child = array();
		foreach ($dataCategory as $key => $value) {
			if (count($value['children']) > 0) {
				foreach ($value['children'] as $v) {
					array_push($child, array(
						'name'=>$v['title'],
						'type'=>$value['title']
					));
				}
				// $data[] = $child;
			}
		}
		echo json_encode($child);

	}

	public function actionLocationList()
	{
		$api = new PlaceGoogle;
		// print_r($dataArray);
		echo json_encode($api->placeAutocomplete($_GET['phrase']));
	}

	public function actionSuburb()
	{
		$suburbData = DoctorClinic::model()->findAll('suburb LIKE :suburb GROUP BY suburb', array(':suburb'=>$_GET['term'].'%'));
		$dataSuburb = array();
		foreach ($suburbData as $key => $value) {
			$dataSuburb[] = array(
				'id'=>$value->suburb,
				'label'=>$value->suburb,
				'value'=>$value->suburb,
			);
		}
		echo(json_encode($dataSuburb));
	}

	public function actionError()
	{
		$this->layout = '//layouts/errorpage';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else{
				// $this->layout='//layouts/column1';

				$this->pageTitle = 'Error '.$error['code'].': '. $error['message'] .' - '.$this->pageTitle;
				$this->render('error', array(
					'error'=>$error,
				));
			}
		}
	}

	public function actionJourney()
	{
		$this->pageTitle = 'Start Journey - '.$this->pageTitle;
		if ($_REQUEST['step'] != '') {
			$v_step = $_REQUEST['step'];
			switch ($v_step) {
				case '2':
					$this->render('step-2', array());
					break;
				case '3':
					if (isset($_GET['category'])) {
						$this->render('step-3c', array());
					} else {
						$this->render('step-3', array());
					}
					
					break;
				case '4':
					$this->render('step-4', array());
					break;
				case '5':
					$this->render('step-5', array());
					break;
				case '6':
					$this->render('step-6', array());
					break;
				case '7':
					$this->render('step-7', array());
					break;
				case '8':
					$this->render('step-8', array());
					break;
				case '9':
					$this->render('step-9', array());
					break;
				case '10':
					$this->render('step-10', array());
					break;
				

				default:
					$this->render('step-1', array());					
					break;
			}
		}else{
			$this->render('journey', array(	
			));
		}
	}

	public function actionBlog()
	{
		$this->pageTitle = 'Blog - '. $this->pageTitle;

		$this->render('blog', array(
		));
	}

	public function actionBlogDetail()
	{
		$this->pageTitle = 'Blog Detail - '. $this->pageTitle;

		$this->render('blog_detail', array(
		));
	}

	public function actionService()
	{
		$this->pageTitle = 'Services - '.$this->pageTitle;

		$this->render('service', array(
		));
	}

	public function actionContact()
	{
		// $this->layout='//layouts/column1';

		$this->pageTitle = 'Contact Us - '.$this->pageTitle;

		$model = new ContactForm;
		$model->scenario = 'insert';

		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];

			if($model->validate())
			{
				// config email
				$messaged = $this->renderPartial('//mail/contact',array(
					'model'=>$model,
				),TRUE);
				$config = array(
					'to'=>array($model->email, $this->setting['email'], $this->setting['contact_email']),
					'subject'=>'Hi, Sym Pictures Contact from '.$model->email,
					'message'=>$messaged,
				);
				if ($this->setting['contact_cc']) {
					$config['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config['bcc'] = array($this->setting['contact_bcc']);
				}
				// kirim email
				Common::mail($config);

				Yii::app()->user->setFlash('success','Thank you for contact us. We will respond to you as soon as possible.');
				$this->refresh();
			}

		}

		$this->render('contact', array(
			'model'=>$model,
		));
	}

	public function actionAbout()
	{
		$this->pageTitle = 'About Us - '.$this->pageTitle;

		$this->render('about', array(
		));
	}

	public function actionCoaching()
	{
		$this->pageTitle = 'Coaching - '.$this->pageTitle;

		$this->render('coaching', array(
		));
	}

	public function actionCoaching2()
	{
		$this->pageTitle = 'Coaching - '.$this->pageTitle;

		$this->render('coaching2', array(
		));
	}

	public function actionEducation()
	{
		$this->pageTitle = '3D Surgery Animation - Education - '.$this->pageTitle;

		$this->render('education', array(
		));
	}

	public function actionEducation2()
	{
		$this->pageTitle = 'Faq - Education - '.$this->pageTitle;

		$this->render('education2', array(
		));
	}

	public function actionProcedures()
	{
		$this->pageTitle = 'Procedures - '.$this->pageTitle;

		$this->render('procedures', array(
		));
	}

	public function actionContact2()
	{
		// $this->pageTitle = 'Procedures - '.$this->pageTitle;

		// $this->render('procedures', array(
		// ));
	}

	public function actionSurgeons()
	{
		$this->pageTitle = 'Surgeons - '. $this->pageTitle;

		$this->render('surgeons', array(
		));
	}

	public function actionSurgeons2()
	{
		$this->pageTitle = 'Surgeons - '. $this->pageTitle;

		$this->render('surgeons2', array(
		));
	}

	public function actionSurgeons_detail()
	{
		$this->pageTitle = 'Surgeons Detail - '. $this->pageTitle;

		$this->render('surgeons_detail', array(
		));
	}

	public function actionProducts_landing()
	{
		$this->pageTitle = 'Shop - '. $this->pageTitle;

		$this->render('product_landing', array(
		));
	}

	public function actionProducts_list()
	{
		$this->pageTitle = 'Shop - '. $this->pageTitle;

		$this->render('product_list', array(
		));
	}

	public function actionProducts_detail()
	{
		$this->pageTitle = 'Shop Details - '. $this->pageTitle;

		$this->render('product_detail', array(
		));
	}

	public function actionWhyClinic()
	{
		$this->pageTitle = 'Why your Clinic should join - '.$this->pageTitle;

		$this->render('why_clinic', array(
		));
	}

	public function actionSurgeons_blog()
	{
		$this->pageTitle = 'Surgeons Blog - '.$this->pageTitle;

		$this->render('surgeon_blog', array(
		));
	}

	public function actionClinic_faq()
	{
		$this->pageTitle = 'Clinic FAQ - '.$this->pageTitle;

		$this->render('clinic_faq', array(
		));
	}

}

