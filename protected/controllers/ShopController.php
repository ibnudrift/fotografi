<?php

class ShopController extends Controller
{

	public function actionIndex()
	{
		$this->pageTitle = 'Product - '.$this->pageTitle;

		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$data = PrdCategory::model()->with('description')->findAll($criteria);

		$this->render('index', array(
			'data'=>$data,
		));
	}

	public function actionList()
	{
		$this->pageTitle = 'Product - '.$this->pageTitle;

		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;

		if (isset($_GET['id'])) {
			$id = $_GET['id'];
			$criteria->addCondition('t.id = :id');
			$criteria->params[':id'] = $id;
		}

		$category = PrdCategory::model()->find($criteria);

		// if($category===null)
		// 	throw new CHttpException(404,'The requested page does not exist.');
		
		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('status = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;

		if (isset($_GET['id'])) {
			$id = $_GET['id'];
			$criteria->addCondition('category_id = :category_id');
			$criteria->params[':category_id'] = $id;
		}

		if ($_GET['pagesize'] != '') {
			$pageSize = $_GET['pagesize'];
		} else {
			$pageSize = 15;
		}

		$data = new CActiveDataProvider('PrdProduct', array(
			'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>$pageSize,
		    ),
		));
		

		$this->render('list', array(
			'data'=>$data,
			'category'=>$category,
		));
	}
	
	public function actionDetail($id)
	{
		$criteria=new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('status = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->addCondition('t.id = :id');
		$criteria->params[':id'] = $id;
		$data = PrdProduct::model()->find($criteria);
		if($data===null)
			throw new CHttpException(404,'The requested page does not exist.');

		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->addCondition('t.id = :id');
		$criteria->params[':id'] = $data->category_id;
		$category = PrdCategory::model()->find($criteria);
		if($category===null)
			throw new CHttpException(404,'The requested page does not exist.');

		$this->pageTitle = $data->description->name.' | '.$this->pageTitle;

		$this->render('detail', array(
			'data'=>$data,
			'category'=>$category,
		));
	}
	public function actionAddcart()
	{
		if ($_POST['id'] != '') {
			if ( ! $_POST['id'])
				throw new CHttpException(404,'The requested page does not exist.');

			$id = $_POST['id'];
			$qty = $_POST['qty'];
			$optional = $_POST['optional'];
			$option = $_POST['option'];

			$model = new Cart;

			$data = PrdProduct::model()->findByPk($id);

			if (is_null($data))
				throw new CHttpException(404,'The requested page does not exist.');

			$model->addCart($id, $qty, $data->harga, $option, $optional);
			
			Yii::app()->user->setFlash('success', $qty.' Item(s) has been added to the cart shop');
			$this->redirect(array('detail', 'id'=>$data->id));
		}
	}
}

