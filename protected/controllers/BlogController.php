<?php

class BlogController extends Controller
{

	public function actionIndex()
	{
		$criteria = new CDbCriteria;

		// // Mengatur Order
		// if ($_GET['order'] == 'alphabetic') {
		// 	$criteria->order = 't.name ASC';
		// } elseif($_GET['order'] == 'most_review') {
		// 	$criteria->order = 'n_review DESC';
		// } elseif($_GET['order'] == 'date_join') {
		// 	$criteria->order = 'date_input DESC';
		// } else {
			$criteria->order = 't.date_input DESC';
		// }

		$criteria->group = 't.id';

		// if ($_GET['name'] != '') {
		// 	$criteria->addCondition('t.name LIKE :name');
		// 	$criteria->params[':name'] = '%'.$_GET['name'].'%';
		// }

		// if ($_GET['specialisation'] != '') {
		// 	$criteria->addCondition('specialication.specialitation_id = :specialisation');
		// 	$criteria->params[':specialisation'] = $_GET['specialisation'];
		// }

		// if ($_GET['suburb'] != '') {
		// 	$criteria->addCondition('clinic.suburb LIKE :suburb');
		// 	$criteria->params[':suburb'] = '%'.$_GET['suburb'].'%';
		// }

		$dataFeatured = new CActiveDataProvider('DoctorBlog', array(
			'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>2,
		    ),
		));
		$arrayFeatured = array();
		foreach ($dataFeatured->getData() as $key => $value) {
			$arrayFeatured[] = $value->id;
		}
		$criteria->addNotInCondition('id', $arrayFeatured);
		$dataBlog = new CActiveDataProvider('DoctorBlog', array(
			'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>8,
		    ),
		));
		$this->render('index', array(
			'dataFeatured'=>$dataFeatured,
			'dataBlog'=>$dataBlog,
			// 'categoryName'=>$categoryName,
			// 'writer'=>$writer,
			// 'data'=> $konten,
			// 'subMenu'=>$subMenu,
			// 'terbaru'=>$terbaru,
		));
	}
	// public function actionDetail($id)
	public function actionDetail($id)
	{
		$criteria = new CDbCriteria;

		$criteria->addCondition('id = :id');
		$criteria->params[':id'] = $id;

		$dataBlog = DoctorBlog::model()->find($criteria);
		if($dataBlog===null)
			throw new CHttpException(404,'The requested page does not exist.');

		$this->pageTitle = $dataBlog->title . ' | '.$this->pageTitle;

		$criteria = new CDbCriteria;
		$criteria->order = 'RAND()';
		$criteria->addCondition('id != :id');
		$criteria->params[':id'] = $dataBlog->id;
		$dataBlogs = new CActiveDataProvider('DoctorBlog', array(
			'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>10,
		    ),
		));

		$this->render('detail', array(
			'dataBlog' => $dataBlog,
			'dataBlogs' => $dataBlogs,
			// 'menu'=>$menu,
			// 'data'=> $konten,
			// 'subMenu'=>$subMenu,
			// 'categoryData'=>$categoryData,
			// 'terbaru'=>$terbaru,
			// 'categoryName'=>$categoryName,
		));
	}

	public function actionList()
	{

		$this->layout='//layouts/home';

		// convert to list item menu
		$categoryName = Product::model()->getCategoryName();

		$konten = Blog::model()->getAllData(10, false, $this->languageID);

		$this->pageTitle = $konten['pageTitle'].' - ' . $this->pageTitle;
		if ($_GET['topik'] == 'topik-panduan-pemula') {
		$this->render('panduan', array(
			'categoryName'=>$categoryName,
			'data'=> $konten,
		));
		}elseif($_GET['topik'] == 'topik-workout-list'){
		$this->render('workout', array(
			'categoryName'=>$categoryName,
			'data'=> $konten,
		));
		}else{
		$this->render('list', array(
			'categoryName'=>$categoryName,
			'data'=> $konten,
		));
		}
	}
	public function actionCalculator()
	{

		$this->layout='//layouts/home';
		$this->pageTitle = 'Fitness Calculator | ' . $this->pageTitle;
		$this->render('calculator', array(
		));
	}
	public function actionCalc($type)
	{
		switch ($type) {
			case 'bmi':
				$tampilan = 'calc-bmi';
				break;
			
			case 'bmr':
				$tampilan = 'calc-bmr';
				break;
			
			case 'kalori':
				$tampilan = 'calc-kalori';
				break;
			
			case 'minum':
				$tampilan = 'calc-minum';
				break;
			
			case 'nutrisi':
				$tampilan = 'calc-nutrisi';
				break;
			
			default:
				$tampilan = 'calc-bmi';
				break;
		}

		$this->layout='//layoutsAdmin/mainKosong';
		$this->pageTitle = 'Fitness Calculator | ' . $this->pageTitle;
		$this->render($tampilan, array(
		));
	}

	// public function actionPanduan()
	// {

	// 	$this->layout='//layouts/home';
	// 	$this->pageTitle = 'Panduan Fitness untuk Pemula | ' . $this->pageTitle;
	// 	$this->render('panduan', array(
	// 	));
	// }
	// public function actionWorkout()
	// {

	// 	$this->layout='//layouts/home';
	// 	$this->pageTitle = 'Workout List Fitness | ' . $this->pageTitle;
	// 	$this->render('workout', array(
	// 	));
	// }
}