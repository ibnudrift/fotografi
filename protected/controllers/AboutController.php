<?php

class AboutController extends Controller
{

	public function actionIndex()
	{
		$this->pageTitle = 'About Us - '.$this->pageTitle;

		$this->render('index', array(
		));
	}
	public function actionMission()
	{
		$this->pageTitle = 'About Us - '.$this->pageTitle;

		$this->render('mission', array(
		));
	}
	public function actionWhy()
	{
		$this->pageTitle = 'About Us - '.$this->pageTitle;

		$this->render('why', array(
		));
	}
	public function actionMe()
	{
		$this->pageTitle = 'About Us - '.$this->pageTitle;

		$this->render('me', array(
		));
	}
}

