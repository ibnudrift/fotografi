<?php

class ContactController extends Controller
{

	public function actionIndex()
	{

		$this->pageTitle = 'Contact Us - '.$this->pageTitle;

		$model = new ContactForm;
		$model->scenario = 'insert';

		if ($_GET['id']) {
			$dataDoctor = Doctor::model()->with(array('clinics', 'spec', 'videos'))->findByPk($_GET['id']);
			$dataMember = MeMember::model()->findByPk($dataDoctor->id);
		}

		// secret key recaptcha
		// 6LcUECkTAAAAAPVdNP-sfV7zcDDCC80AN4PrJSfe
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];

			$status = true;
	        $secret_key = "6LcUECkTAAAAAPVdNP-sfV7zcDDCC80AN4PrJSfe";
	        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
	        $response = json_decode($response);
	        if($response->success==false)
	        {
	        	$model->addError('verifyCode','Pastikan anda sudah menyelesaikan Captcha.');
	        	$status = false;
	        }

	        // 
			if($status AND $model->validate() )
			{
				// contact customer
				if ($model->contact_type == 'customer') {
					if ($model->coaching == 1) {
						$model->coaching = 'Booking a coaching service';	
					}else{
						$model->coaching = 'Not Booking a coaching service';
					}

					// config email
					$messaged = $this->renderPartial('//mail/contact',array(
						'model'=>$model,
					),TRUE);

					if ($_GET['id']) {
						$config = array(
							'to'=>array($model->email, $this->setting['email'], $dataMember->email),
							'subject'=>'Hi, '.$dataMember->first_name.' Contact for '.strtoupper( ($model->contact_type != '')? $model->contact_type:'' ),
							'message'=>$messaged,
						);
					}else{
						$config = array(
							'to'=>array($model->email, $this->setting['email']),
							'subject'=>'Hi, Trusted Surgeons Contact for '.strtoupper( ($model->contact_type != '')? $model->contact_type:'' ),
							'message'=>$messaged,
						);
					}
					// print_r($config); exit;
					// kirim email
					Common::mail($config);

					Yii::app()->user->setFlash('success','Thank you for contact us. We will respond to you as soon as possible.');
					if ($_GET['id']) {
						$this->redirect(array('/surgeons/detail', 'id'=>$GET['id']));
					}else{
						$this->refresh();
					}
					// end contact customer
				}else{
					// contact surgeons
					// 
					// config email
					$messaged = $this->renderPartial('//mail/contact',array(
						'model'=>$model,
					),TRUE);

					$config = array(
						'to'=>array($model->email, $this->setting['email']),
						'subject'=>'Hi, Trusted Surgeons Contact for '.strtoupper( ($model->contact_type != '')? $model->contact_type:'' ),
						'message'=>$messaged,
					);
					// print_r($config); exit;
					// kirim email
					Common::mail($config);

					Yii::app()->user->setFlash('success','Thank you for contact us. We will respond to you as soon as possible.');
					$this->refresh();
				}
				// end contact surgeons
			}

		}

		$this->render('index', array(
			'model'=>$model,
		));
	}

}