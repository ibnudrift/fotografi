<?php

class MemberclinicController extends ControllerMember
{
	public function actionIndex()
	{
		// https://www.youtube.com/watch?v=mC2q2SGy2e0
		// $data = file_get_contents("http://youtube.com/get_video_info?video_id=".'mC2q2SGy2e0');
		// parse_str($data, $output);

		$model = Doctor::model()->find('member_id = :member_id', array(':member_id'=>$this->memberData['id']));

		if ($_GET['update']) {
		$modelClinic = DoctorClinic::model()->findByPk($_GET['update']);
		}else{
		$modelClinic = new DoctorClinic;
		}

		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		if ($_POST['Doctor']) {
			$photo = $model->photo;//mengamankan nama file
			$cover = $model->cover;//mengamankan nama file
			$model->attributes = $_POST['Doctor'];
			$model->photo = $photo;//mengembalikan nama file
			$model->cover = $cover;//mengembalikan nama file

			$photo = CUploadedFile::getInstance($model,'photo');
			if ($photo->name != '') {
				$model->photo = substr(md5(time()),0,5).'-'.$photo->name;
			}
			$cover = CUploadedFile::getInstance($model,'cover');
			if ($cover->name != '') {
				$model->cover = substr(md5(time()),0,5).'-'.$cover->name;
			}

			if ($photo->name != '') {
				$photo->saveAs(Yii::getPathOfAlias('webroot').'/images/doctor/'.$model->photo);
			}

			if ($cover->name != '') {
				$cover->saveAs(Yii::getPathOfAlias('webroot').'/images/doctor/'.$model->cover);
			}

			$model->save();
			$this->refresh();
		}

		// if ($_FILES['Doctor']) {
		// 	$photo = $model->photo;//mengamankan nama file
		// 	$cover = $model->cover;//mengamankan nama file
		// 	$model->attributes = $_POST['Doctor'];
		// 	$model->photo = $photo;//mengembalikan nama file
		// 	$model->cover = $cover;//mengembalikan nama file

		// 	$photo = CUploadedFile::getInstance($model,'photo');
		// 	if ($photo->name != '') {
		// 		$model->photo = substr(md5(time()),0,5).'-'.$photo->name;
		// 	}
		// 	$cover = CUploadedFile::getInstance($model,'cover');
		// 	if ($cover->name != '') {
		// 		$model->cover = substr(md5(time()),0,5).'-'.$cover->name;
		// 	}
		// 	exit;

		// 	if ($photo->name != '') {
		// 		$photo->saveAs(Yii::getPathOfAlias('webroot').'/images/doctor/'.$model->photo);
		// 	}

		// 	if ($cover->name != '') {
		// 		$cover->saveAs(Yii::getPathOfAlias('webroot').'/images/doctor/'.$model->cover);
		// 	}
		// 	$model->save();
		// 	$this->refresh();
		// }


		if ($_POST['DoctorClinic']) {
			$modelClinic->attributes=$_POST['DoctorClinic'];
			if($modelClinic->validate()){
				$modelClinic->doctor_id = $model->id;
				$modelClinic->save();
				$this->redirect(array('index'));
			}
		}

		$this->render('index', array(
			'model'=>$model,
			'modelClinic'=>$modelClinic,
			// 'model2'=>$model2,
		));	
	}
	public function actionSpecialisation()
	{
		$model = Doctor::model()->find('member_id = :member_id', array(':member_id'=>$this->memberData['id']));

		if ($_POST['Specialisation']) {
			DoctorSpecialication::model()->deleteAll('doctor_id = :id', array(':id'=>$model->id));
			foreach ($_POST['Specialisation'] as $key => $value) {
				if ($value == 1) {
					$modelSpesialis = new DoctorSpecialication;
					$modelSpesialis->doctor_id = $model->id;
					$modelSpesialis->specialitation_id = $key;
					$modelSpesialis->save();
				}
			}
			$this->refresh();
		}

		$this->render('specialisation', array(
			'model'=>$model,
			// 'model2'=>$model2,
		));	
	}
	public function actionAbout()
	{
		$model = Doctor::model()->find('member_id = :member_id', array(':member_id'=>$this->memberData['id']));

		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		if ($_POST['Doctor']) {
			$model->attributes = $_POST['Doctor'];
			$model->save();
			$this->refresh();
		}

		$this->render('about', array(
			'model'=>$model,
			// 'model2'=>$model2,
		));	
	}
	public function actionPhoto()
	{

		$model = Doctor::model()->find('member_id = :member_id', array(':member_id'=>$this->memberData['id']));

		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		if ($_GET['update']) {
		$modelPhoto = DoctorPhoto::model()->findByPk($_GET['update']);
		}else{
		$modelPhoto = new DoctorPhoto;
		}

		if ($_POST['DoctorPhoto'] OR $_POST['DoctorPhoto2']) {
			// $modelPhoto->attributes=$_POST['DoctorPhoto'];
			// print_r($modelPhoto->attributes);
			// exit;

			// // $image = CUploadedFile::getInstance($modelPhoto,'image');
			// // if ($image->name != '') {
			// // 	$modelPhoto->image = substr(md5(time()),0,5).'-'.$image->name;
			// // }

			// if($modelPhoto->validate()){
			// 	// if ($image->name != '') {
			// 	// 	$image->saveAs(Yii::getPathOfAlias('webroot').'/images/doctor_photo/'.$modelPhoto->image);
			// 	// }
			// 	$modelPhoto->doctor_id = $model->id;
			// 	$modelPhoto->save();
			// 	$this->redirect(array('photo'));
			// }
			// print_r($_POST['DoctorPhoto2']);
			// exit;
			DoctorPhoto::model()->deleteAll('doctor_id = :id', array(':id'=>$model->id));
			$mime = array(
				'data:image/jpeg' => '.jpg',
				'data:image/jpg' => '.jpg',
				'data:image/png' => '.png',
				'data:image/gif' => '.gif',
			);
			if (count($_POST['DoctorPhoto']['image']) > 0) {
				foreach ($_POST['DoctorPhoto']['image'] as $key => $value) {
					$modelAttributes[$key] = new DoctorPhoto;
					if ($value != '') {
						// print_r($_POST['DoctorPhoto']['image'][$key]);
						list($type, $dataImage) = explode(';', $_POST['DoctorPhoto']['image'][$key]);
						list(, $dataImage)      = explode(',', $dataImage);
						$dataImage = base64_decode($dataImage);
						// echo $type;
						// file_put_contents('/tmp/image.png', $dataImage);
						if (array_key_exists($type, $mime)) {
							$modelAttributes[$key]->title = $_POST['DoctorPhoto']['title'][$key];
							$modelAttributes[$key]->doctor_id = $model->id;
							$modelAttributes[$key]->image = substr(md5(time()),0,5).'-'.Slug::create($modelAttributes[$key]->title).'.jpg';
							file_put_contents(Yii::getPathOfAlias('webroot').'/images/doctor_photo/'.$modelAttributes[$key]->image, $dataImage);
							$modelAttributes[$key]->procedures_id = $_POST['DoctorPhoto']['procedures_id'][$key];
							$modelAttributes[$key]->save(false);
						}
					}
					
				}
			}
			// exit;
			if (count($_POST['DoctorPhoto2']['image']) > 0) {
				foreach ($_POST['DoctorPhoto2']['image'] as $key => $value) {
					$modelAttributes[$key] = new DoctorPhoto;
					if ($value != '') {
						$modelAttributes[$key]->title = $_POST['DoctorPhoto2']['title'][$key];
						$modelAttributes[$key]->doctor_id = $model->id;
						$modelAttributes[$key]->image = $value;
						$modelAttributes[$key]->procedures_id = $_POST['DoctorPhoto2']['procedures_id'][$key];
						$modelAttributes[$key]->save(false);
					}
					
				}
			}
			$this->redirect(array('photo'));
		}

		$this->render('photo', array(
			'model'=>$model,
			'modelPhoto'=>$modelPhoto,
		));	
	}
	public function actionVideo()
	{
		$model = Doctor::model()->find('member_id = :member_id', array(':member_id'=>$this->memberData['id']));

		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		$modelVideo = array();
		$modelVideo = DoctorVideo::model()->findAll('doctor_id = :id ORDER BY id', array(':id'=>$model->id));

		if ($_POST['DoctorVideo']) {
			DoctorVideo::model()->deleteAll('doctor_id = :id', array(':id'=>$model->id));
			if (count($_POST['DoctorVideo']['url']) > 0) {
				foreach ($_POST['DoctorVideo']['url'] as $key => $value) {
					$modelVideo[$key] = new DoctorVideo;
					if ($value != '') {
						$modelVideo[$key]->id_str = $_POST['DoctorVideo']['id_str'][$key];
						$modelVideo[$key]->doctor_id = $model->id;
						$modelVideo[$key]->url = $value;
						parse_str( parse_url( $value, PHP_URL_QUERY ), $dataUrl );
						$modelVideo[$key]->code = $dataUrl['v'];
						$dataVideo = @file_get_contents("http://youtube.com/get_video_info?video_id=".$dataUrl['v']);
						parse_str($dataVideo, $dataYoutube);
						$modelVideo[$key]->title = $dataYoutube['title'];
						$modelVideo[$key]->save(false);
						$modelVideo[$key]->id_str = $modelVideo[$key]->id;
						$modelVideo[$key]->save(false);
					}
					
				}
			}
			$this->refresh();
		}
		$this->render('video', array(
			'model'=>$model,
			'modelVideo'=>$modelVideo,
		));	
	}
	public function actionPublish()
	{
		$model = Doctor::model()->find('member_id = :member_id', array(':member_id'=>$this->memberData['id']));
		$dataSpec = DoctorSpecialication::model()->findAll('doctor_id = :id', array(':id'=>$model->id));
		if (count($dataSpec) == 0) {
			Yii::app()->user->setFlash('danger','Please select specialisation');
			$this->redirect(array('index'));
		}
		$dataClinic = DoctorClinic::model()->findAll('doctor_id = :id', array(':id'=>$model->id));
		if (count($dataClinic) == 0) {
			Yii::app()->user->setFlash('danger','Please fill clinic form');
			$this->redirect(array('index'));
		}
		if ($model->name == '') {
			Yii::app()->user->setFlash('danger','Please fill your name');
			$this->redirect(array('index'));
		}
		$model->status = 1;
		$model->save();
		Yii::app()->user->setFlash('success','Profile published');
		$this->redirect(array('index'));
	}
	public function actionUnpublish()
	{
		$model = Doctor::model()->find('member_id = :member_id', array(':member_id'=>$this->memberData['id']));
		$model->status = 0;
		$model->save();
		Yii::app()->user->setFlash('danger','Profile unpublished');
		$this->redirect(array('index'));
	}
}