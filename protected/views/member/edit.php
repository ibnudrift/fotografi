<section class="outers_inside_page p_member">
	<div class="insides_page">
		<div class="back-white backs_cloud_insidep content-text text-center">
			<div class="prelatife container">
				<div class="clear h60"></div>
				<h1 class="title-pages">Lengkapi data anda  dengan mengisi <span class="qwig">Form di Bawah Ini</span></h1>
				<div class="clear height-50"></div><div class="height-20"></div>
				<div class="mw1035 tengah">
					<span class="fRoboto">Cahaya Diagnostic Centre sebagai pemimpin dalam dunia medis berteknologi mutakhir, memberikan layanan kepada pasien yang telah menjadi member, untuk dapat memanfaatkan layanan online dalam mengakses hasil pemeriksaan secara ringkas dan nyaman di database terbuka kami.</span>
					
					<p>Bagi anda yang pertama kali login, silahkan lakukan pengisian data terlebih dahulu.</p>
				</div>
				<div class="clear height-50"></div><div class="height-0"></div>

				<div class="clear"></div>
			</div>
		</div>
		<div class="back-white">
			<div class="prelatife container">
				<div class="clear height-50"></div><div class="height-40"></div>

				<div class="mw800 tengah outers_cont_defaultmember">
					<div class="row">
						<div class="col-md-12 col-sm-12">
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'daftar-form',
    // 'type'=>'horizontal',
	'enableClientValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
)); ?>
							  <h3>Aktifasi</h3>
							  <?php echo CHtml::errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>
							  <div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<?php echo $form->labelEx($model, 'first_name', array('class'=>'control-label')) ?>
						    	<?php echo $form->textField($model, 'first_name', array('class'=>'form-control')) ?>
							</div>
							<div class="form-group">
								<?php echo $form->labelEx($model, 'email', array('class'=>'control-label')) ?>
						    	<?php echo $form->textField($model, 'email', array('class'=>'form-control')) ?>
							</div>
							<div class="form-group">
								<?php echo $form->labelEx($model, 'hp', array('class'=>'control-label')) ?>
						    	<?php echo $form->textField($model, 'hp', array('class'=>'form-control')) ?>
							</div>
							<div class="form-group">
								<?php echo $form->labelEx($model, 'address', array('class'=>'control-label')) ?>
						    	<?php echo $form->textField($model, 'address', array('class'=>'form-control')) ?>
							</div>
							<div class="form-group">
							  	<div class="col-sm-4">
							  		<a href="<?php echo CHtml::normalizeUrl(array('index')); ?>" class="forgt_pasword">Back</a>
							  	</div>
							    <div class="col-sm-8">
							      <button type="submit" class="btn btn-default btn_custom_member">EDIT SEKARANG</button>
							    </div>
							</div>

							<div class="clear"></div>
						</div>							
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<?php echo $form->labelEx($model, 'passold', array('class'=>'control-label')) ?>
						    	<?php echo $form->passwordField($model, 'pass', array('class'=>'form-control')) ?>
							</div>
							<div class="form-group">
								<?php echo $form->labelEx($model, 'pass', array('class'=>'control-label')) ?>
						    	<?php echo $form->passwordField($model, 'pass', array('class'=>'form-control')) ?>
							</div>
							<div class="form-group">
								<?php echo $form->labelEx($model, 'pass2', array('class'=>'control-label')) ?>
						    	<?php echo $form->passwordField($model, 'pass2', array('class'=>'form-control')) ?>
							</div>

							<div class="clear"></div>
						</div>
						</div>						
<?php $this->endWidget(); ?>
						</div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="clear height-50"></div><div class="height-40"></div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</section>
<div class="clear"></div>