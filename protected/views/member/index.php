<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static page_member_signup_content">
        <h1 class="title_page">SIGN IN</h1>
        <div class="clear height-50"></div> <div class="clear height-5"></div>
        <div class="clear"></div>
            <?php if(Yii::app()->user->hasFlash('success_register')): ?>
              <div class="padding-left-50 padding-right-50">
                <?php $this->widget('bootstrap.widgets.TbAlert', array(
                    'htmlOptions' => array('class'=>'alert-success'),
                    'alerts'=>array('success_register'),
                )); ?>
                <div class="clear height-20"></div>
              </div>
            <?php endif; ?>
            <?php if(Yii::app()->user->hasFlash('success')): ?>
              <div class="padding-left-50 padding-right-50">
                <?php $this->widget('bootstrap.widgets.TbAlert', array(
                    'htmlOptions' => array('class'=>'alert-success'),
                    'alerts'=>array('success'),
                )); ?>
                <div class="clear height-20"></div>
              </div>
            <?php endif; ?>
            <?php if(Yii::app()->user->hasFlash('danger')): ?>
              <div class="padding-left-50 padding-right-50">
                <?php $this->widget('bootstrap.widgets.TbAlert', array(
                    'htmlOptions' => array('class'=>'alert-success'),
                    'alerts'=>array('danger'),
                )); ?>
                <div class="clear height-20"></div>
              </div>
            <?php endif; ?>

        <div class="row">
          <div class="col-md-6 text-left border-right">
            <div class="mw400 tengah text-left box_sign_default">
              <div class="height-10"></div>
              <div class="clear"></div>
              <div class="pict_full"><img src="<?php echo $this->assetBaseurl ?>picture-sign-1.jpg" alt="" class="img-responsive"></div>
              <div class="clear height-25"></div>
              <div class="b_form_sign">
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    // 'id'=>'daftar-form',
    // 'type'=>'horizontal',
  'enableClientValidation'=>false,
  'clientOptions'=>array(
    'validateOnSubmit'=>false,
  ),
)); ?>
                  <h3 class="sub_title">Patient Sign In</h3>
                  <?php echo CHtml::errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>
                  <div class="clear"></div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <?php echo $form->textField($model, 'email', array('class'=>'form-control')) ?>
                  </div>

                  <div class="form-group">
                    <label for="">Password</label>
                    <?php echo $form->passwordField($model, 'pass', array('class'=>'form-control')) ?>
                  </div>

                  <button type="submit" class="btn btn-default btns_form_default_submit">SIGN IN</button>
                  <div class="clear height-25"></div>
                  <p class="help-block"><a href="<?php echo CHtml::normalizeUrl(array('/member/forgot')); ?>" target="_blank">Forgot Password</a>?<br />
                  Don&#39;t have an account? <a href="<?php echo CHtml::normalizeUrl(array('/member/signup', 'ret'=>$_GET['ret'])); ?>" target="_blank">Sign Up</a></p>
<?php $this->endWidget(); ?>
                <div class="clear"></div>
              </div>
              <div class="clear"></div>
            </div>
            <!-- end box --> <div class="clear height-10"></div>

          </div>
          <div class="col-md-6 text-left">
            <div class="mw400 tengah text-left box_sign_default">
              <div class="height-10"></div>
              <div class="clear"></div>
              <div class="pict_full"><img src="<?php echo $this->assetBaseurl ?>picture-sign-2.jpg" alt="" class="img-responsive"></div>
              <div class="clear height-25"></div>
              <div class="b_form_sign">
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    // 'id'=>'daftar-form',
    // 'type'=>'horizontal',
  'enableClientValidation'=>false,
  'clientOptions'=>array(
    'validateOnSubmit'=>false,
  ),
)); ?>                  
                  <h3 class="sub_title">Doctor Sign In</h3>
                  <?php echo CHtml::errorSummary($model2, '', '', array('class'=>'alert alert-danger')); ?>
                  <div class="clear"></div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <?php echo CHtml::textField('MeMember2[email]', $model2->email, array('class'=>'form-control')) ?>
                  </div>

                  <div class="form-group">
                    <label for="">Password</label>
                    <?php echo CHtml::passwordField('MeMember2[pass]', $model2->pass, array('class'=>'form-control')) ?>
                  </div>
                  <button type="submit" class="btn btn-default btns_form_default_submit">SIGN IN</button>
                  <div class="clear height-25"></div>
                  <p class="help-block"><a href="<?php echo CHtml::normalizeUrl(array('/member/forgot')); ?>" target="_blank">Forgot Password</a>?<br />
                  Don&#39;t have an account? <a href="<?php echo CHtml::normalizeUrl(array('/member/signup', 'ret'=>$_GET['ret'])); ?>" target="_blank">Sign Up</a></p>
<?php $this->endWidget(); ?>
                <div class="clear"></div>
              </div>
              <div class="clear"></div>
            </div>
            <!-- end box -->

          </div>
          <!-- end col md 6 -->
        </div>
        <div class="clear height-20"></div>

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>

