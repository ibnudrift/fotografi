<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="insides_static page_member_s_content">
        <div class="insides_static page_member_s_content content-text">
        <h1 class="title_page">MEMBER AREA</h1>
        <div class="clear height-50"></div> <div class="clear height-5"></div>
        <div class="clear"></div>
        </div>
        <div class="row default box_outers_dashboardmember">
          <div class="col-md-3">
            <?php $this->renderPartial('_menu', array(
            )) ?>

          </div>
          <div class="col-md-9">



                            <div class="inside contn-gold">
                                <h2 class="m-0">History Order </h2>
                                <div class="clear height-10"></div> <div class="height-3"></div>
                                <div class="lines-grey"></div>
                                <div class="clear height-10"></div> <div class="height-3"></div>
<form action="" methof="get">
<div class="row">
  <div class="col-md-6">
    <select name="status" id="search-status" class="form-control">
      <option value="">All</option>
      <option value="Processing">Processing</option>
      <option value="Complete">Complete</option>
      <option value="Refund">Refund</option>
      <option value="Partial Refund">Partial Refund</option>
    </select>
    <!-- <input type="text" name="OrOrderProduct[search]" placeholder="masukkan kode transaksi di sini untuk mencari" class="form-control"> -->
  </div>
  <div class="col-md-6">
    <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</div>
</form>
<script type="text/javascript">
  $('#search-status').val('<?php echo $_GET['status'] ?>');
</script>
<div class="clear"></div>
<?php /*
<pre><?php print_r($result) ?></pre>
*/ ?>
<?php
$listData = $data->getData();
?>
<?php if (count($listData) > 0): ?>
  
                                <!-- Start in Content Riwayat Order -->
                                    <div class="table_wrapper table-full-ordervoucher order-tab">
<?php foreach ($listData as $key => $value): ?>

                                        <section class="paymentwrp">
                                          <?php /*
                                          
                                          */ ?>
                                            <?php /*
                                            <?php if ($value->order_status_id == 18): ?>
                                            <span class="fr">
                                                <a href="<?php echo CHtml::normalizeUrl(array('/member/indomog', 'nota'=>$value->invoice_prefix.'-'.substr('00000'.$value->invoice_no, -5))); ?>" class="btw mr2" target="background">
                                                    <span>Bayar Sekarang</span>
                                                </a>
                                            </span>
                                            <?php endif ?>
                                            */ ?>
                                            <span class="fwb mr10 ml5">Transaction Number <a href="<?php echo CHtml::normalizeUrl(array('vieworder', 'nota'=>$value->invoice_prefix.'-'.$value->invoice_no)); ?>" target="_blank"># <?php echo $value->invoice_prefix.'-'.$value->invoice_no ?></a></span>
                                            <!-- Dibuat pada <time class="num fs11 mr10"><?php echo $value->date_add ?></time> -->
                                            Total <span class="price fs16"><?php echo Cart::money($value->grand_total) ?></span>

                                            <!-- <div class="paymentinfo">
                                                <span class="fr mr10">Kedaluwarsa</span>
                                                <span class="ml5 lh2">Metode Pembayaran:
                                                <b>Saldo Kredit JuraganGame</b></span>
                                            </div> -->

                                            <table class="table orderA table-bordered">
                                                <tbody>
<?php
$itemList = OrOrderProduct::model()->findAll('order_id = :order_id', array(':order_id'=>$value->id));
?>
                                                    <?php foreach ($itemList as $k => $v): ?>
                                                    <tr>
                                                    <!-- <td class="pid">
                                                        <a title="Order 4178696" target="tukibox" href="#">C4178696</a>
                                                    </td> -->
                                                    <td>

                                                      <img align="left" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(50,50, '/images/product/'.$v->image , array('method' => 'resize', 'quality' => '90')) ?>" class="fl mr5">
                                                        <p class="lh14">
                                                            <a href="#">
                                                            <?php echo $v->name ?></a>
                                                        <br>
                                                        <small class="detl"><?php echo Cart::money($v->price) ?> 
                                                          (<?php echo number_format($v->qty) ?>)
                                                        </small>
                                                        </p>
                                                    </td>
                                                    <?php
                                                    // $statusOrder = array(
                                                    //   '0'=>'Belum Di Bayar',
                                                    //   '1'=>'Di Bayar',
                                                    //   '2'=>'Proses',
                                                    //   '3'=>'Selesai',
                                                    // )
                                                    ?>
                                                    <td class="qty"><?php echo OrOrderStatus::model()->findByPk($value->order_status_id)->name ?></td>
                                                    <td class="pid wsnw">
                                                        <a href="<?php echo CHtml::normalizeUrl(array('vieworder', 'nota'=>$value->invoice_prefix.'-'.$value->invoice_no)); ?>">View Detail</a>
                                                    </td>
                                                    </tr>
                                                    <?php endforeach ?>
                                                </tbody>
                                            </table>
                                        </section>
                                      <?php endforeach ?>
                                </div>
                                <!-- End in Content Riwayat Order -->
                                <?php $this->widget('CLinkPager', array(
                                    'pages' => $data->getPagination(),
                                    // 'itemCount'=>$result['totalPageSize'],
                                    // 'pageSize'=>10,
                                    // 'currentPage'=>$_GET['page']-1,
                                )) ?>
                                <!-- -------- End row data -------- -->
<?php else: ?>
  <?php if ($_GET['status'] != ''): ?>
  <h3>Maaf tidak ada data dengan status <?php echo $_GET['status'] ?></h3>
    
  <?php else: ?>
  <h3>Maaf tidak ada data</h3>
  <?php endif ?>
<?php endif ?>
                                <div class="height-20"></div>
                                <div class="clear"></div>
                            </div>


            
          </div>
          <!-- end col md 12 -->
        </div>
        <div class="clear height-20"></div>

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>







