<?php
$session = new CHttpSession;
$session->open();
?>
<div class="lefts_cont_member">
  <ul class="list-unstyled">
    <li <?php if ($this->id == 'member' AND $this->action->id == 'index'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">Manage Account</a></li>
    <?php if ($session['login_member']['type'] == 1): ?>
    <li <?php if ($this->id == 'memberclinic'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/memberclinic/index')); ?>">Manage Clinic</a></li>
    <?php endif ?>
    <li <?php if ($this->id == 'memberreview'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/memberreview/index')); ?>">Manage Reviews</a></li>
    <li <?php if ($this->id == 'membermessage'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/membermessage/index')); ?>">Manage Messages</a></li>
    <?php if ($session['login_member']['type'] == 1): ?>
    <li <?php if ($this->id == 'memberblog'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/memberblog/index')); ?>">Manage Blog</a></li>
    <?php endif ?>
    <li <?php if ($this->id == 'member' AND $this->action->id == 'order'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/member/order')); ?>">Manage Order</a></li>
  </ul>
  <div class="clear"></div>
</div>
<div class="clear"></div>
