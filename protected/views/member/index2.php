<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static page_member_s_content">
        <h1 class="title_page">MEMBER AREA</h1>
        <div class="clear height-50"></div> <div class="clear height-5"></div>
        <div class="clear"></div>

        <div class="row default box_outers_dashboardmember">
          <div class="col-md-3">
            <?php $this->renderPartial('_menu', array(
            )) ?>

          </div>
          <div class="col-md-9">
            <div class="rights_cont_member">
              <div class="row default">
                <div class="col-md-6">
                  <div class="box_default text-left">
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    // 'id'=>'daftar-form',
    // 'type'=>'horizontal',
  'enableClientValidation'=>false,
  'clientOptions'=>array(
    'validateOnSubmit'=>false,
  ),
)); ?>  
                    <h3 class="sub_title">Change your password</h3>
<?php if(Yii::app()->user->hasFlash('success')): ?>
    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>
<?php endif; ?>
                    <?php echo CHtml::errorSummary($model2, '', '', array('class'=>'alert alert-danger')); ?>
                    <div class="clear"></div>
                      <div class="form-group">
                        <?php echo CHtml::passwordField('MeMember2[passold]', $model2->passold, array('class'=>'form-control', 'placeholder'=>'Current Password')) ?>
                      </div>
                      <div class="form-group">
                        <?php echo CHtml::passwordField('MeMember2[pass]', $model2->pass, array('class'=>'form-control', 'placeholder'=>'New Password')) ?>
                      </div>
                      <div class="form-group">
                        <?php echo CHtml::passwordField('MeMember2[pass2]', $model2->pass2, array('class'=>'form-control', 'placeholder'=>'Confirm New Password')) ?>
                      </div>
                      <div class="form-group">
                        <button type="reset" class="btn btn-default btn_purple_member">CANCEL</button>&nbsp;&nbsp;&nbsp;
                        <button type="submit" class="btn btn-default btn_purple_member">SAVE CHANGES</button>
                      </div>
<?php $this->endWidget(); ?>
                  </div>    
                </div>
              </div>

              <div class="clear height-50"></div>

              <div class="row">
                <div class="col-md-12">
                  <div class="box_default text-left">
                    <h3 class="sub_title">Contact Info</h3>
                    <?php echo CHtml::errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>
                    <div class="clear"></div>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    // 'id'=>'daftar-form',
    // 'type'=>'horizontal',
  'enableClientValidation'=>false,
  'clientOptions'=>array(
    'validateOnSubmit'=>false,
  ),
)); ?>                      
                      <div class="row default">
                        <div class="col-md-6">
                          <div class="form-group">
                            <?php echo $form->textField($model, 'first_name', array('class'=>'form-control', 'placeholder'=>'First Name')) ?>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <?php echo $form->textField($model, 'last_name', array('class'=>'form-control', 'placeholder'=>'Last Name')) ?>
                          </div>
                        </div>
                      </div>

                      <div class="row default">
                        <div class="col-md-6">
                          <div class="form-group">
                            <?php echo $form->textField($model, 'email', array('class'=>'form-control', 'placeholder'=>'Email')) ?>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <?php echo $form->textField($model, 'phone', array('class'=>'form-control', 'placeholder'=>'Phone')) ?>
                          </div>
                        </div>
                      </div>

                      <div class="clear height-50"></div>
                      <h3 class="sub_title">Billing Address</h3>
                      <div class="clear"></div>
                      <div class="form-group">
                        <?php echo $form->textField($model, 'billing_address_line_1', array('class'=>'form-control', 'placeholder'=>'Address Line 1')) ?>
                      </div>
                      <div class="form-group">
                        <?php echo $form->textField($model, 'billing_address_line_2', array('class'=>'form-control', 'placeholder'=>'Address Line 2')) ?>
                      </div>
                      <div class="row default">
                        <div class="col-md-6">
                          <div class="form-group">
                            <?php echo $form->textField($model, 'billing_suburb', array('class'=>'form-control', 'placeholder'=>'Suburb')) ?>
                          </div>
                          <div class="form-group">
                            <?php echo $form->textField($model, 'billing_postcode', array('class'=>'form-control', 'placeholder'=>'Post Code')) ?>
                          </div>
                          <div class="form-group">
                            <?php echo $form->dropDownList($model, 'billing_state', array(
                                'Australian Capital Territory' => 'Australian Capital Territory',
                                'Jervis Bay Territory' => 'Jervis Bay Territory',
                                'New South Wales' => 'New South Wales',
                                'Northern Territory' => 'Northern Territory',
                                'Queensland' => 'Queensland',
                                'South Australia' => 'South Australia',
                                'Tasmania' => 'Tasmania',
                                'Victoria' => 'Victoria',
                                'Western Australia' => 'Western Australia',
                            ),array('class'=>'form-control', 'placeholder'=>'Post Code')) ?>
                          </div>

                        </div>
                        <div class="col-md-6"></div>
                      </div>

                      <div class="clear height-50"></div>
                      <h3 class="sub_title">Delivery Address</h3>
                      <div class="clear"></div>
                      <div class="form-group">
                        <?php echo $form->textField($model, 'delivery_address_line_1', array('class'=>'form-control', 'placeholder'=>'Address Line 1')) ?>
                      </div>
                      <div class="form-group">
                        <?php echo $form->textField($model, 'delivery_address_line_2', array('class'=>'form-control', 'placeholder'=>'Address Line 2')) ?>
                      </div>
                      <div class="row default">
                        <div class="col-md-6">
                          <div class="form-group">
                            <?php echo $form->textField($model, 'delivery_suburb', array('class'=>'form-control', 'placeholder'=>'Suburb')) ?>
                          </div>
                          <div class="form-group">
                            <?php echo $form->textField($model, 'delivery_postcode', array('class'=>'form-control', 'placeholder'=>'Post Code')) ?>
                          </div>
                          <div class="form-group">
                            <?php echo $form->dropDownList($model, 'delivery_state', array(
                                'Australian Capital Territory' => 'Australian Capital Territory',
                                'Jervis Bay Territory' => 'Jervis Bay Territory',
                                'New South Wales' => 'New South Wales',
                                'Northern Territory' => 'Northern Territory',
                                'Queensland' => 'Queensland',
                                'South Australia' => 'South Australia',
                                'Tasmania' => 'Tasmania',
                                'Victoria' => 'Victoria',
                                'Western Australia' => 'Western Australia',
                            ),array('class'=>'form-control', 'placeholder'=>'Post Code')) ?>
                          </div>

                        </div>
                        <div class="col-md-6"></div>
                      </div>
                      <div class="form-group">
                        <button type="reset" class="btn btn-default btn_purple_member">CANCEL</button>&nbsp;&nbsp;&nbsp;
                        <button type="submit" class="btn btn-default btn_purple_member">SAVE CHANGES</button>
                      </div>

                      <div class="clear"></div>
<?php $this->endWidget(); ?>
                  </div>
                </div>
              </div>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
          <!-- end col md 12 -->
        </div>
        <div class="clear height-20"></div>

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>