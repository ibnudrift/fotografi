<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static page_faq_content">
        <h1 class="title_page">EDUCATION</h1>
        <div class="clear height-50"></div>
        <div class="clear"></div>
        <div class="row">
          <div class="col-md-9 text-left">
            <div class="left_cont">
              <div class="mw906">
                <h4><?php echo nl2br($this->setting['education_faq_subtitle']) ?></h4>
                <div class="clear height-10"></div>
                <div class="pict_full"><img src="<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['education_faq_image']; ?>" alt="" class="img-responsive"></div>
                <?php echo $this->setting['education_faq_content'] ?>
                <form action="#" class="form-inline" method="get" id="form-topic">
                  <div class="form-group">
                    <label for="" class="control-label"><b>Topic</b></label>
                    <div class="d-inline box_select_f padding-left-15">
                      <select id="select-topic" class="form-control selectpicker" name="topic">
                        <option value="">All</option>
                        <option value="1">Trusted surgeons</option>
                        <option value="2">Surgery</option>
                        <option value="3">Travel</option>
                      </select>
                    </div>
                  </div>
                </form>
                <script type="text/javascript">
                $('#select-topic').change(function() {
                  $('#form-topic').submit();
                })
                $('#select-topic').val('<?php echo $_GET['topic'] ?>');
                </script>

                <div class="clear height-20"></div><div class="height-2"></div>
                <div class="lines-grey"></div>
                <div class="clear height-30"></div>

                 <div class="panel-group" id="accordion">
                    <?php foreach ($faq->getData() as $key => $value): ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel_<?php echo $key ?>"><?php echo $value->description->question ?></a>
                            </h4>
                        </div>
                        <div id="panel_<?php echo $key ?>" class="panel-collapse collapse <?php if ($key == 0): ?>in<?php endif ?>">
                            <div class="panel-body">
                                <?php echo $value->description->answer ?>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>

                </div>
                <div class="clear height-20"></div>
                <?php $this->widget('CLinkPager', array(
                    'pages' => $faq->getPagination(),
                    'header' => '',
                    'htmlOptions' => array('class' => 'pagination'),
                )) ?>

                <!-- end accordion -->
                <div class="clear height-20"></div><div class="height-2"></div>
                <div class="lines-grey"></div>
                <div class="clear height-40"></div>

                <div class="pict_full"><a href="<?php echo CHtml::normalizeUrl(array('/contact/index')); ?>"><img src="<?php echo $this->assetBaseurl ?>pict-faq-education-bottom.png" alt="" class="img-responsive"></a></div>


                <div class="clear"></div>
              </div>
            </div>

          </div>
          <div class="col-md-3 text-left">
            <div class="right_cont">
              <div class="padding-left-25">
                  <span class="sub_page_title">EDUCATION</span>
              </div>
              <div class="clear"></div>
              <?php echo $this->renderPartial('//education/_right_menu', array('active'=>'faq')); ?>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
        </div>

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>
<script type="text/javascript">
  $(function(){
      var iconOpen = 'icon_minus',
        iconClose = 'icon_plus';
        // icons 

    var $active = $('#accordion .panel-collapse.in').prev().addClass('active');
    $active.find('a').prepend('<i class="icons icon_minus"></i>');
    $('#accordion .panel-heading').not($active).find('a').prepend('<i class="icons icon_plus"></i>');
    $('#accordion').on('show.bs.collapse', function (e) {
      $('#accordion .panel-heading.active').removeClass('active').find('.icons').toggleClass('icon_plus icon_minus');
      $(e.target).prev().addClass('active').find('.icons').toggleClass('icon_plus icon_minus');
    });
    // end function accordion

  });
</script>
<style type="text/css">
  .panel-default .panel-body p{
    margin-bottom: 0 !important;
  }
</style>