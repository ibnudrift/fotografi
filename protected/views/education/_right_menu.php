<div class="right_sub_menu">
    <ul class="list-unstyled">
      <li class="dropdown <?php if ($active == 'education'): ?>active<?php endif ?>"><a  class="dropdown-toggle" data-toggle="dropdown" href="<?php echo CHtml::normalizeUrl(array('index')); ?>"><?php echo $this->setting['education_3d_title'] ?></a>
		<ul class="dropdown-menu">
			<li><a href="<?php echo CHtml::normalizeUrl(array('/education/sub', 'name' => 'face' )); ?>">Face</a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/education/sub', 'name' => 'breast' )); ?>">Breast</a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/education/sub', 'name' => 'body' )); ?>">Body</a></li>
		</ul>
      </li>
			
      <li <?php if ($active == 'faq'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('faq')); ?>"><?php echo $this->setting['education_faq_title'] ?></a></li>
    </ul>
    <div class="clear"></div>
  </div>