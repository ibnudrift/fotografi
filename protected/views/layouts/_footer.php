<footer class="foots">
    <div class="prelatife container_ft">
        <div class="inner_foot">
            <div class="row default">
                <div class="col-md-3">
                    <div class="lgo_footer"><a href="#"><img src="<?php echo $this->assetBaseurl.'news/lgo-footers.jpg'; ?>" alt="" class="img-responsive"></a></div>
                    <div class="clear height-40"></div>
                    <div class="boxs_filter_ft">
                        <form action="#">
                            <label for="">Cari Fotografer di</label>
                            <div class="clear"></div>
                            <select name="" id="" class="form-control">
                                <option value="">Semua Kota</option>
                                <option value="surabaya">Surabaya</option>
                            </select>
                        </form>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="row default">
                        <div class="col-md-3">
                            <div class="bloc_menuft">
                                <span class="tops_ft">Browse Fotografer.id</span>
                                <ul class="list-unstyled">
                                    <li><a href="#">Tentang Fotografer.id</a></li>
                                    <li><a href="#">Mencari Fotografer</a></li>
                                    <li><a href="#">Spesialisasi</a></li>
                                    <li><a href="#">Menjadi Fotografer Kami</a></li>
                                    <li><a href="#">Artikel & Blog</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="bloc_menuft">
                                        <span class="tops_ft">Spesialisasi Fotografer</span>
                                        <ul class="list-unstyled">
                                            <li><a href="#">Semua Spesialisasi</a></li>
                                            <li><a href="#">Wedding & Family</a></li>
                                            <li><a href="#">Profil & Perusahaan</a></li>
                                            <li><a href="#">Architecture & Interior</a></li>
                                            <li><a href="#">Makanan, Minuman & Produk</a></li>
                                        </ul>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="bloc_menuft">
                                        <span class="tops_ft">&nbsp;</span>
                                        <ul class="list-unstyled">
                                            <li><a href="#">Drone</a></li>
                                            <li><a href="#">Sport & Event</a></li>
                                            <li><a href="#">Seni & Pemandangan</a></li>
                                            <li><a href="#">Video</a></li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="bloc_menuft">
                                <span class="tops_ft">Informasi Fotografer.id</span>
                                <ul class="list-unstyled">
                                    <li><a href="#">Kontak</a></li>
                                    <li><a href="#">Sign In</a></li>
                                    <li><a href="#">Sign Up Fotografer</a></li>
                                    <li><a href="#">Sign Up Pelanggan</a></li>
                                </ul>
                            </div>
                            <div class="clear height-30"></div>
                            <div class="socil_med">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>

            <div class="clear height-30"></div>
            <div class="lines-white"></div>
            <div class="clear height-30"></div>
            
            <div class="bottoms_ftcopyrights">
                <div class="row">
                    <div class="col-md-6">
                        <div class="t-copyrights">
                            <p>&copy; <?php echo date("Y"); ?> Fotografer Indonesia.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="t-copyrights text-right">
                            <p><a href="#">Sitemap</a>&nbsp;&nbsp;|&nbsp;&nbsp;Design by <a target="_blank" title="Website Design Surabaya" href="https://www.markdesign.net/">Mark Design</a></p>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>

            <div class="clear"></div>
        </div>
    </div>
</footer>