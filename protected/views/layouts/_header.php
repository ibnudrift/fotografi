<?php // echo $this->action->id; ?>
<header class="head">
    <div class="backs_respons prelatife">
        <div class="visible-lg">
            <div class="logo_header_web">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
                    <img src="<?php echo $this->assetBaseurl ?>logo_header_trusted_surgeons.jpg" alt="" class="img-responsive">
                </a>
            </div>
            <div class="d-inline v-top right_header">
                <div class="top_header"></div>
                <div class="bottom_header">
                    <div class="row">
                        <div class="col-md-10">
                            <div class="top-menu">
                                <ul class="list-inline">
                                    <li><a href="#">Tentang Fotografer.id</a></li>
                                    <li><a href="#">Mencari Fotografer</a></li>
                                    <li><a href="#">Spesialis</a></li>
                                    <li><a href="#">Menjadi Fotografer Kami</a></li>
                                    <li><a href="#">Artikel & Blog</a></li>
                                    <li><a href="#">Kontak</a></li>
                                </ul>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="text-right sMenu_header_right">
                                <ul class="list-inline">
                                    <?php
                                    $session = new CHttpSession;
                                    $session->open();
                                    if ( ! isset($session['login_member'])):
                                    ?>
                                        <li><a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">SIGN IN</a></li>
                                        <li><a href="<?php echo CHtml::normalizeUrl(array('/member/signup')); ?>">SIGN UP</a></li>
                                    <?php else: ?>
                                    <li><a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>"><i class="fa fa-user fa-flip-horizontal" aria-hidden="true"></i> &nbsp;My Dash</a></li>
                                    <li><a href="<?php echo CHtml::norcccmalizeUrl(array('/member/logout')); ?>"><i class="fa fa-sign-out fa-flip-horizontal" aria-hidden="true"></i> &nbsp;Log Out</a></li>
                                    <?php endif ?>
                                </ul>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>

                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <!-- <div class="hidden-lg">
            <div class="row default">
                <div class="col-md-4 col-sm-4">
                    <div class="block top_left_header2 text-left">
                        <div class="d-inline v-top padding-right-15">
                            <a href="javascript:;" class="showmenu_barresponsive2"><i class="fa fa-user"></i></a>
                        </div>
                        <div class="d-inline v-top">
                            <a href="#"><i class="fa fa-shopping-cart"></i></a>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="logo_header_web">
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
                            <img src="<?php echo $this->assetBaseurl ?>logo_header_trusted_surgeons.jpg" alt="" class="img-responsive center-block">
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="block top_right_header2 text-right">
                        <a href="javascript:;" class="showmenu_barresponsive"><i class="fa fa-bars"></i></a>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="clear clearfix"></div>
    </div>

<script type="text/javascript">
    $(function(){
        // searching header
        $('.search-form .form-group input').live('click', function() {
            $(this).parent().addClass('fullw'); 
        });
    });
</script>

</header>

<?php echo $this->renderPartial('//layouts/_box_searchs', array()); ?>