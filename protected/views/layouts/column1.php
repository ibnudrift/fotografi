<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<body class="">
	<div class="full_hmes h100per">
		<?php echo $this->renderPartial('//layouts/_header', array()); ?>
		<?php echo $content ?>
		<?php echo $this->renderPartial('//layouts/_footer', array()); ?>
		<div class="clear"></div>
	</div>
</body>
<?php $this->endContent(); ?>