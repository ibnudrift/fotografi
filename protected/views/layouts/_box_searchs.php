<section class="outers_box_search">
	<div class="prelatife container">
		<div class="inner_middle">
			<div class="tops_title">
				<h3>Cari Fotografer Terbaik Untuk Anda Di Indonesia</h3>
			</div>
			<div class="clear height-15"></div>
			<div class="box_form">

				<form class="form-inline" method="post" action="#">
				  <div class="row default">
				  	<div class="col-md-9">
						  <div class="form-group">
						    <label class="sr-only" for="exampleInputAmount">kota</label>
						    <div class="input-group">
						      <div class="input-group-addon"><i class="fa fa-search"></i></div>
						      <input type="text" class="form-control" id="exampleInputAmount" placeholder="Ketik kota yang anda inginkan">
						    </div>
						  </div>
				  	</div>
				  	<div class="col-md-3">
					  <div class="form-group">
					    <label class="sr-only" for="exampleInputAmount">spesialisasi</label>
					      <select name="" id="" class="form-control">
						  	<option value="">Semua Spesialisasi</option>
						  	<option value="">Spesialis 1</option>
						  </select>
					  </div>
				  	</div>
				  </div>
				</form>

			</div>
			<div class="clear height-15"></div>
			<div class="info_bt">
				<p>Atau <a target="_blank" href="#">klik di sini</a> untuk melihat semua Fotografer terdaftar</p>
			</div>
		</div>
	</div>
</section>