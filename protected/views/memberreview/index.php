<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="inside  s sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static page_member_s_content">
        <h1 class="title_page">MEMBER AREA</h1>
        <div class="clear height-50"></div> <div class="clear height-5"></div>
        <div class="clear"></div>

        <div class="row default box_outers_dashboardmember">
          <div class="col-md-3">
            <?php $this->renderPartial('/member/_menu', array(
            )) ?>
          </div>
          <div class="col-md-9">
            <div class="rights_cont_member">
              <!-- start review cont -->
              <?php if(Yii::app()->user->hasFlash('success')): ?>
                  <?php $this->widget('bootstrap.widgets.TbAlert', array(
                      'alerts'=>array('success'),
                  )); ?>
              <?php endif; ?>
              <?php if ($dataReview->getTotalItemCount() > 0): ?>
              <div class="list_box_default_member_white">
              <?php foreach ($dataReview->getData() as $key => $value): ?>
                <div class="items prelatife">
                  <div class="padding">
                    <div class="row default">
                      <?php if ($this->memberData['type'] == 0): ?>
                        
                      <div class="col-md-3">
                        <div class="items back-transp">
                          <div class="picture doctor_p pic_doctor">
                            <a href="#">
                              <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(127,127, '/images/doctor/'.$value->doctor->photo , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                            </a>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-9">
                        <div class="desc">
                          <div class="doctor_info">
                            <div class="row">
                              <div class="col-md-12">
                                <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail', 'id'=>$value->doctor_id)); ?>">
                                <span class="names" style="margin: 0;"><?php echo $value->doctor->name ?></span>
                                </a>
                                <p>
                                    <?php if ($value->doctor->n_review > 0): ?>
                                        <?php echo DoctorReviewCategory::model()->createStar($value->doctor->rating) ?> &nbsp; <?php echo $value->doctor->n_review ?> reviews <br>
                                    <?php endif ?>
                                    <?php echo $value->doctor->clinics[0]->address_1 ?> <?php echo $value->doctor->clinics[0]->address_2 ?> <br>
                                    <?php if ($value->doctor->clinics[0]->phone != ''): ?>
                                            Phone: <?php echo $value->doctor->clinics[0]->phone ?>
                                    <?php endif ?>
                                </p>
                                <div class="height-20"></div>

                    <?php
                    $criteria = new CDbCriteria;
                    $criteria->select = 'SUM(`value`)/COUNT(`value`) as `value`';
                    $criteria->addCondition('review_id = :review_id');
                    $criteria->params[':review_id'] = $value->id;
                    $criteria->group = 'review_id';
                    $dataReviewValue = DoctorCategoryReview::model()->find($criteria);
                    ?>
                                <div class="row">
                                  <div class="col-md-8">
                                    <span class="b_star">
                                      <?php echo DoctorReviewCategory::model()->createStar($dataReviewValue->value) ?>
                                      &nbsp; <?php echo $value->name ?>
                                    </span>
                                    <?php echo nl2br($value->comment) ?>
                                  </div>
                                  <div class="col-md-4">
                                    <h4 class="titles_s text-right">
                                    <?php if ($value->status == 0): ?>
                                      (Pending)
                                    <?php else: ?>
                                      <?php if ($value->featured == 1): ?>
                                        (Approved | Featured)
                                      <?php else: ?>
                                        (Approved)
                                      <?php endif ?>
                                    <?php endif ?>
                                    </h4>
                                  </div>
                                </div>

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <?php else: ?>
                      <div class="col-md-5">
                    <?php
                    $criteria = new CDbCriteria;
                    $criteria->select = 'SUM(`value`)/COUNT(`value`) as `value`';
                    $criteria->addCondition('review_id = :review_id');
                    $criteria->params[':review_id'] = $value->id;
                    $criteria->group = 'review_id';
                    $dataReviewValue = DoctorCategoryReview::model()->find($criteria);
                    ?>
                        <span class="b_star">
                          <?php echo DoctorReviewCategory::model()->createStar($dataReviewValue->value) ?>
                          &nbsp; <?php echo $value->name ?>
                        </span>
                        <div class="clear"></div>
                        <h4 class="titles_s"><?php echo $value->name ?><?php if ($value->address != ''): ?>, <?php echo $value->address ?><?php endif ?>
                        <?php if ($value->status == 0): ?>
                          (Pending)
                        <?php else: ?>
                          <?php if ($value->featured == 1): ?>
                            (Approved | Featured)
                          <?php else: ?>
                            (Approved)
                          <?php endif ?>
                        <?php endif ?>
                        </h4>
                        <div class="clear"></div>
                        <span class="dates"><?php echo Common::time_elapsed_string($value->date_input); ?></span>

                        <p><?php echo nl2br($value->comment) ?></p>
                      </div>
                      <?php endif ?>
                    </div>
                  </div>
                    <div class="height-50"></div>
                  <?php if ($this->memberData['type'] == 1): ?>
                   <div class="blocks_bottom_d">
                      <div class="padding h143 pt0">
                        <div class="row default prelatife h143">
                            <div class="text-center">
                                <!-- <span class="sub_t center-block"><b>Message</b></span> -->
                            </div>
                          <div class="col-md-10">
                            <!-- <p><?php echo nl2br($value->comment) ?></p> -->
                          </div>
                          <?php if ($this->memberData['type'] == 1): ?>
                            
                          <div class="col-md-2 border-left h143">
                            <div class="buttons_d">
                              <a class="btn btn-default btn-link" href="<?php echo CHtml::normalizeUrl(array('delete', 'id'=>$value->id)); ?>" role="button"><i class="fa fa-times"></i>&nbsp;&nbsp;Delete</a>
                              <div class="clear height-3"></div>
                              <?php if ($value->featured == 1): ?>
                              <a class="btn btn-default btn-link" href="<?php echo CHtml::normalizeUrl(array('del_featured', 'id'=>$value->id)); ?>" role="button"><i class="fa fa-quote-right"></i>&nbsp;&nbsp;Delete Featured</a>
                              <?php else: ?>
                              <a class="btn btn-default btn-link" href="<?php echo CHtml::normalizeUrl(array('featured', 'id'=>$value->id)); ?>" role="button"><i class="fa fa-quote-right"></i>&nbsp;&nbsp;Feature this review</a>
                              <?php endif ?>
                              <div class="clear height-3"></div>
                              <?php /*
                              <a class="btn btn-default btn-link" href="#" role="button"><i class="fa fa-comments"></i>&nbsp;&nbsp;Reply</a>
                              <div class="clear height-3"></div>
                              */ ?>
                              <?php if ($value->status == 1): ?>
                              <a class="btn btn-default btn-link" href="<?php echo CHtml::normalizeUrl(array('hide', 'id'=>$value->id)); ?>" role="button"><i class="fa fa-check"></i>&nbsp;&nbsp;Hidden</a>
                              <?php else: ?>
                              <a class="btn btn-default btn-link" href="<?php echo CHtml::normalizeUrl(array('approve', 'id'=>$value->id)); ?>" role="button"><i class="fa fa-check"></i>&nbsp;&nbsp;Approve</a>
                              <?php endif ?>
                            </div>
                            <div class="clear"></div>
                          </div>
                          <?php else: ?>
                            
                          <?php endif ?>
                        </div>

                        <div class="clear"></div>
                      </div>
                    </div>
                    <?php endif ?>
                </div>
                <?php endforeach ?>


                <div class="clear"></div>
              </div>
              <?php $this->widget('CLinkPager', array(
                  'pages' => $dataReview->getPagination(),
                  'header' => '',
                  'htmlOptions' => array('class' => 'pagination'),
              )) ?>
              <?php else: ?>
              <h3>No data review</h3>
              <?php endif ?>
              <!-- end review cont -->

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
          <!-- end col md 12 -->
        </div>
        <div class="clear height-20"></div>

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>