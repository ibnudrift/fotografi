<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static procedures">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>

      <div class="content-text insides_static">
        <h1 class="title_page">PROCEDURES</h1>
        <div class="clear height-50"></div>
        <div class="clear"></div>

        <div class="wrapper-cont-procedures">
          <div class="top-filter">
            <div class="search_surgeons_doctor-procedures">
              <span>Already know your stuff?</span>
              <div class="clear"></div>
              <a href="#" class="btn btns_search_stag_procedureDoctor">CONTINUE TO SEARCH</a>
            </div>
          </div>
          <div class="clear height-50"></div>
          <div class="height-50"></div>

          <div class="block_procedures_con_details">
            <div class="text-center">
              <h3 class="title"><?php echo $data->description->question ?></h3>
            </div>
            <div class="clear height-50"></div>
            <div class="height-10"></div>
            <?php echo $data->description->answer ?>
            <div class="clear"></div>            
          </div>
          <!-- end detail procedures -->

          <div class="clear"></div>
        </div>
        <div class="info_bottom_proced_details">
            <div class="row">
              <div class="col-md-3">
                <div class="pict"><img src="<?php echo $this->assetBaseurl ?>plastic-society.png" alt="" class="img-responsive"></div>
              </div>
              <div class="col-md-9">
                <p>Electing surgery can be a daunting experience, to ensure you select a highly qualified and experienced surgeon look for the ASPS logo. The logo demonstrates the surgeon is a member of the Australian Society of Plastic Surgeons. To accomplish this membership the surgeon achieved post graduate qualifications in plastic surgery and has a minimum of 12 years medical and surgical education. ASPS members are Specialist Plastic Surgeons promoting quality, safety and expertise.</p>
              </div>
            </div>
            <div class="clear"></div>
          </div>

        

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>
