<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static procedures">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      
      <!-- <div class="pict_full illustration_picture"><img src="<?php // echo $this->assetBaseurl ?>pict_top_procedures.jpg" alt="" class="img-responsive"></div>
      <div class="clear height-50"></div> -->

      <div class="content-text insides_static">
        <h1 class="title_page">PROCEDURES</h1>
        <div class="clear height-50"></div>
        <div class="clear"></div>
        <div class="row">
          <div class="col-md-9 text-left">
            <div class="left_cont">
              <div class="mw906">
                <?php
                  $urls = '';
                  $name_proc = isset($_GET['name'])? $_GET['name'] : '';
                  // echo $name_proc; exit;
                  if ($name_proc == 'face') {
                    echo "<h4>".nl2br($this->setting['procedures_face_title'])."</h4>";
                  }elseif ($name_proc == 'body') {
                    echo "<h4>".nl2br($this->setting['procedures_body_title'])."</h4>";
                  } else {
                    echo "<h4>".nl2br($this->setting['procedures_breast_title'])."</h4>";
                  }
                  
                ?>
                <div class="clear height-10"></div>
                
                <?php
                  $urls = '';
                  $name_proc = isset($_GET['name'])? $_GET['name'] : '';
                  if ($name_proc == 'face') {
                    $urls = 'https://content.understand.com/tsaustralia.menu?CatalogID=4d07e4b8-dfd0-4bf2-bea8-3aaf1af34237&SingleCatalog=1';
                  }elseif ($name_proc == 'body') {
                    $urls = 'https://content.understand.com/tsaustralia.menu?CatalogID=980e7aef-30f9-44ef-aba8-6a8d719fe8fc&SingleCatalog=1';
                  } else {
                    $urls = 'https://content.understand.com/tsaustralia.menu?CatalogID=b4d9716a-d5ff-4ec9-8d1a-931ad6997c9e&SingleCatalog=1';
                  }
                  
                ?>
                <div class="box_slider_step6">
                    <div class="pict_full">
                        <style>
                        /*.embed-container { position: relative; padding-bottom: 61.45%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }*/
                        </style>
                        <!-- <div class='embed-container'><iframe src='https://content.understand.com/tsaustralia.menu?CatalogID=b4d9716a-d5ff-4ec9-8d1a-931ad6997c9e&SingleCatalog=1' style='border:0'></iframe></div> -->
                        <iframe src="<?php echo $urls; ?>" border="0" frameborder="0" scrolling="no" width="909" style="min-width:100%; width:100%;" height="557"></iframe>
                    </div>
                  <div class="clear"></div>
                </div>

                <div class="clear height-30"></div>
                <div class="lines-grey" style="background-color: #a09f9f;"></div>
                <div class="clear height-30"></div>

                <div class="page_faq_content">
                   <div class="panel-group" id="accordion">

                      <?php foreach ($procedure->getData() as $key => $value): ?>
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel_<?php echo $key ?>"><?php echo $value->description->question ?></a>
                              </h4>
                          </div>
                          <div id="panel_<?php echo $key ?>" class="panel-collapse collapse <?php if ($key == 0): ?>in<?php endif ?>">
                              <div class="panel-body">
                                  <?php echo $value->description->answer ?>
                              </div>
                          </div>
                      </div>
                      <?php endforeach ?>
                  </div>
                  <div class="clear height-10"></div>

                  <!-- Checking upload -->
                  <?php $this->widget('CLinkPager', array(
                      'pages' => $procedure->getPagination(),
                      'header'=>'', 
                      'htmlOptions' => array('class'=>'pagination'),
                  )) ?>

                  <div class="clear"></div>
                </div>
                <div class="clear height-20"></div>
                <?php /*
                <div class="box_donwload_procedures hide hidden">
                  <div class="titles"><b>DOWNLOADS</b></div>
                  <div class="clear height-20"></div>

                  <div class="list">
                    <div class="items">
                      <div class="row">
                        <div class="col-md-10 text-left">
                          <span>Download Breast Augmentation - Saline Implants PDF</span>
                        </div>
                        <div class="col-md-2 text-right">
                          <div class="fright padding-right-5">
                            <a href="#"><img src="<?php echo $this->assetBaseurl ?>icons-pdf.png" alt="" class="img-responsive"></a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="items">
                      <div class="row">
                        <div class="col-md-10 text-left">
                          <span>Download Breast Augmentation - Silicone Implants PDF</span>
                        </div>
                        <div class="col-md-2 text-right">
                          <div class="fright padding-right-5">
                            <a href="#"><img src="<?php echo $this->assetBaseurl ?>icons-pdf.png" alt="" class="img-responsive"></a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="items">
                      <div class="row">
                        <div class="col-md-10 text-left">
                          <span>Download Breast Lift PDF</span>
                        </div>
                        <div class="col-md-2 text-right">
                          <div class="fright padding-right-5">
                            <a href="#"><img src="<?php echo $this->assetBaseurl ?>icons-pdf.png" alt="" class="img-responsive"></a>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                  <div class="clear"></div>
                </div>
                <div class="clear height-20"></div>*/
                 ?>                

                <div class="clear"></div>
              </div>
            </div>

          </div>
          <div class="col-md-3 text-left">
            <div class="right_cont">
              <div class="padding-left-25">
                  <span class="sub_page_title">PROCEDURES</span>
              </div>
              <div class="clear"></div>
              <div class="right_sub_menu">
                <ul class="list-unstyled">
                  <li <?php if ($name_proc == 'breast'): ?>class="active"<?php endif ?> ><a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'name' => 'breast')); ?>">Breast</a></li>
                  <li <?php if ($name_proc == 'body'): ?>class="active"<?php endif ?> ><a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'name' => 'body' )); ?>">Body</a></li>
                  <li <?php if ($name_proc == 'face'): ?>class="active"<?php endif ?> ><a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'name' => 'face')); ?>">Face</a></li>
                </ul>
                <div class="clear"></div>
              </div>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<script type="text/javascript">
  $(function(){
      var iconOpen = 'icon_minus',
        iconClose = 'icon_plus';
        // icons 

    var $active = $('#accordion .panel-collapse.in').prev().addClass('active');
    $active.find('a').prepend('<i class="icons icon_minus"></i>');
    $('#accordion .panel-heading').not($active).find('a').prepend('<i class="icons icon_plus"></i>');
    $('#accordion').on('show.bs.collapse', function (e) {
      $('#accordion .panel-heading.active').removeClass('active').find('.icons').toggleClass('icon_plus icon_minus');
      $(e.target).prev().addClass('active').find('.icons').toggleClass('icon_plus icon_minus');
    });
    // end function accordion

  });
</script>