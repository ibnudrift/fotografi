<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="inside  s sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static page_member_s_content">
        <h1 class="title_page">MEMBER AREA</h1>
        <div class="clear height-50"></div> <div class="clear height-5"></div>
        <div class="clear"></div>

        <div class="row default box_outers_dashboardmember">
          <div class="col-md-3">
            <?php $this->renderPartial('/member/_menu', array(
            )) ?>
          </div>
          <div class="col-md-9">
            <div class="rights_cont_member">
<?php
$dataClaim = DoctorClaim::model()->find('member_id = :member_id AND status = 0', array(':member_id'=>$this->memberData['id']));
?>
<?php if ($dataClaim === null): ?>
            <?php if(Yii::app()->user->hasFlash('danger')): ?>
              <div class="padding-left-50 padding-right-50">
                <?php $this->widget('bootstrap.widgets.TbAlert', array(
                    'htmlOptions' => array('class'=>'alert-success'),
                    'alerts'=>array('danger'),
                )); ?>
                <div class="clear height-20"></div>
              </div>
            <?php endif; ?>
            <?php if(Yii::app()->user->hasFlash('success')): ?>
              <div class="padding-left-50 padding-right-50">
                <?php $this->widget('bootstrap.widgets.TbAlert', array(
                    'htmlOptions' => array('class'=>'alert-success'),
                    'alerts'=>array('success'),
                )); ?>
                <div class="clear height-20"></div>
              </div>
            <?php endif; ?>

              <!-- Start box manages_clinic -->
              <div class="outers_p_manageclinic">
                <div class="tops">
                  <div class="picture_ill pict_full"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(983,283, '/images/doctor/'.$model->cover , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></div>
                  <div class="back-white prelatife h55 fdesc_top">
                    <div class="insides prelatife">
                      <div class="cont_abs_tops">
                        <div class="row">
                          <div class="col-md-2">
                            <div class="pic_doctor">
                              <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(127,127, '/images/doctor/'.$model->photo , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="">
                            </div>
                          </div>
                          <div class="col-md-10">
                            <div class="padding-left-25">
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    // 'id'=>'daftar-form',
    // 'type'=>'horizontal',
  'action'=>array('index'),
  'enableClientValidation'=>false,
  'clientOptions'=>array(
    'validateOnSubmit'=>false,
  ),
)); ?>
                                <div class="form-group">
                                  <div class="mw395">
                                    <?php echo $form->textField($model,'name',array('class'=>'form-control', 'placeholder'=>'Doctor`s Name')); ?>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="mw395">
                                    <?php echo $form->textField($model,'certification',array('class'=>'form-control', 'placeholder'=>'Certification')); ?>
                                  </div>
                                </div>
                                <div class="clear height-20"></div>

                                <div class="form-group">
                                  <div class="row">
                                    <?php if ($model->status == 0): ?>
                                      
                                    <div class="col-md-6">
                                      <button onclick="window.location = '<?php echo CHtml::normalizeUrl(array('publish')); ?>';" class="btn_purple_member btn btn-default defaults">Publish Profile</button>
                                    </div>
                                    <?php else: ?>
                                    <div class="col-md-6">
                                      <button onclick="window.location = '<?php echo CHtml::normalizeUrl(array('unpublish')); ?>';" class="btn_purple_member btn btn-default defaults">Unpublish Profile</button>
                                    </div>
                                    <?php endif ?>
                                    <div class="col-md-6">
                                      <div class="fright">
                                        <button type="submit" class="btn_purple_member btn btn-default defaults">Edit Name</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
<?php $this->endWidget(); ?>
                              <!-- end forms -->
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- end cont abs top -->
                      <div class="clear"></div>
                    </div>
                  </div>
                  <!-- end backwhite -->
                  <div class="clear"></div>
                </div>
                <!-- end top -->
                <div class="clear height-35"></div>

                <div class="middles">
                  <!-- start end box middles -->
                  <div class="box_clinic_tab_cont">
                    <div>
                      <?php $this->renderPartial('_menu', array(
                      )) ?>


                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="clinic_info">
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    // 'id'=>'daftar-form',
    // 'type'=>'horizontal',
  'action'=>array('index'),
  'enableClientValidation'=>false,
  'clientOptions'=>array(
    'validateOnSubmit'=>false,
  ),
  'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
  <div class="box_default prelatife">
    <h3 class="sub_title">Photo and Cover</h3>
    <div class="clear"></div>

    <div class="row memberc">
      <div class="col-md-6">
        <div class="form-group">
          <label for="">Photo</label>
          <?php echo $form->fileField($model,'photo',array('class'=>'')); ?>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="">Cover</label>
          <?php echo $form->fileField($model,'cover',array('class'=>'')); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="clear height-15"></div>
  <div class="row">
    <div class="col-md-12">
      <button class="btn_purple_member defaults btn btn-default">SUBMIT</button>
    </div>
  </div>
  
<?php $this->endWidget(); ?>
<div class="height-20"></div>
<?php
$criteria = new CDbCriteria;
$criteria->addCondition('doctor_id = :doctor_id');
$criteria->params[':doctor_id'] = $model->id;
$dataClinic = new CActiveDataProvider('DoctorClinic', array(
  'criteria'=>$criteria,
));
?>
  <div class="box_default prelatife">
    <h3 class="sub_title">Data Contact Information</h3>
    <div class="clear"></div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
  'id'=>'promotion-grid',
  'dataProvider'=>$dataClinic,
  // 'filter'=>$model,
  'enableSorting'=>false,
  'summaryText'=>false,
  'type'=>'bordered',
  'columns'=>array(
    'name',
    'phone',
    'website',
    'suburb',
    // array(
    //  'name'=>'last_update_by',
    // ),
    // array(
    //  'name'=>'active',
    //  'filter'=>array(
    //    '0'=>'Non Active',
    //    '1'=>'Active',
    //  ),
    //  'type'=>'raw',
    //  'value'=>'($data->active == "1") ? "Show" : "Hide"',
    // ),
    array(
      'class'=>'bootstrap.widgets.TbButtonColumn',
      'template'=>'{update} {delete}',
      'deleteButtonUrl'=>'CHtml::normalizeUrl(array("index", "delete"=>$data->id))',
      'updateButtonUrl'=>'CHtml::normalizeUrl(array("index", "update"=>$data->id))',
    ),
  ),
)); ?>
  </div>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    // 'id'=>'daftar-form',
    // 'type'=>'horizontal',
  'enableClientValidation'=>false,
  'clientOptions'=>array(
    'validateOnSubmit'=>false,
  ),
)); ?>

  <div class="box_default prelatife">
    <h3 class="sub_title">Clinic Contact Information</h3>
    <div class="clear"></div>

    <div class="row memberc">
      <div class="col-md-6">
        <div class="form-group">
          <?php echo $form->textField($modelClinic,'name',array('class'=>'form-control', 'placeholder'=>'Clinic`s Name')); ?>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <?php echo $form->textField($modelClinic,'website',array('class'=>'form-control', 'placeholder'=>'Website')); ?>
        </div>
      </div>
    </div>
    <div class="row memberc">
      <div class="col-md-6">
        <div class="form-group">
          <?php echo $form->textField($modelClinic,'phone',array('class'=>'form-control', 'placeholder'=>'Phone')); ?>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <?php echo $form->textField($modelClinic,'fax',array('class'=>'form-control', 'placeholder'=>'Fax')); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- <div class="clear height-35"></div> -->

  <div class="box_default prelatife">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <?php echo $form->textField($modelClinic,'address_1',array('class'=>'form-control', 'placeholder'=>'Address line 1')); ?>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <?php echo $form->textField($modelClinic,'address_2',array('class'=>'form-control', 'placeholder'=>'Address line 2')); ?>
        </div>
      </div>
    </div>
    <div class="row memberc">
      <div class="col-md-4">
        <div class="form-group">
          <?php echo $form->textField($modelClinic,'suburb',array('class'=>'form-control', 'placeholder'=>'Suburb')); ?>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <?php echo $form->textField($modelClinic,'postcode',array('class'=>'form-control', 'placeholder'=>'Postcode')); ?>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <?php echo $form->textField($modelClinic,'state',array('class'=>'form-control', 'placeholder'=>'State')); ?>
        </div>
      </div>
    </div>
    <?php echo $form->hiddenField($modelClinic,'latitude',array('class'=>'form-control', 'placeholder'=>'Ltitude')); ?>
    <?php echo $form->hiddenField($modelClinic,'longitude',array('class'=>'form-control', 'placeholder'=>'Longitude')); ?>
  </div>


<!-- <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js"></script> -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnVYV9PU2hDS4GMEJ_TZ2Hy-zy1iXfQX0&callback=initialize"></script>
<div id="map_canvas" style="width:100%; height:400px;"></div>
<div class="height-20"></div>
<script type="text/javascript">
var map;
function initialize() {
  <?php if ($modelClinic->scenario == 'update'): ?>
  var myLatlng = new google.maps.LatLng(<?php echo $modelClinic->latitude ?>,<?php echo $modelClinic->longitude ?>);
  <?php else: ?>
  var myLatlng = new google.maps.LatLng(-24.90636703004104,133.63494856249997);
  <?php endif ?>


  var myOptions = {
     zoom: 4,
     center: myLatlng,
     mapTypeId: google.maps.MapTypeId.ROADMAP
     }
  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions); 

  var marker = new google.maps.Marker({
    draggable: true,
    position: myLatlng, 
    map: map,
  title: "Your location"
  });

  google.maps.event.addListener(marker, 'dragend', function (event) {
      document.getElementById("DoctorClinic_latitude").value = this.getPosition().lat();
      document.getElementById("DoctorClinic_longitude").value = this.getPosition().lng();
  });

  // if (navigator.geolocation) {
  //   navigator.geolocation.getCurrentPosition(function(position) {
  //     var pos = {
  //       lat: position.coords.latitude,
  //       lng: position.coords.longitude
  //     };

  //     map.setCenter(pos);
  //     marker.setMap(null);

  //     // map = new google.maps.Map(document.getElementById("map_canvas"), myOptions); 

  //     var marker = new google.maps.Marker({
  //       draggable: true,
  //       position: pos, 
  //       map: map,
  //       title: "Your location"
  //     });

  //     marker.setMap(map);

  //     google.maps.event.addListener(marker, 'dragend', function (event) {
  //         // document.getElementById("DoctorClinic_latitude").value = this.getPosition().lat();
  //         // document.getElementById("DoctorClinic_longitude").value = this.getPosition().lng();
  //     });
      

  //   }, function() {
  //     handleLocationError(true, infoWindow, map.getCenter());
  //   });
  // } else {
  //   // Browser doesn't support Geolocation
  //   handleLocationError(false, infoWindow, map.getCenter());
  // }

}

$(document).load(function() {
  initialize()
})
</script> 
  <?php if ($modelClinic->scenario == 'update'): ?>
  <a href="<?php echo CHtml::normalizeUrl(array('index')); ?>" class="btn btn-default border-r0" type="submit" role="button"><i class="fa fa-arrow-left"></i> &nbsp;Back</a>

  <button class="btn btn-default border-r0" type="submit" role="button"><i class="fa fa-pencil"></i> &nbsp;Update clinic</button>
  <?php else: ?>
  <button class="btn btn-default border-r0" type="submit" role="button"><i class="fa fa-plus"></i> &nbsp;Add another clinic</button>
  <?php endif ?>
<?php $this->endWidget(); ?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    // 'id'=>'daftar-form',
    // 'type'=>'horizontal',
  'enableClientValidation'=>false,
  'clientOptions'=>array(
    'validateOnSubmit'=>false,
  ),
)); ?>
  <div class="clear height-35"></div>
  <div class="box_default prelatife">
    <h3 class="sub_title">Social Links</h3>
        <div class="clear"></div>
        <div class="row memberc">
          <div class="col-md-4">
            <div class="form-group">
              <?php echo $form->textField($model,'social_facebook',array('class'=>'form-control', 'placeholder'=>'Facebook')); ?>
            </div>
          </div>
          <div class="clear"></div>
          <div class="col-md-4">
            <div class="form-group">
              <?php echo $form->textField($model,'social_twitter',array('class'=>'form-control', 'placeholder'=>'Twitter')); ?>
            </div>
          </div>
          <div class="clear"></div>
          <div class="col-md-4">
            <div class="form-group">
              <?php echo $form->textField($model,'social_instagram',array('class'=>'form-control', 'placeholder'=>'Instagram')); ?>
            </div>
          </div>
        </div>
  </div>
  <div class="clear height-15"></div>
  <div class="row">
    <div class="col-md-12">
      <button class="btn_purple_member defaults btn btn-default">SUBMIT</button>
    </div>
  </div>
<?php $this->endWidget(); ?>





                        </div>

                      </div>

                    </div>
                    <div class="clear"></div>
                  </div>
                  <!-- end box white tab content -->
                  <div class="clear"></div>
                </div>
                <!-- end middle -->

                <div class="clear"></div>
              </div>
              <!-- End box manages_clinic -->

              <div class="clear"></div>

<?php else: ?>
<div class="alert alert-danger">
  <div class="height-20"></div>
  <p>You have already claimed a profile, you must wait until we evaluate your claim</p>
</div>
<?php endif ?>



            </div>
            <div class="clear"></div>
          </div>
          <!-- end col md 12 -->
        </div>
        <div class="clear height-20"></div>

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>
<?php /*
<script src="//cdn.ckeditor.com/4.5.10/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'editor1' );
</script>
*/ ?>