<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="inside  s sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static page_member_s_content">
        <h1 class="title_page">MEMBER AREA</h1>
        <div class="clear height-50"></div> <div class="clear height-5"></div>
        <div class="clear"></div>

        <div class="row default box_outers_dashboardmember">
          <div class="col-md-3">
            <?php $this->renderPartial('/member/_menu', array(
            )) ?>
          </div>
          <div class="col-md-9">
            <div class="rights_cont_member">
              
              <!-- Start box manages_clinic -->
              <div class="outers_p_manageclinic">
                <div class="tops">
                  <div class="picture_ill pict_full"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(983,283, '/images/doctor/'.$model->cover , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></div>
                  <div class="back-white prelatife h55 fdesc_top">
                    <div class="insides prelatife">
                      <div class="cont_abs_tops">
                        <div class="row">
                          <div class="col-md-2">
                            <div class="pic_doctor">
                              <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(127,127, '/images/doctor/'.$model->photo , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="">
                            </div>
                          </div>
                          <div class="col-md-10">
                            <div class="padding-left-25">
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    // 'id'=>'daftar-form',
    // 'type'=>'horizontal',
  'action'=>array('index'),
  'enableClientValidation'=>false,
  'clientOptions'=>array(
    'validateOnSubmit'=>false,
  ),
)); ?>
                                <div class="form-group">
                                  <div class="mw395">
                                    <?php echo $form->textField($model,'name',array('class'=>'form-control', 'placeholder'=>'Doctor`s Name')); ?>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="mw395">
                                    <?php echo $form->textField($model,'certification',array('class'=>'form-control', 'placeholder'=>'Certification')); ?>
                                  </div>
                                </div>
                                <div class="clear height-20"></div>

                                <div class="form-group">
                                  <div class="row">
                                    <?php if ($model->status == 0): ?>
                                      
                                    <div class="col-md-6">
                                      <button onclick="window.location = '<?php echo CHtml::normalizeUrl(array('publish')); ?>';" class="btn_purple_member btn btn-default defaults">Publish Profile</button>
                                    </div>
                                    <?php else: ?>
                                    <div class="col-md-6">
                                      <button onclick="window.location = '<?php echo CHtml::normalizeUrl(array('unpublish')); ?>';" class="btn_purple_member btn btn-default defaults">Unpublish Profile</button>
                                    </div>
                                    <?php endif ?>
                                    <div class="col-md-6">
                                      <div class="fright">
                                        <button type="submit" class="btn_purple_member btn btn-default defaults">Edit Name</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
<?php $this->endWidget(); ?>
                              <!-- end forms -->
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- end cont abs top -->
                      <div class="clear"></div>
                    </div>
                  </div>
                  <!-- end backwhite -->
                  <div class="clear"></div>
                </div>
                <!-- end top -->
                <div class="clear height-35" id="editdata"></div>

                <div class="middles">
                  <!-- start end box middles -->
                  <div class="box_clinic_tab_cont">
                    <div>
                      <?php $this->renderPartial('_menu', array(
                      )) ?>

                      <div class="tab-content">
                        <div>

<?php
$criteria = new CDbCriteria;
$criteria->addCondition('doctor_id = :doctor_id');
$criteria->params[':doctor_id'] = $model->id;
$dataPhoto = new CActiveDataProvider('DoctorPhoto', array(
  'criteria'=>$criteria,
));
?>

<form action="" method="post">
    <div class="box_default prelatife">
      <h3 class="sub_title">Update Photo</h3>
          <div class="clear"></div>
          <div class="row memberc paste-html">
            <!-- start left -->
            <div class="col-md-3">
              <div class="form-group">
                <div class="bx_imageupload">
                  <input type="file" class="hidden" id="uploadFile"/>
                  <div class="button" id="uploadTrigger">
                    <img src="<?php echo $this->assetBaseurl ?>plus-member-photoUpdate.png" alt="" class="img-responsive">
                  </div>
                </div>
              </div>
            </div>
            <!-- End left -->
            <?php foreach ($dataPhoto->getData() as $key => $value): ?>
            <div class="col-md-3">
              <div class="form-group">
                <div class="picture">
                  <?php /*<img src="<?php echo $this->assetBaseurl ?>t-updatephoto-member.jpg" alt="" class="img-responsive mb-0">*/ ?>
                  <div class="backs_picturem_updatephoto">
                    <div class="in_photo photo_doctor" style="background-image: url('<?php echo Yii::app()->baseUrl; ?>/images/doctor_photo/<?php echo $value->image ?>');"></div>
                  </div>
                </div>
                <div class="clear height-10"></div>
                <input type="text" class="form-control" name="DoctorPhoto2[title][]" value="<?php echo $value->title ?>" placeholder="Description">
                <input type="hidden" class="form-control input-blog" name="DoctorPhoto2[image][]" value="<?php echo $value->image ?>">
                <input type="hidden" name="DoctorPhoto2[id][]" value="<?php echo $value->id ?>">
                <div class="clear height-10"></div>
                <select name="DoctorPhoto2[procedures_id][]" id="DoctorPhoto2_<?php echo $key ?>" class="form-control">
<?php 
$dataCategory = (PrdCategory::model()->categoryTree('spesialis', $this->languageID));
?>

                    <option value="">Select Procedures</option>
                      <?php foreach ($dataCategory as $val): ?>
                        <?php if (count($val['children']) > 0): ?>
                        <optgroup label="<?php echo $val['title'] ?>">
                          <?php foreach ($val['children'] as $k => $v): ?>
                          <option value="<?php echo $v['id'] ?>"><?php echo $v['title'] ?></option>
                          <?php endforeach ?>
                        </optgroup>
                        <?php else: ?>
                        <option value="<?php echo $val['id'] ?>"><?php echo $val['title'] ?></option>
                        <?php endif ?>
                      <?php endforeach ?>
                </select>
                <script type="text/javascript">
                  $('#DoctorPhoto2_<?php echo $key ?>').val('<?php echo $value->procedures_id ?>');
                </script>
              </div>
            </div>
            <?php endforeach ?>

            <!-- end  -->
          </div>
    </div>
    <?php for ($i=0; $i < 1; $i++) { ?>
    <div class="col-md-3 html-add">
      <div class="form-group">
        <div class="picture">
          <?php /*<img src="<?php echo $this->assetBaseurl ?>t-updatephoto-member.jpg" alt="" class="img-responsive mb-0">*/ ?>
          <div class="backs_picturem_updatephoto">
            <div class="in_photo photo_doctor" style="background-image: url('<?php echo $this->assetBaseurl ?>t-updatephoto-member.jpg');"></div>
          </div>
        </div>
        <div class="clear height-10"></div>
        <input type="text" class="form-control" name="DoctorPhoto[title][]" placeholder="Description">
        <input type="hidden" class="form-control input-blog" name="DoctorPhoto[image][]" placeholder="Description">
        <div class="clear height-10"></div>
        <select name="DoctorPhoto[procedures_id][]" id="" class="form-control">
<?php 
$dataCategory = (PrdCategory::model()->categoryTree('spesialis', $this->languageID));
?>

                    <option value="">Select Procedures</option>
                      <?php foreach ($dataCategory as $key => $value): ?>
                        <?php if (count($value['children']) > 0): ?>
                        <optgroup label="<?php echo $value['title'] ?>">
                          <?php foreach ($value['children'] as $k => $v): ?>
                          <option value="<?php echo $v['id'] ?>"><?php echo $v['title'] ?></option>
                          <?php endforeach ?>
                        </optgroup>
                        <?php else: ?>
                        <option value="<?php echo $value['id'] ?>"><?php echo $value['title'] ?></option>
                        <?php endif ?>
                      <?php endforeach ?>
        </select>
      </div>
    </div>
    <?php } ?>

    <div class="clear height-15"></div>
    <div class="row">
      <div class="col-md-12">
        <button class="btn_purple_member defaults btn btn-default">SUBMIT</button>
      </div>
    </div>
</form>

  <script type="text/javascript">
var htmlAdd = $('.html-add').html();
$('.html-add').html('');
function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function (e) {
          $('.paste-html').append('<div class="col-md-3">'+htmlAdd+'</div>');
          // alert(e.target.result)
          $('.photo_doctor:last').attr('style', 'background-image: url('+e.target.result+');');
          $('.input-blog:last').val(e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
  }
}
  $(document).ready(function(){
    $("#uploadTrigger").click(function(){
       $("#uploadFile").click();
    });
    $("#uploadFile").change(function(){
        readURL(this);
    });
  })
  </script>



                        </div>

                      </div>

                    </div>
                    <div class="clear"></div>
                  </div>
                  <!-- end box white tab content -->
                  <div class="clear"></div>
                </div>
                <!-- end middle -->

                <div class="clear"></div>
              </div>
              <!-- End box manages_clinic -->

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
          <!-- end col md 12 -->
        </div>
        <div class="clear height-20"></div>

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>

<script src="//cdn.ckeditor.com/4.5.10/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'editor1' );
</script>