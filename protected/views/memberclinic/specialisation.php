<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="inside  s sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static page_member_s_content">
        <h1 class="title_page">MEMBER AREA</h1>
        <div class="clear height-50"></div> <div class="clear height-5"></div>
        <div class="clear"></div>

        <div class="row default box_outers_dashboardmember">
          <div class="col-md-3">
            <?php $this->renderPartial('/member/_menu', array(
            )) ?>
          </div>
          <div class="col-md-9">
            <div class="rights_cont_member">
              
              <!-- Start box manages_clinic -->
              <div class="outers_p_manageclinic">
                <div class="tops">

                  <div class="picture_ill pict_full"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(983,283, '/images/doctor/'.$model->cover , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></div>
                  <div class="back-white prelatife h55 fdesc_top">
                    <div class="insides prelatife">
                      <div class="cont_abs_tops">
                        <div class="row">
                          <div class="col-md-2">
                            <div class="pic_doctor">
                              <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(127,127, '/images/doctor/'.$model->photo , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="">
                            </div>
                          </div>
                          <div class="col-md-10">
                            <div class="padding-left-25">
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    // 'id'=>'daftar-form',
    // 'type'=>'horizontal',
  'action'=>array('index'),
  'enableClientValidation'=>false,
  'clientOptions'=>array(
    'validateOnSubmit'=>false,
  ),
)); ?>
                                <div class="form-group">
                                  <div class="mw395">
                                    <?php echo $form->textField($model,'name',array('class'=>'form-control', 'placeholder'=>'Doctor`s Name')); ?>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="mw395">
                                    <?php echo $form->textField($model,'certification',array('class'=>'form-control', 'placeholder'=>'Certification')); ?>
                                  </div>
                                </div>
                                <div class="clear height-20"></div>

                                <div class="form-group">
                                  <div class="row">
                                    <?php if ($model->status == 0): ?>
                                      
                                    <div class="col-md-6">
                                      <button onclick="window.location = '<?php echo CHtml::normalizeUrl(array('publish')); ?>';" class="btn_purple_member btn btn-default defaults">Publish Profile</button>
                                    </div>
                                    <?php else: ?>
                                    <div class="col-md-6">
                                      <button onclick="window.location = '<?php echo CHtml::normalizeUrl(array('unpublish')); ?>';" class="btn_purple_member btn btn-default defaults">Unpublish Profile</button>
                                    </div>
                                    <?php endif ?>
                                    <div class="col-md-6">
                                      <div class="fright">
                                        <button type="submit" class="btn_purple_member btn btn-default defaults">Edit Name</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
<?php $this->endWidget(); ?>
                              <!-- end forms -->
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- end cont abs top -->
                      <div class="clear"></div>
                    </div>
                  </div>
                  <!-- end backwhite -->
                  <div class="clear"></div>
                  
                </div>
                <!-- end top -->
                <div class="clear height-35" id="editdata"></div>

                <div class="middles">
                  <!-- start end box middles -->
                  <div class="box_clinic_tab_cont">
                    <div>
                      <?php $this->renderPartial('_menu', array(
                      )) ?>

                      <div class="tab-content">
                        <div>



<?php 
$dataCategory = (PrdCategory::model()->categoryTree('spesialis', $this->languageID));
$modelSpesialis = DoctorSpecialication::model()->findAll('doctor_id = :doctor_id', array(':doctor_id'=>$model->id));
$dataKey = array();
foreach ($modelSpesialis as $key => $value) {
  $dataKey[$value->specialitation_id] = 1;
}
?>
<form action="" method="post">
  <?php foreach ($dataCategory as $key => $value): ?>
  <div class="box_default prelatife">
    <h3 class="sub_title"><?php echo $value['title'] ?></h3>
        <div class="clear"></div>
        <div class="row memberc">
          <?php foreach ($value['children'] as $k => $v): ?>
          <div class="col-md-4">
            <div class="form-group">
              <div class="checkbox">
              <label>

                <input type="checkbox" <?php if (array_key_exists($v['id'], $dataKey)): ?>checked="checked"<?php endif ?> name="Specialisation[<?php echo $v['id'] ?>]" value="1"> <?php echo $v['title'] ?>
              </label>
            </div>
            </div>
          </div>
          <?php endforeach ?>
        </div>
  </div>
  <div class="clear height-35"></div>
  <?php endforeach ?>

  <div class="row">
    <div class="col-md-12">
      <button type="submit" class="btn_purple_member defaults btn btn-default" name="button" value="SUBMIT">SUBMIT</button>
    </div>
  </div>
</form>



                        </div>

                      </div>

                    </div>
                    <div class="clear"></div>
                  </div>
                  <!-- end box white tab content -->
                  <div class="clear"></div>
                </div>
                <!-- end middle -->

                <div class="clear"></div>
              </div>
              <!-- End box manages_clinic -->

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
          <!-- end col md 12 -->
        </div>
        <div class="clear height-20"></div>

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>

<script src="//cdn.ckeditor.com/4.5.10/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'editor1' );
</script>