<div class="header-tab">
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" <?php if ($this->action->id == 'index'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('index')); ?>">CLINIC CONTACT INFO</a></li>
    <li role="presentation" <?php if ($this->action->id == 'specialisation'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('specialisation')); ?>#editdata">SELECT SPECIALISATION</a></li>
    <li role="presentation" <?php if ($this->action->id == 'about'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('about')); ?>#editdata">ABOUT US</a></li>
    <li role="presentation" <?php if ($this->action->id == 'photo'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('photo')); ?>#editdata">UPDATE PHOTO</a></li>
    <li role="presentation" <?php if ($this->action->id == 'video'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('video')); ?>#editdata">UPDATE VIDEO</a></li>
  </ul>
</div>
