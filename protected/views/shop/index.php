<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static page_products">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>

      <div class="content-text insides_static">
        <h1 class="title_page">SHOP</h1>
        <div class="clear height-35"></div>

        <!-- start list product_landing -->
        <div class="box_listing_productslanding mw1000 tengah">
          <?php foreach ($data as $key => $value): ?>
          <div class="items <?php if ($i == 2): ?>last<?php endif ?>">
            <h3 class="titles text-center"><?php echo $value->description->name ?></h3>
            <div class="clear"></div>
            <div class="middle_products_desc">
              <div class="row">
                <div class="col-md-9">
                  <div class="big_picture"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(744,547, '/images/category/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></div>
                </div>
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
$criteria->addCondition('terbaru = :terbaru');
$criteria->params[':terbaru'] = 1;
$criteria->addCondition('category_id = :category_id');
$criteria->params[':category_id'] = $value->id;
$criteria->order = 'RAND()';
$criteria->limit = 3;

$product = PrdProduct::model()->findAll($criteria);
?>
                <div class="col-md-3">
                  <div class="thumb_picture">
                    <ul class="list-unstyled">
                      <?php foreach ($product as $k => $v): ?>
                      <li>
                        <a href="<?php echo CHtml::normalizeUrl(array('detail', 'id'=>$v->id)); ?>">
                        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(232,170, '/images/product/'.$v->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                        </a>
                      </li>
                      <?php endforeach ?>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="clear height-25"></div>
              <div class="row">
                <div class="col-md-9 text-left">
                  <p><?php echo nl2br($value->description->desc) ?></p>
                </div>
                <div class="col-md-3">
                  <div class="fright text-center">
                    <a href="<?php echo CHtml::normalizeUrl(array('list', 'id'=>$value->id)); ?>" class="d-inline back_purple_defaults_tl">SHOP NOW</a>
                  </div>
                </div>
              </div>
              <div class="clear"></div>
            </div>
            <!-- end middle product desc -->
          </div>
          <?php endforeach ?>

          <div class="clear"></div>
        </div>
        <!-- // end list product_landing -->

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>