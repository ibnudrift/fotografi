<section class="section_default back_cream mh710 back_grey_pattern sub_page start_journey">
  <div class="prelatife container z-15">
    <div class="clear height-50"></div>
    <div class="clear height-50"></div>

    <div class="blocks_journey text-center step_1 step_4">
      <div class="tops">
        <h6 class="sub_title">START JOURNEY</h6>
        <div class="clear"></div>
        <div class="lines_purple_journey tengah"></div>
        <div class="clear height-0"></div>

        <h1 class="titlepage">Step 4</h1>
        <p class="c1"><?php echo $this->setting['journey_step_4_title'] ?></p>
      </div>

      <div class="clear height-50"></div><div class="height-20"></div>

      <div class="middle prelatife">
        <div class="row default">
          <div class="col-md-9">
            <div class="mw909 content-text">

              <h5><?php echo nl2br($this->setting['journey_step_4_subtitle']) ?></h5>
              <div class="landing_hero pict_full">
                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(909,1000, '/images/static/'.$this->setting['journey_step_4_image'] , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive">
              </div>
              <div class="clear"></div>
              
              <?php echo $this->setting['journey_step_4_content'] ?>

              <div class="clear height-5"></div>
              <div class="row">
                <div class="col-md-6">
                  <a href="https://www.maccredit.com.au/" target="_blank"><img src="<?php echo $this->assetBaseurl ?>back-banner1-step-4.png" alt="" class="img-responsive"></a>
                </div>
                <div class="col-md-6">
                  <a href="<?php echo CHtml::normalizeUrl(array('/contact/index')); ?>"><img src="<?php echo $this->assetBaseurl ?>back-banner2-step-4.png" alt="" class="img-responsive"></a>
                </div>
              </div>


              <div class="clear"></div>
            </div>

            <div class="clear"></div>
          </div>
          <div class="col-md-3">
            <div class="blocks_right_stepmenu_inside">
              <?php echo $this->renderPartial('//layouts/_menu_step', array()); ?>
            </div>
          </div>
        </div>
        <div class="clear height-40"></div>
        <div class="clear"></div>
      </div>

      <div class="clear"></div>
    </div>
    
    <div class="clear"></div>
  </div>

</section>