<section class="outers_page_static back_cream back_grey_pattern" style="min-height: 300px;">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>

      <div class="content-text insides_static">
        <h1 class="title_page">Error <?php echo $error['code'] ?></h1>
        <div class="clear"></div>
        
        <div class="clear height-50"></div>
        <h2><?php echo $error['message'] ?></h2>

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<style type="text/css">
    body{
        background-color: #231f2e;
    }
</style>