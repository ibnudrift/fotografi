<section class="section_default back_cream mh710 back_grey_pattern sub_page start_journey">
  <div class="prelatife container z-15">
    <div class="clear height-50"></div>
    <div class="clear height-50"></div>

    <div class="blocks_journey text-center">
      <div class="tops">
        <h6 class="sub_title">START JOURNEY</h6>
        <div class="clear"></div>
        <div class="lines_purple_journey tengah"></div>
        <div class="clear height-30"></div>
        <p class="c1">Begin Your Journey in Surgery</p>
        <p>We&#39;ll help you to stay on the <span>RightSteps</span></p>
      </div>

      <div class="clear height-50"></div><div class="height-50"></div>

      <div class="middle prelatife">
          <div class="row">
            <div class="col-md-12">
                <div class="back-left mw1000 prelatife block_slide_landingjourney">
                  <!-- <img src="<?php // echo $this->assetBaseurl ?>step_landing_bg1.jpg" alt="" class="img-responsive">
                  <div class="intext">
                    <small>Identify why you want to have surgery</small>
                    <div class="clear"></div>
                    <h5>The first step is to identify why you want to have surgery?</h5>
                    <div class="clear"></div>
                    <p>Is it for you? Or is it for someone else?</p>
                    <div class="clear height-30"></div>
                    <a href="<?php // echo CHtml::normalizeUrl(array('/home/journey/', 'step'=>'1')); ?>" class="back-purple_default btn btn-default">GO TO STEP 1</a>
                  </div> -->
                  <div id="carousel_landing_journey" class="carousel fade" data-pause="true">
                    <div class="carousel-inner" role="listbox">
                      <div class="item active" data-id="1">
                        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1022,566, '/images/static/'.$this->setting['journey_step_1_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                        <div class="carousel-caption">
                          <div class="intext">
                              <small>Step 1</small>
                              <div class="clear"></div>
                              <h5><?php echo $this->setting['journey_step_1_title'] ?></h5>
                              <div class="clear"></div>
                              <p><?php echo $this->setting['journey_step_1_subtitle'] ?></p>
                              <div class="clear height-30"></div>
                              <a href="<?php echo CHtml::normalizeUrl(array('/home/journey/', 'step'=>'1')); ?>" class="back-purple_default btn btn-default">GO TO STEP 1</a>
                          </div>
                        </div>
                      </div>
                      <?php $numbs_active = array(
                      // '2', '3', '4', '6', '9'
                        2 => '3',
                        3 => '4',
                        4 => '4',
                        5 => '7',
                        6 => '10',
                      ); ?>
                      <?php foreach ($numbs_active as $key => $vil) { ?>
                      <div class="item" data-id="<?php echo $key ?>">
                        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1022,566, '/images/static/'.$this->setting['journey_step_'.$vil.'_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                        <div class="carousel-caption">
                          <div class="intext">
                              <small>Step  <?php echo $key ?></small>
                              <div class="clear"></div>
                              <h5><?php echo $this->setting['journey_step_'.$vil.'_title'] ?></h5>
                              <div class="clear"></div>
                              <p><?php echo $this->setting['journey_step_'.$vil.'_subtitle'] ?></p>
                              <div class="clear height-30"></div>
                              <a href="<?php echo CHtml::normalizeUrl(array('/home/journey/', 'step'=>$key)); ?>" class="back-purple_default btn btn-default">GO TO STEP <?php echo $key; ?></a>
                          </div>
                        </div>
                      </div>
                      <?php } ?>

                    </div>

                  </div>
                  <!-- end fcs landing -->
                </div>
                <div class="pos_right_abs landing_page">
                  <?php echo $this->renderPartial('//layouts/_menu_step', array()); ?>
                </div>
                <div class="clear"></div>
            </div>
          </div>

        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    
    <div class="clear"></div>
  </div>
  <div class="back-purple-journeybottom">
    <div class="back-purple"></div>
    <div class="clear height-50"></div>
    <div class="clear height-50"></div>
    <div class="clear height-50"></div>
    <div class="clear height-50"></div>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function(){
      var c_loc;

      $('.landing_page .block_default_rightmenustatic ul li').live("hover",function () {
        c_loc = $(this).attr('data-id');
        // console.log("active-" + c_loc);
        c_loc = parseInt(c_loc) - 1;
        $('.carousel').carousel(c_loc);
      });
      $('.carousel').on('slide.bs.carousel', function (e) {
        // console.log($(e).attr('data-id'))
        // console.log($(e.relatedTarget).attr('data-id'));
        $('.block_default_rightmenustatic .active').removeClass('active');
        $('.block_default_rightmenustatic .c'+$(e.relatedTarget).attr('data-id')).addClass('active');
      })
      $('.block_default_rightmenustatic .c1').addClass('active');
  });
</script>