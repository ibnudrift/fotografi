<section class="section_default back_cream mh710 back_grey_pattern sub_page start_journey">
  <div class="prelatife container z-15">
    <div class="clear height-50"></div>
    <div class="clear height-50"></div>

    <div class="blocks_journey text-center step_1">
      <div class="tops">
        <h6 class="sub_title">START JOURNEY</h6>
        <div class="clear"></div>
        <div class="lines_purple_journey tengah"></div>
        <div class="clear height-0"></div>

        <h1 class="titlepage">Step 2</h1>
        <p class="c1"><?php echo $this->setting['journey_step_2_title'] ?></p>
      </div>

      <div class="clear height-50"></div><div class="height-20"></div>

      <div class="middle prelatife">
        <div class="row default">
          <div class="col-md-9">
            <div class="mw909 content-text">
              <h4><?php echo nl2br($this->setting['journey_step_2_subtitle']) ?></h4>
              <div class="landing_hero pict_full">
                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(909,1000, '/images/static/'.$this->setting['journey_step_2_image'] , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive">
              </div>
              <div class="clear"></div>
              <?php echo $this->setting['journey_step_2_content'] ?>

              <div class="box_form_step1">
                <form class="form-inline">
                    <div class="form-group mr-8">
                      <label class="sr-only" for=""></label>
                      <input type="text" class="form-control" id="" placeholder="Your Name">
                    </div>
                    <div class="form-group">
                      <label class="sr-only" for=""></label>
                      <select name="" id="" class="form-control css-select-moz">
                        <option value="0">Select Category</option>
                        <option value="1">Setup Category 1</option>
                        <option value="1">Setup Category 2</option>
                        <option value="1">Setup Category 3</option>
                      </select>
                    </div>
                    <div class="clear"></div>
                    <div class="form-group dblock">
                      <label class="sr-only" for=""></label>
                      <textarea name="" id="" rows="4" class="form-control" placeholder="Share your concerns"></textarea>
                    </div>
                    <div class="clear"></div>
                    <button type="submit" class="btn btn-default btn_submit_custom"></button>
                  </form>
                <div class="clear"></div>
              </div>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>

            <!-- start comment -->
            <div class="box_comment_community mw909">
              <div class="tops">
                <div class="row">
                  <div class="col-md-3">
                    <span>Fear fears community</span>
                  </div>
                  <div class="col-md-9">
                    <div class="box_search_community">
                      <form action="#" class="form-inline">
                        <div class="form-group mr-8">
                          <label class="sr-only" for=""></label>
                          <select name="" id="" class="form-control css-select-moz">
                            <option value="0">Select Category</option>
                            <option value="1">Setup Category 1</option>
                            <option value="1">Setup Category 2</option>
                            <option value="1">Setup Category 3</option>
                          </select>
                        </div>
                        <div class="form-group prelatife">
                          <label class="sr-only" for=""></label>
                          <input type="text" class="form-control" id="" placeholder="Your Name">
                          <button type="submit" class="btn btn-default magnify_submit"><i class="fa fa-search"></i></button>
                        </div>
                       
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div class="clear"></div>
              <!-- start list comment  -->
              <div class="list_comment_block">
                <div class="items">
                  <div class="top">
                    <div class="fleft pict">
                      <img src="<?php echo $this->assetBaseurl ?>user_comment.png" alt="" class="img-responsive">
                    </div>
                    <div class="fleft titles">
                      <span>Megan Fox</span>
                      in Breast Implant Category
                    </div>
                    <div class="clear"></div>
                  </div>
                  <div class="desc">
                    <p>I worry about them looking fake, making me look too heavy or matronly. I worry about what my teenage kids will think. I also worry about this being a selfish and vain decision only.</p>
                  </div>
                </div>
                <div class="items">
                  <div class="top">
                    <div class="fleft pict">
                      <img src="<?php echo $this->assetBaseurl ?>user_comment.png" alt="" class="img-responsive">
                    </div>
                    <div class="fleft titles">
                      <span>Corry Marry</span> in Breast Implant Category
                    </div>
                    <div class="clear"></div>
                  </div>
                  <div class="desc">
                    <p>I am afraid I won&#39;t be able to train like before.... loose my fitness achievement and feel like they&#39;re too big that I could be able to be dynamic enough.... feeling that they&#39;re too big and maybe I would prefer to stay petite..... (from latin america where all the girls are supposed to be big!)</p>
                  </div>
                  <div class="subs">
                    <div class="top">
                      <div class="fleft pict">
                        <img src="<?php echo $this->assetBaseurl ?>user_comment.png" alt="" class="img-responsive">
                      </div>
                      <div class="fleft titles">
                        <span>Corry Marry</span> in Breast Implant Category
                      </div>
                      <div class="clear"></div>
                    </div>
                    <div class="desc">
                      <p>I am afraid I won&#39;t be able to train like before.... loose my fitness achievement and feel like they&#39;re too big that I could be able to be dynamic enough.... feeling that they&#39;re too big and maybe I would prefer to stay petite..... (from latin america where all the girls are supposed to be big!)</p>
                    </div>
                  </div>
                </div>

                <div class="clear"></div>
              </div>
              <div class="bottoms_comment">
                  <div class="text-right">
                    <a href="#" class="tx_more">Click for more </a>
                  </div>
                </div>
              <!-- end list comment  -->
              <div class="clear"></div>
            </div>
            <!-- end comment -->

            <div class="clear"></div>
          </div>
          <div class="col-md-3">
            <div class="blocks_right_stepmenu_inside">
              <?php echo $this->renderPartial('//layouts/_menu_step', array()); ?>
            </div>
          </div>
        </div>
        <div class="clear height-50"></div>
        <div class="clear"></div>
      </div>

      <div class="clear"></div>
    </div>
    
    <div class="clear"></div>
  </div>

</section>