<section class="section_default back_cream mh710 back_grey_pattern sub_page start_journey">
  <div class="prelatife container z-15">
    <div class="clear height-50"></div>
    <div class="clear height-50"></div>

    <div class="blocks_journey text-center step_1 step_8">
      <div class="tops">
        <h6 class="sub_title">START JOURNEY</h6>
        <div class="clear"></div>
        <div class="lines_purple_journey tengah"></div>
        <div class="clear height-0"></div>

        <h1 class="titlepage">Step 10</h1>
        <p class="c1"><?php echo $this->setting['journey_step_10_title'] ?></p>
      </div>

      <div class="clear height-50"></div><div class="height-20"></div>

      <div class="middle prelatife">
        <div class="row default">
          <div class="col-md-9">
            <div class="mw909 content-text pr-0">
              <h5><?php echo nl2br($this->setting['journey_step_10_subtitle']) ?></h5>
              <div class="landing_hero pict_full">
                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(909,1000, '/images/static/'.$this->setting['journey_step_10_image'] , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive">
              </div>
              <div class="clear"></div>
              
              <?php echo $this->setting['journey_step_10_content'] ?>
              <div class="clear height-10"></div>
              <a href="#" class="btns_purples w180">SHARE YOUR STORY</a>
              <div class="clear height-50"></div>

              <div class="box_sharing_step10 back-white">
                <div class="listing_sharing_step10">
                  <?php for ($i=1; $i < 4; $i++) { ?>
                  <div class="items <?php if ($i == 3): ?>last<?php endif ?>">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="pict"><img src="<?php echo $this->assetBaseurl ?>pict-sharing-ex.jpg" alt="" class="img-responsive"></div>
                      </div>
                      <div class="col-md-8">
                        <div class="descriptions">
                          <h5 class="titles">Finding the right surgeons - And why it matters. </h5>
                          <span class="dates">June 7th, 2016</span>
                          <div class="clear height-30"></div>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi enim neque, vestibulum ut blandit at, ullamcorper ut quam. Mauris a risus neque. In non imperdiet dolor. netus et malesuada fames ac turpis egestas. Morbi iaculis porttitor lacinia.</p>
                          <div class="clear"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                </div>
              </div>

              <div class="clear"></div>
            </div>

            <div class="clear"></div>
          </div>
          <div class="col-md-3">
            <div class="blocks_right_stepmenu_inside">
              <?php echo $this->renderPartial('//layouts/_menu_step', array()); ?>
            </div>
          </div>
        </div>
        <div class="clear height-30"></div>
        <div class="clear"></div>
      </div>

      <div class="clear"></div>
    </div>
    
    <div class="clear"></div>
  </div>

</section>