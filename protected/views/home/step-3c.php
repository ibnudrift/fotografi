<section class="section_default back_cream mh710 back_grey_pattern sub_page start_journey">
  <div class="prelatife container z-15">
    <div class="clear height-50"></div>
    <div class="clear height-50"></div>

    <div class="blocks_journey text-center step_1 step_3">
      <div class="tops">
        <h6 class="sub_title">START JOURNEY</h6>
        <div class="clear"></div>
        <div class="lines_purple_journey tengah"></div>
        <div class="clear height-0"></div>

        <h1 class="titlepage">Step 3</h1>
        <p class="c1"><?php echo $this->setting['journey_step_1_title'] ?></p>
      </div>

      <div class="clear height-50"></div><div class="height-20"></div>

      <div class="middle detailStep_3 prelatife">
        <div class="row default">
          <div class="col-md-9">
            <div class="mw909 content-text">
              <div class="on_top">
                <div class="row">
                  <div class="col-md-8">
                    <div class="t_category">
                      <a href="<?php echo CHtml::normalizeUrl(array('/home/journey/', 'step'=>'3')); ?>">Category</a> &nbsp;/&nbsp; <b><?php echo $this->setting['journey_step_3_menu_'.$_GET['category'].'_title'] ?></b>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="back_category text-right">
                      <a href="<?php echo CHtml::normalizeUrl(array('/home/journey/', 'step'=>'3')); ?>">Back to category selections</a>
                    </div>
                  </div>
                </div>
                <div class="clear height-10"></div>
                <div class="lines"></div>
              </div>

              <?php echo $this->setting['journey_step_3_menu_'.$_GET['category'].'_content'] ?>

                <div class="list_category_step3 details_step3">
                  <div class="row">
<?php
$criteria = new CDbCriteria;
$criteria->addCondition('category_id = :category_id');
$criteria->params[':category_id'] = $_GET['category'];
$stepProcedure = StepProcedure::model()->findAll($criteria);
?>
                    <?php foreach ($stepProcedure as $key => $value): ?>
                    <div class="col-md-4">
                      <div class="items">
                        <div class="pict">
                          <a href="<?php echo $value->url ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(270,270, '/images/stepprocedure/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></a>
                        </div>
                        <div class="clear height-10"></div>
                        <div class="titles">
                          <a href="<?php echo $value->url ?>"><?php echo nl2br($value->title) ?></a>
                        </div>
                      </div>
                    </div>
                    <?php endforeach ?>
                  </div>
                </div>

              <div class="clear"></div>
            </div>

            <div class="clear"></div>
          </div>
          <div class="col-md-3">
            <div class="blocks_right_stepmenu_inside">
              <?php echo $this->renderPartial('//layouts/_menu_step', array()); ?>
            </div>
          </div>
        </div>
        <div class="clear height-20"></div>
        <div class="clear"></div>
      </div>

      <div class="clear"></div>
    </div>
    
    <div class="clear"></div>
  </div>

</section>