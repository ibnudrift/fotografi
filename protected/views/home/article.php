<section class="outers_inside_page p_gallery">
	<div class="insides_page">
		<div class="back-white backs_cloud_insidep content-text text-center">
			<div class="prelatife container">
				<div class="clear h60"></div>
				<h1 class="title-pages">Articles</h1>
				<div class="clear height-50"></div><div class="height-20"></div>
				

				<div class="listing_article_default">
					<div class="row">
						<?php for ($i=0; $i < 4; $i++) { ?>
						<div class="col-lg-4 col-md-6">
							<div class="items">
								<div class="pict">
									<img src="<?php echo $this->assetBaseurl ?>artikel-1.jpg" alt="" class="img-responsive">
								</div>
								<div class="desc">
									<div class="d-inline text-left v-top mw290">
										<span class="title d-inline">
											Promosi 20% discount perawatan kulit di Cahaya Medical Centre
										</span>
									</div>
									<div class="d-inline text-right v-top">
										<div class="hidden-xs">
										<a href="<?php echo CHtml::normalizeUrl(array('/home/articlesdetail')); ?>"><img src="<?php echo $this->assetBaseurl ?>btns-grey-chevron-rights.png" alt=""></a>
										</div>
										<div class="visible-xs">
										  <a href="<?php echo CHtml::normalizeUrl(array('/home/articlesdetail')); ?>"><img src="<?php echo $this->assetBaseurl ?>btns-grey-chevron-rights2.png" alt=""></a>
										</div>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="items">
								<div class="pict">
									<img src="<?php echo $this->assetBaseurl ?>artikel-2.jpg" alt="" class="img-responsive">
								</div>
								<div class="desc">
									<div class="d-inline text-left v-top mw290">
										<span class="title d-inline">
											Mengapa penting untuk tidur lebih pagi
										</span>
									</div>
									<div class="d-inline text-right v-top">
										<div class="hidden-xs">
										<a href="<?php echo CHtml::normalizeUrl(array('/home/articlesdetail')); ?>"><img src="<?php echo $this->assetBaseurl ?>btns-grey-chevron-rights.png" alt=""></a>
										</div>
										<div class="visible-xs">
										  <a href="<?php echo CHtml::normalizeUrl(array('/home/articlesdetail')); ?>"><img src="<?php echo $this->assetBaseurl ?>btns-grey-chevron-rights2.png" alt=""></a>
										</div>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="items">
								<div class="pict">
									<img src="<?php echo $this->assetBaseurl ?>artikel-3.jpg" alt="" class="img-responsive">
								</div>
								<div class="desc">
									<div class="d-inline text-left v-top mw290">
										<span class="title d-inline">
											Mengapa hati yang gembira dapat menyembuhkan penyakit
										</span>
									</div>
									<div class="d-inline text-right v-top">
										<div class="hidden-xs">
										<a href="<?php echo CHtml::normalizeUrl(array('/home/articlesdetail')); ?>"><img src="<?php echo $this->assetBaseurl ?>btns-grey-chevron-rights.png" alt=""></a>
										</div>
										<div class="visible-xs">
										  <a href="<?php echo CHtml::normalizeUrl(array('/home/articlesdetail')); ?>"><img src="<?php echo $this->assetBaseurl ?>btns-grey-chevron-rights2.png" alt=""></a>
										</div>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>

				<div class="clear height-50"></div><div class="height-40"></div>

				<div class="clear"></div>
			</div>
		</div>

		<div class="clear"></div>
	</div>
</section>
<div class="clear"></div>