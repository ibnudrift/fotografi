<section class="section_default back_cream mh710 back_grey_pattern sub_page start_journey">
  <div class="prelatife container z-15">
    <div class="clear height-50"></div>
    <div class="clear height-50"></div>

    <div class="blocks_journey text-center step_1 step_8">
      <div class="tops">
        <h6 class="sub_title">START JOURNEY</h6>
        <div class="clear"></div>
        <div class="lines_purple_journey tengah"></div>
        <div class="clear height-0"></div>

        <h1 class="titlepage">Step 7</h1>
        <p class="c1"><?php echo $this->setting['journey_step_9_title'] ?></p>
      </div>

      <div class="clear height-50"></div><div class="height-20"></div>

      <div class="middle prelatife">
        <div class="row default">
          <div class="col-md-9">
            <div class="mw909 content-text pr-0">
              <h5><?php echo nl2br($this->setting['journey_step_9_subtitle']) ?></h5>
              <div class="landing_hero pict_full">
                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(909,1000, '/images/static/'.$this->setting['journey_step_9_image'] , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive">
              </div>
              <div class="clear"></div>
              
                <?php echo $this->setting['journey_step_9_content'] ?>

                <div class="clear height-5"></div>
                <a href="#" class="btns_purples">REVEAL YOUR BEAUTY</a>

                <div class="clear height-30"></div><div class="height-3"></div>
                <div class="lines-grey2"></div>
                <div class="clear height-50"></div>

                <div class="list_reveal_news">
                  <div class="row">
                    <?php for ($i=1; $i < 3; $i++) { ?>
                    <div class="col-md-6">
                      <div class="items prelatife">
                        <div class="picture">
                        <img src="<?php echo $this->assetBaseurl ?>reveal-1.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="description">
                          <div class="titles">Beauty Surgery trends in the early age of 20s? Good or Bad?</div>
                          <div class="clear"></div>
                          <span class="dates">11 June 2016</span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="items prelatife">
                        <div class="picture">
                        <img src="<?php echo $this->assetBaseurl ?>reveal-2.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="description">
                          <div class="titles">Beauty Surgery trends in the early age of 20s? Good or Bad?</div>
                          <div class="clear"></div>
                          <span class="dates">11 June 2016</span>
                        </div>
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div>

              <div class="clear"></div>
            </div>

            <div class="clear"></div>
          </div>
          <div class="col-md-3">
            <div class="blocks_right_stepmenu_inside">
              <?php echo $this->renderPartial('//layouts/_menu_step', array()); ?>
            </div>
          </div>
        </div>
        <div class="clear height-30"></div>
        <div class="clear"></div>
      </div>

      <div class="clear"></div>
    </div>
    
    <div class="clear"></div>
  </div>

</section>