<?php 
  $dt_steps = array(
             1 => array(
                'name' => 'Step 1',
                'desc' => strtolower($this->setting['journey_step_1_title']),
                ),
              // array(
              //   'name' => 'Step 2',
              //   'desc' => strtolower($this->setting['journey_step_2_title']),
              //   ),
              array(
                'name' => 'Step 2',
                'desc' => strtolower($this->setting['journey_step_3_title']),
                ),
              array(
                'name' => 'Step 3',
                'desc' => strtolower($this->setting['journey_step_4_title']),
                ),
              array(
                'name' => 'Step 4',
                'desc' => strtolower($this->setting['journey_step_5_title']),
                ),
              // array(
              //   'name' => 'Step 6',
              //   'desc' => strtolower($this->setting['journey_step_6_title']),
              //   ),
              array(
                'name' => 'Step 5',
                'desc' => strtolower($this->setting['journey_step_7_title']),
                ),
              // array(
              //   'name' => 'Step 8',
              //   'desc' => strtolower($this->setting['journey_step_8_title']),
              //   ),
              // array(
              //   'name' => 'Step 9',
              //   'desc' => strtolower($this->setting['journey_step_9_title']),
              //   ),
              array(
                'name' => 'Step 6',
                'desc' => strtolower($this->setting['journey_step_10_title']),
                ),
            );

?>
<div class="outers_blck_fcs h100per sect_cfcs prelatife">
    <div id="myCarousel" class="carousel fade" data-ride="carousel">
      <div class="bottoms_carouselindic">
        <div class="prelatife container">
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <!-- <li data-target="#myCarousel" data-slide-to="1"></li> -->
                <!-- <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
                <li data-target="#myCarousel" data-slide-to="4"></li> -->
            </ol>
          </div>
        </div>

        <div class="carousel-inner">
            <div class="item active">
                <div class="fill visible-lg visible-md visible-sm" style="background-image:url('<?php echo $this->assetBaseurl ?>fcs-1.jpg');"></div>
                <!-- <div class="picturr visible-sm">
                  <img src="<?php // echo $this->assetBaseurl ?>fcs-respons-ipad.jpg" alt="" class="img-responsive">
                </div>
                <div class="picturr visible-xs">
                  <img src="<?php // echo $this->assetBaseurl ?>fcs-respons-iphone.jpg" alt="" class="img-responsive">
                </div> -->
            </div>
            <!-- <div class="item">
                <div class="fill visible-lg visible-md" style="background-image:url('<?php echo $this->assetBaseurl ?>fcs-2.jpg');"></div> -->
                <!-- <div class="picturr visible-sm">
                  <img src="<?php // echo $this->assetBaseurl ?>fcs-respons-ipad.jpg" alt="" class="img-responsive">
                </div>
                <div class="picturr visible-xs">
                  <img src="<?php // echo $this->assetBaseurl ?>fcs-respons-iphone.jpg" alt="" class="img-responsive">
                </div> -->
            <!-- </div> -->
            <!-- <div class="item">
                <div class="fill" style="background-image:url('<?php echo $this->assetBaseurl ?>fcs-1n.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('<?php echo $this->assetBaseurl ?>fcs-1n.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('<?php echo $this->assetBaseurl ?>fcs-1n.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('<?php echo $this->assetBaseurl ?>fcs-1n.jpg');"></div>
            </div> -->
        </div>

    </div>
    <!-- <div class="blocks_txt_infcs">
      <div class="prelatife container">
        <div class="clear h90"></div>
          <div class="mw540">
            <h3>Selamat datang di Cahaya Diagnostic Centre, pusat pelayanan kesehatan anda</h3>
            <div class="clear"></div>
            <p>Cahaya Diagnostic Centre - grup manajemen dari Cahaya Medika Healthcare kini hadir untuk memberikan pelayanan lebih baik bagi siapapun yang membutuhkan penanganan kesehatan profesional, terjangkau serta smart.</p>
            <div class="clear height-35"></div>
            <a href="#"><img src="<?php echo $this->assetBaseurl ?>back-btns-aboutus-ifcs.png" alt=""></a> &nbsp;&nbsp;
            <a href="#"><img src="<?php echo $this->assetBaseurl ?>back-btns-callus-ifcs.png" alt=""></a>
              <div class="clear"></div>
          </div>
            <div class="clear"></div>
        </div>
    </div> -->
    <div class="back_pattern_purple_bottom_slider">
      <div class="container prelatife text-center">
        <div class="clear height-40"></div>
        <div class="inside_text">
          <span class="top">Find a Surgeon You Can Trust</span>
          <div class="clear height-25"></div><div class="height-2"></div>
          <div class="box_filter_center">
            <form class="form-inline" method="get" action="<?php echo CHtml::normalizeUrl(array('/surgeons/list')); ?>">
              <div class="form-group">
                <label for="exampleInputName2">Procedure</label>
                <div class="d-inline selection v-top">
<script src="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/jquery.easy-autocomplete.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.min.css">
                  <input type="text" name="specialisation" id="specialisation" class="form-control selectpick css-select-moz"/>

<script type="text/javascript">
var options = {
    url: "<?php echo CHtml::normalizeUrl(array('/home/specialisationList')); ?>",
    list: {
        match: {
          enabled: true
        }
    },
    getValue: "name",

    template: {
      type: "description",
      fields: {
        description: "type"
      }
    }

};

$("#specialisation").easyAutocomplete(options);
</script>
                  <?php /*
                  <select name="specialisation" id="specialisation" class="form-control selectpick css-select-moz">
<?php 
$dataCategory = (PrdCategory::model()->categoryTree('spesialis', $this->languageID));
?>

                    <option value="">Select Services</option>
                      <?php foreach ($dataCategory as $key => $value): ?>
                        <?php if (count($value['children']) > 0): ?>
                        <optgroup label="<?php echo $value['title'] ?>">
                          <?php foreach ($value['children'] as $k => $v): ?>
                          <option value="<?php echo $v['id'] ?>"><?php echo $v['title'] ?></option>
                          <?php endforeach ?>
                        </optgroup>
                        <?php else: ?>
                        <option value="<?php echo $value['id'] ?>"><?php echo $value['title'] ?></option>
                        <?php endif ?>
                      <?php endforeach ?>
                    </select>
                    <script type="text/javascript">
                      $('#specialisation').val('<?php echo $_GET['specialisation'] ?>');
                    </script>
                    */ ?>
                </div>
              </div>
              <div class="form-group">
                <label for="exampleInputName2">Location based on?</label>
                <div class="d-inline selection v-top">
                  <input type="text" name="suburb" id="suburb-auto" class="form-control selectpick css-select-moz"/>
<script type="text/javascript">
var options = {
    url: function(phrase) {
      return "<?php echo CHtml::normalizeUrl(array('/home/locationList')); ?>";
    },

    ajaxSettings: {
      dataType: "json",
      method: "POST",
      data: {
        dataType: "json"
      }
    },
    preparePostData: function(data) {
      data.phrase = $("#suburb-auto").val();
      return data;
    },
    // url: "<?php echo CHtml::normalizeUrl(array('/home/locationList')); ?>",
    // list: {
    //     match: {
    //       enabled: true
    //     }
    // },

    requestDelay: 400

};

$("#suburb-auto").easyAutocomplete(options);
</script>
                </div>
              </div>
              <button type="submit" class="btn btn-default btn_submit_custom"></button>
            </form>
            <div class="clear height-20"></div>
            <div class="text-center">
              <p>Or <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/index')); ?>">click here</a> to search for a Surgeon</p>
            </div>
            <div class="clear"></div>
          </div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <div class="clear"></div>
</div>
<div class="clear"></div>

<script>
  // $( function() {
  //   $( "#suburb-auto" ).autocomplete({
  //     source: "<?php echo CHtml::normalizeUrl(array('/home/suburb')); ?>"
  //   });
  // } );
</script>
<!-- featured section surgeons -->
<section class="section_default back_surgeons_home mh710">
  <div class="prelatife container">
    <div class="clear h65"></div>
    
    <div class="tops margin-bottom-50">
      <h6>FEATURED TRUSTED SURGEONS</h6>
      <div class="clear"></div>
      <h5 class="smalls hide hidden">We connect you with qualified and trained registered professionals</h5>
      <div class="clear height-40"></div>
<?php
$dataCategory = ViewCategory::model()->findAll('language_id = :language_id AND type = :type AND parent_id = 0 ORDER BY sort ASC', array(':language_id'=>$this->languageID, ':type'=>'spesialis'));
?>
      <div class="block_nav">
        <ul class="list-inline">
          <?php foreach ($dataCategory as $key => $value): ?>
          <li <?php if ($key == 0): ?>class="active"<?php endif ?>><a href="#face_<?php echo Slug::create($value->name) ?>" aria-controls="<?php echo Slug::create($value->name) ?>" role="tab" data-toggle="tab"><?php echo $value->name ?></a></li>
          <?php endforeach ?>
        </ul>
        <div class="clear"></div>
      </div>
    </div>
    <div class="clear height-2"></div>
    <div class="middles">
      
       <div class="tab-content">
          <?php foreach ($dataCategory as $key => $value): ?>
          <div role="tabpanel" class="tab-pane <?php if ($key == 0): ?>active<?php endif ?>" id="face_<?php echo Slug::create($value->name) ?>">
            <div class="box_listing_surgeons1">
              <div class="row owl-carousel owl-theme">
<?php
$dataFeatured = DoctorFeatured::model()->findAll('type = :type', array(':type'=>'Home | '.$value->name));
?>
                <?php foreach ($dataFeatured as $v): ?>
<?php
$value = Doctor::model()->with(array('specialication', 'clinics', 'clinic'))->findByPk($v->doctor_id);
?>
                <div class="col-md-12 col-lg-12 item">
                  <div class="paddings">
                    <div class="items">
                      <div class="picture">
                        <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail', 'id'=>$value->id)); ?>">
                        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(404,179, '/images/doctor/'.$value->cover , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                        </a>
                      </div>
                      <div class="desc">
                        <div class="doctor_p d-inline padding-right-15">
                          <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(82,82, '/images/doctor/'.$value->photo , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                        </div>
                        <div class="doctor_info d-inline">
                          <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail', 'id'=>$value->id)); ?>">
                          <span class="names"><?php echo $value->name ?></span>
                          </a>
                          <p><?php echo $value->certification ?> <br>
                            <?php if ($value->n_review > 0): ?>
                                <?php echo DoctorReviewCategory::model()->createStar($value->rating) ?> &nbsp; <?php echo $value->n_review ?> reviews <br>
                            <?php endif ?>
                             <?php echo $value->clinics[0]->suburb ?></p>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>
                </div>
                <?php endforeach ?>
              </div>
              <!-- end row -->
              <div class="clear"></div>
            </div>
            <!-- end box listing -->
          </div>
          <?php endforeach ?>
        </div>

      <div class="clear"></div>
    </div>
    <div class="clear"></div>

    <div class="h65"></div>
      <div class="tops margin-bottom-50">
        <h6>NEWS & MEDIA</h6>
        <div class="clear height-5"></div>
      </div>
      <div class="middles outer-block-news">
        <div class="blocks_news_data">
<?php
    $criteria = new CDbCriteria;

    $criteria->order = 't.date_input DESC';

    $criteria->group = 't.id';

    $dataFeatured = new CActiveDataProvider('DoctorBlog', array(
      'criteria'=>$criteria,
        'pagination'=>array(
            'pageSize'=>2,
        ),
    ));

?>
          <div class="row">
            <?php foreach ($dataFeatured->getData() as $key => $value): ?>
            <div class="col-md-6">
              <div class="items">
                <div class="picture prelatife"><a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(614,362, '/images/blog/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></a></div>
                <div class="info">
                  <div class="row">
                    <div class="col-md-3">
                      <span class="dates"><a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id)); ?>"><?php echo strtoupper(date('d F Y', strtotime($value->date_input))) ?></a></span>
                    </div>
                    <div class="col-md-9 bdr_right">
                      <div class="titles"><a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id)); ?>"><?php echo $value->title ?></a></div>
                    </div>
                  </div>
                </div>
                <div class="clear"></div>
              </div>
            </div>
            <?php endforeach ?>
          </div>
        </div>
        <div class="clear"></div>
      </div>

      <!-- instagram container -->
      <div class="h65"></div>
      <div class="tops margin-bottom-50">
        <h6>FOLLOW US</h6>
        <div class="clear height-5"></div>
      </div>
      <div class="middles outer-block-feeds">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/instafeed.js/1.4.1/instafeed.min.js"></script>
<script type="text/javascript">
    var feed = new Instafeed({
        get: 'user', 
        userId: '1712530961',
        // tagName: 'sumantour',
        // https://instagram.com/oauth/authorize/?client_id=3c03a0fd2a354ad59621aea34ceeb3be&redirect_uri=http://sumantour.com&response_type=token&scope=public_content+basic 
        // clientId: '1677ed07ddd54db0a70f14f9b1435579',
        clientId: 'c7615b9ef3344cdaa551eedc8a539e98',
        accessToken: '1712530961.c7615b9.3740d2ddf82447e9bb2131bdf82bd049',
        target: 'instafeed',
        sortBy: 'most-recent',
        limit: '100',
        resolution: 'low_resolution',
        //template: '<li><a href="{{link}}" target="_blank"><img src="{{image}}" alt=""></a></li>',
        template: '<div class="item"><div class="col-md-12"><div class="picture prelatife"><a href="{{link}}" target="_blank"><img src="{{image}}" width="300" alt=""></a></div></div></div>',
        after: function(){
        $('.owl-carousel').owlCarousel({
            loop:false,
            margin:0,
            nav:false,
            pagination: true,
            items: 4,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:4
                }
            }
        })

      // $("#instafeed").owlCarousel();
      // var isidataGal =  $('#instafeed').html();
      // var owl = $(".bxslider").html(isidataGal);
 
      // $('.bxslider').bxSlider({
      //   minSlides: 1,
      //   maxSlides: 7,
      //   moveSlides: 1,
      //   slideWidth: 134,
      //   slideMargin: 6,
      //   pager: false,
      //   auto: true,
      //   // dimatikan sementara
      //   // auto: false,
      // });

              // owl.owlCarousel({
              //     items : 6, //10 items above 1000px browser width
              //     itemsDesktop : [1000,5], //5 items between 1000px and 901px
              //     itemsDesktopSmall : [900,3], // betweem 900px and 601px
              //     itemsTablet: [600,2], //2 items between 600 and 0
              //     pagination: false,
              //     itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
              // });

              // add pagination
              // var strPagination = '<div class="arrow-left"><a href="#" class="arrow-left"></a></div>
              //   <div class="arrow-right"><a href="#" class="arrow-right"></a></div>';
              // $('.customNavigation').html(strPagination);

               // Custom Navigation Events
              // $(".customNavigation a.arrow-right").click(function(){
              //     owl.trigger('owl.next');
              //     // alert('ajsdfkljsdklfjdlfkj');
              // });

              // $(".customNavigation a.arrow-left").click(function(){
              //     owl.trigger('owl.prev');
              // });

            },
    });
  feed.run();
</script>
        <div class="block_feed row owl-carousel owl-theme" id="instafeed">
<!-- 1712530961.c7615b9.3740d2ddf82447e9bb2131bdf82bd049 -->
        </div>
        <div class="clear"></div>
      </div>

    <div class="clear height-50"></div>
    <div class="clear"></div>
  </div>
</section>
<!-- End featured section surgeons -->

<div class="clearfix clear"></div>
