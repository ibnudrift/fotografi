<section class="middle-content back-white back-birds-ontheback min-h500">
    <div class="prelatife container">
        <div class="clear height-50"></div><div class="height-50"></div><div class="height-35"></div>
        <div class="insides-cont text-left content-text">
            <h1 class="title-pages text-center">PRODUCT</h1>
            <div class="clear height-5"></div><div class="height-4"></div>
            <div class="lines-back-greycenter-w"></div>
            <div class="clear height-45"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs">
                        <ol class="breadcrumb">
                          <li><a href="#">Home</a></li>
                          <li class="active">Products</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="clear height-45"></div>
            <div class="row">
                <div class="col-md-3 col-lg-3">
                    
                    <div class="left-menu-category">
                        <ul class="list-unstyled">
                            <li><a href="#"><b>Deep Seating Collections</b></a></li>
                            <li><a href="#">Bella Collection</a></li>
                            <li><a href="#">Ciera Collection</a></li>
                            <li><a href="#">Monterey Collection</a></li>
                        </ul>

                        <ul class="list-unstyled">
                            <li><a href="#"><b>Dining and Bar Chairs</b></a></li>
                            <li><a href="#">Bella Dining Chairs</a></li>
                            <li><a href="#">Braxton Folding Chairs</a></li>
                            <li><a href="#">Monterey Dining Chairs</a></li>
                            <li><a href="#">Monterey Bar Chairs</a></li>
                            <li><a href="#">Riviera Folding Sling Chairs</a></li>
                            <li><a href="#">Riviera Stacking Sling Chairs</a></li>
                            <li><a href="#">Sedona Stacking Chairs</a></li>
                            <li><a href="#">SoHo Stacking and Counter Height Chairs</a></li>
                            <li><a href="#">Vienna Dining Chair</a></li>
                        </ul>

                        <ul class="list-unstyled">
                            <li><a href="#"><b>Dining and Bar Tables</b></a></li>
                            <li><a href="#">Brunswick Square and Rectangle Tables</a></li>
                            <li><a href="#">Cambridge Round Folding Cafe Tables</a></li>
                            <li><a href="#">Camden Oval Extension and Fixed Tables</a></li>
                            <li><a href="#">Chelsea Round Extension Table</a></li>
                            <li><a href="#">Chelsea Oval and Rectangular Extension Tables</a></li>
                            <li><a href="#">Newport Square Dining Table</a></li>
                            <li><a href="#">Oxford Round Dining and Bar Tables</a></li>
                            <li><a href="#">Shelburne Dining Tables</a></li>
                            <li><a href="#">SoHo Dining, Extension and Counter Height Tables</a></li>
                        </ul>

                        <ul class="list-unstyled">
                            <li><a href="#"><b>Benches</b></a></li>
                            <li><a href="#">Charleston Backless Benches</a></li>
                            <li><a href="#">Classic Benches and Armchair</a></li>
                            <li><a href="#">Victoria Garden Benches, Armchair and Swing</a></li>
                        </ul>

                        <ul class="list-unstyled">
                            <li><a href="#"><b>Loungers and Occasional Seating</b></a></li>
                            <li><a href="#">Classic Lounger</a></li>
                            <li><a href="#">Riviera Sling Lounger</a></li>
                            <li><a href="#">Adirondack Chair and Footstool</a></li>
                            <li><a href="#">Brittany Rocker</a></li>
                        </ul>

                        <ul class="list-unstyled">
                            <li><a href="#"><b>Occasional Tables</b></a></li>
                            <li><a href="#">Canterbury Round Tables</a></li>
                            <li><a href="#">Ciera Square and Rectangular Tables</a></li>
                            <li><a href="#">Newport Square and Rectangular Tables</a></li>
                        </ul>

                        <ul class="list-unstyled">
                            <li><a href="#"><b>Accessories</b></a></li>
                            <li><a href="#">Flower Boxes</a></li>
                            <li><a href="#">Cushion Box</a></li>
                            <li><a href="#">Umbrellas</a></li>
                            <li><a href="#">Umbrella Bases</a></li>
                            <li><a href="#">Umbrella Reducer Rings</a></li>
                        </ul>
                        <div class="clear"></div>
                            <div class="lines-grey"></div>
                        <div class="clear height-25"></div>
                        <ul class="list-unstyled">
                            <li><a href="#"><b>Collections</b></a></li>
                            <li><a href="#">Ciera</a></li>
                            <li><a href="#">Monterey</a></li>
                            <li><a href="#">Riviera</a></li>
                            <li><a href="#">SoHo</a></li>
                        </ul>

                        <div class="clear"></div>
                    </div>

                </div>
                <!-- End Left content product -->

                <div class="col-md-9 col-lg-9">
                    <div class="exam-big-slideproduct">
                        <div id="carousel-example-productsli" class="carousel fade" data-ride="carousel">
                          <!-- Wrapper for slides -->
                          <div class="carousel-inner" role="listbox">
                            <div class="item active">
                              <img src="<?php echo $this->assetBaseurl; ?>exam-product-slides.jpg" alt="">
                            </div>
                            <div class="item">
                              <img src="<?php echo $this->assetBaseurl; ?>exam-product-slides.jpg" alt="">
                            </div>

                          </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear height-30"></div><div class="height-2"></div>
                    <div class="list-products-categorylist">
                        <div class="row">
                            <?php if ($model): ?>
                            <?php foreach ($model as $key => $value): ?>
                            <div class="col-md-4">
                                <div class="itemss">
                                    <div class="pict"><img class="img-responsive" src="<?php echo $this->assetBaseurl; ?><?php echo $value['pict'] ?>" alt=""></div>
                                    <div class="clear height-10"></div>
                                    <div class="desc">
                                        <span class="name"><?php echo $value['name'] ?></span>
                                        <div class="clear"></div>
                                        <a href="#" class="vw-rproducts">View Products</a>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach ?>
                            <?php endif ?>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="clear"></div>
                </div>
                <!-- End right cont product -->
            </div>



            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
</section>
<div class="clear height-30"></div>