<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static page_products">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      
      <div class="content-text insides_static outers_page_detail_product">
        <div class="tops">
          <div class="row">
            <div class="col-md-9">
              <div class="n_breadcrumbs">
                <ol class="breadcrumb">
                  <li><a href="#">SHOP</a></li>
                  <li class="active">Post Surgery Skincare Treatment</li>
                </ol>
                <div class="clear"></div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="backs_btn_product text-right"><a href="#">BACK</a></div>
            </div>
          </div>
        </div>
        <!-- end tops -->
        <div class="middle">
          <div class="details_product_box_content">
            
          <div class="row">
            <div class="col-md-6">
              <div class="picture"><img src="<?php echo $this->assetBaseurl ?>pict_detail_product_big.jpg" alt="" class="img-responsive"></div>
            </div>
            <div class="col-md-6">
              <div class="description">
                <h4 class="titles">Magnolia Rosemary - Therapeutic Minerals Body Soak</h4>
                <div class="clear"></div>
                <form action="#">
                  <div class="child_desription_product">
                    <table class="table">
                      <tr>
                        <td>Price </td>
                        <td><b>$120</b></td>
                      </tr>
                      <tr>
                        <td>Stock </td>
                        <td><b>Available</b></td>
                      </tr>
                      <tr>
                        <td>Delivery </td>
                        <td><b>Free</b></td>
                      </tr>
                      <tr>
                        <td>Quantity</td>
                        <td>
                          <input type="number" class="form-control quantity" name="qty" value="1">
                        </td>
                      </tr>
                    </table>
                    <div class="clear"></div>
                  </div>
                  <button type="submit" class="btn btn-default btns_purple_cart back_purple_defaults_tl">ADD TO CART</button> &nbsp;&nbsp;
                  <button type="button" class="btn btn-default btns_purple_cart back_purple_defaults_tl">BUY IT NOW</button>
                  <div class="clear"></div>
                </form>
                <div class="clear height-40"></div>
                <p><strong>Product Description</strong><br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus neque, vulputate vitae finibus quis, faucibus vel mi. Nulla tincidunt efficitur justo in egestas. Curabitur molestie a ipsum eu bibendum.</p>

                <p>Donec at tincidunt ante. Aliquam neque metus, lobortis a nunc id, pharetra tempus metus. Proin eleifend condimentum placerat.</p>

                <p><strong>Tip</strong><br />
                Nullam vehicula libero a leo scelerisque ornare. Nulla lacinia dictum eros, a egestas quam posuere nec. Donec urna odio, commodo ac aliquam pellentesque, varius vitae nunc. Nulla suscipit efficitur tellus, vitae lacinia purus efficitur et.</p>

                <p><strong>Function</strong><br />
                Detoxing, Energizing, Stress Relief, Muscle Ache and Cramp Relief</p>

                <p><strong>Skin Type</strong><br />
                All</p>

                <p><strong>Emotional Properties</strong><br />
                Soothing, Relaxing, Uplifting</p>

                <p><strong>Suitable For</strong><br />
                All Ages</p>

                <p><strong>Usage Directions</strong><br />
                Add to running bath, immerse yourself and enjoy. May also be used as an exfoliating body scrub.</p>

                <p><strong>Ingredients</strong><br />
                Epsom salt (USP grade), kaolin clay, essential oils of peppermint, spearmint &amp; eucalyptus, peppermint leaves, green tea, oatmeal.</p>


                <div class="clear"></div>
              </div>
            </div>
          </div>
          <div class="clear"></div>
          </div>
          <!-- end detail product content -->

          <div class="clear"></div>
        </div>
        <!-- end middle -->
        <div class="clear"></div>

        <div class="clear"></div>
      </div>
      <!-- end page detal product -->

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>