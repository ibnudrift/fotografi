<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">

    <div class="pict_full illustration_picture prelatife mxh480">
      <img src="<?php echo $this->assetBaseurl ?>ill-surgeons-1.jpg" alt="" class="img-responsive">
    </div>
    <div class="prelatife middle-bottom_surgeonsindex">
      <div class="clear height-50"></div>
      <div class="prelatife container">
        <div class="content-text insides_static">
          <h1 class="title_page shadow">SURGEONS</h1>
          <div class="clear"></div>
          <h3 class="tagline shadow">Choose a Surgeon You Can Trust</h3>
          <div class="clear"></div>

          <!-- Start content surgeons -->
          <div class="insides_c_content_surgeons">
            
            <div class="tops">
              <div class="filter_surgeons_top">
                <form action="#" class="form-inline">
                   <div class="form-group w25p">
                    <label class="sr-only" for="">Services</label>
                    <select name="#" id="" class="form-control">
                      <option value="">Select Services</option>
                      <option value="1">Services 1</option>
                      <option value="2">Services 2</option>
                      <option value="3">Services 3</option>
                    </select>
                  </div>
                   <div class="form-group w25p">
                    <label class="sr-only" for="">Doctor Name</label>
                    <input type="email" class="form-control" id="" placeholder="Doctor Name">
                  </div>
                   <div class="form-group w25p">
                    <label class="sr-only" for="">Nearest Location</label>
                    <input type="email" class="form-control empty" id="" placeholder="&#xf041; Nearest Location">
                  </div>
                  <div class="form-group w25p">
                    <button class="btn btn-default btns_submit_filter">
                      <i class="fa fa-search"></i> SEARCH
                    </button>
                  </div>
                </form>
                <div class="clear"></div>
              </div>

              <div class="clear height-15"></div>
            </div>
            <!-- end tops -->

            <div class="middles">

              <div class="box_sorting text-left">
                <span class="d-inline">Sort by:</span> 
                &nbsp;&nbsp;
                <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/index')); ?>" class="active">Alphabetical order</a>&nbsp;&nbsp;
                <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/index2')); ?>">Most reviewed</a>&nbsp;&nbsp;
                <a href="#">Date joined</a>
              </div>
              <div class="clear height-30"></div>
              
              <div class="box_listing_surgeons1">
                <div class="row">
                  <div class="col-md-4">
                    <div class="items">
                      <div class="picture">
                        <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail')); ?>">
                        <img src="<?php echo $this->assetBaseurl ?>ex-item-surgeons1.jpg" alt="" class="img-responsive">
                        </a>
                      </div>
                      <div class="desc">
                        <div class="doctor_p d-inline padding-right-15">
                          <img src="<?php echo $this->assetBaseurl ?>pict-doctor-surgeons.png" alt="" class="img-responsive">
                        </div>
                        <div class="doctor_info d-inline">
                          <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail')); ?>">
                          <span class="names">Alexander Phoon</span>
                          </a>
                          <p>MBBS, FRACS <br>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i> &nbsp; 50 reviews <br>
                             Wooloomooloo, NSW 2022</p>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="items">
                      <div class="picture">
                        <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail')); ?>">
                        <img src="<?php echo $this->assetBaseurl ?>ex-item-surgeons2.jpg" alt="" class="img-responsive">
                        </a>
                      </div>
                      <div class="desc">
                        <div class="doctor_p d-inline padding-right-15">
                          <img src="<?php echo $this->assetBaseurl ?>pict-doctor-surgeons2.png" alt="" class="img-responsive">
                        </div>
                        <div class="doctor_info d-inline">
                          <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail')); ?>">
                          <span class="names">Alexander Phoon</span>
                          </a>
                          <p>MBBS, FRACS <br>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i> &nbsp; 50 reviews <br>
                             Wooloomooloo, NSW 2022</p>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="items">
                      <div class="picture">
                        <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail')); ?>">
                        <img src="<?php echo $this->assetBaseurl ?>ex-item-surgeons3.jpg" alt="" class="img-responsive">
                        </a>
                      </div>
                      <div class="desc">
                        <div class="doctor_p d-inline padding-right-15">
                          <img src="<?php echo $this->assetBaseurl ?>pict-doctor-surgeons3.png" alt="" class="img-responsive">
                        </div>
                        <div class="doctor_info d-inline">
                          <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail')); ?>">
                          <span class="names">Alexander Phoon</span>
                          </a>
                          <p>MBBS, FRACS <br>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i> &nbsp; 50 reviews <br>
                             Wooloomooloo, NSW 2022</p>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="items">
                      <div class="picture">
                        <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail')); ?>">
                        <img src="<?php echo $this->assetBaseurl ?>ex-item-surgeons4.jpg" alt="" class="img-responsive">
                        </a>
                      </div>
                      <div class="desc">
                        <div class="doctor_p d-inline padding-right-15">
                          <img src="<?php echo $this->assetBaseurl ?>pict-doctor-surgeons.png" alt="" class="img-responsive">
                        </div>
                        <div class="doctor_info d-inline">
                          <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail')); ?>">
                          <span class="names">Alexander Phoon</span>
                          </a>
                          <p>MBBS, FRACS <br>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i> &nbsp; 50 reviews <br>
                             Wooloomooloo, NSW 2022</p>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="items">
                      <div class="picture">
                        <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail')); ?>">
                        <img src="<?php echo $this->assetBaseurl ?>ex-item-surgeons5.jpg" alt="" class="img-responsive">
                        </a>
                      </div>
                      <div class="desc">
                        <div class="doctor_p d-inline padding-right-15">
                          <img src="<?php echo $this->assetBaseurl ?>pict-doctor-surgeons2.png" alt="" class="img-responsive">
                        </div>
                        <div class="doctor_info d-inline">
                          <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail')); ?>">
                          <span class="names">Alexander Phoon</span>
                          </a>
                          <p>MBBS, FRACS <br>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i> &nbsp; 50 reviews <br>
                             Wooloomooloo, NSW 2022</p>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="items">
                      <div class="picture">
                        <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail')); ?>">
                        <img src="<?php echo $this->assetBaseurl ?>ex-item-surgeons6.jpg" alt="" class="img-responsive">
                        </a>
                      </div>
                      <div class="desc">
                        <div class="doctor_p d-inline padding-right-15">
                          <img src="<?php echo $this->assetBaseurl ?>pict-doctor-surgeons3.png" alt="" class="img-responsive">
                        </div>
                        <div class="doctor_info d-inline">
                          <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail')); ?>">
                          <span class="names">Alexander Phoon</span>
                          </a>
                          <p>MBBS, FRACS <br>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i> &nbsp; 50 reviews <br>
                             Wooloomooloo, NSW 2022</p>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>

                </div>
                <!-- end row -->

                <div class="clear"></div>
              </div>
              <!-- end box listing -->
              <div class="clear height-40"></div>
              <div class="lines-grey"></div>
              <div class="clear height-50"></div>
              <h2 class="title_page">SURGEONS BLOGS</h2>
              <div class="clear"></div>

              <div class="clear height-45"></div>
              
              <div class="blocks_outer_list_blog pinside">
                <div class="row">
                  <div class="col-md-4">
                    <div class="items prelatife">
                      <div class="pict prelatife"><img src="<?php echo $this->assetBaseurl ?>ex_blog_inside_1.jpg" alt="" class="img-responsive"></div>
                      <div class="block_purple desription">
                        <span class="title">
                          Beauty Surgery trends in the early age of 20s? Good or Bad?
                        </span>
                        <span class="date">
                          11 June 2016
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="items prelatife">
                      <div class="pict prelatife"><img src="<?php echo $this->assetBaseurl ?>ex_blog_inside_2.jpg" alt="" class="img-responsive"></div>
                      <div class="block_purple desription">
                        <span class="title">
                          Beauty Surgery trends in the early age of 20s? Good or Bad?
                        </span>
                        <span class="date">
                          11 June 2016
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="items prelatife">
                      <div class="pict prelatife"><img src="<?php echo $this->assetBaseurl ?>ex_blog_inside_2.jpg" alt="" class="img-responsive"></div>
                      <div class="block_purple desription">
                        <span class="title">
                          Beauty Surgery trends in the early age of 20s? Good or Bad?
                        </span>
                        <span class="date">
                          11 June 2016
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="clear"></div>
              </div>
              <!-- end list blogs surgeons -->

              <div class="clear"></div>
            </div>
            <!-- end middles -->
            <div class="clear"></div>
          </div>
          <!-- End content surgeons -->
          

          <div class="clear"></div>
        </div>

        <div class="clear height-50"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear height-25"></div>
  </div>
</section>