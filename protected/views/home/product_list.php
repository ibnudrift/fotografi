<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static page_products">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      
      <div class="pict_full"><img src="<?php echo $this->assetBaseurl ?>picture_toplanding_productlist.jpg" alt="" class="img-responsive"></div>
      <div class="clear height-50"></div><div class="height-10"></div>
      <div class="content-text insides_static outers_page_listing_product">
        <h1 class="title_page">SHOP</h1>
        <div class="clear height-35"></div>

        <!-- start list product_list -->
        <h3 class="titles text-center">Pre Surgery Skincare Treatment</h3>

        <div class="box_listing_products_listdata">
          <div class="row">
            <?php for ($j=1; $j < 3; $j++) {  ?>
          <?php for ($i=1; $i <= 3; $i++) {  ?>
          <div class="col-md-4">
            <div class="items">
              <div class="picture">
                <a href="#">
                  <img src="<?php echo $this->assetBaseurl ?>ex_item_listproduct_t<?php echo $i ?>.jpg" alt="" class="img-responsive tengah">
                </a>
              </div>
              <div class="desc">
                <a href="#"><span class="title">Angelica Duo</span></a>
                <div class="clear"></div>
                <span class="price">$ 24.00</span>
              </div>
            </div>
          </div>
          <?php } ?>
          <?php } ?>
          </div>
          <div class="clear"></div>
        </div>
        <!-- // end list product_list -->

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>