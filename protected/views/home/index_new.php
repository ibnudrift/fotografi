<section class="outers_content_wrap pg_home">

  <div class="tops_filter_block_category">
    <div class="prelatife container">
      <div class="inner_block text-center">
        <ul class="list-inline">
          <li class="active"><a href="#">Semua Spesialisasi</a></li>
          <li><a href="#">Wedding & Family</a></li>
          <li><a href="#">Profil & Perusahaan</a></li>
          <li><a href="#">Architecture & Interior</a></li>
          <li><a href="#">Makanan, Minuman & Produk</a></li>
          <li><a href="#">Drone</a></li>
          <li><a href="#">Sport & Event</a></li>
          <li><a href="#">Seni & Pemandangan</a></li>
          <li><a href="#">Video</a></li>
        </ul>
      </div>
    </div>
  </div>
  

  <div class="outers_block_middle lists_det_fotog">
    <div class="row">
        <?php for ($i=1; $i < 33; $i++) { ?>
        <div class="col-md-3 col-xs-3">
        <img src="https://loremflickr.com/500/500" alt="" class="img-responsive">
        </div>
        <?php } ?>
    </div>
    <div class="clear"></div>
  </div>

</section>
  
  <section class="blocks_blog_home text-center">
    <div class="prelatife container_inside">
      <div class="inners_blog">
        <h3>ARTIKEL & BLOG</h3>
        <div class="height-30"></div>

        <div class="lists_blog_def row default">
          <?php for ($i=1; $i < 4; $i++) { ?>
            <div class="col-md-4">
            <div class="items">
              <div class="picture"><img src="https://placehold.it/366x366" alt="" class="img-resposnive"></div>
              <div class="info">
                <h5>SLIM ARRONS</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit aut rerum numquam hic eum, aperiam fuga, pariatur repellendus.</p>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </section>

  <section class="about_me home_pg">
    <div class="prelatife container_inside">
      <div class="inners_blocks">
        <h3>TENTANG FOTOGRAFER.ID</h3>
        <div class="height-10"></div>
        <div class="lines"></div>
        <div class="height-35"></div>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia obcaecati, sit. Aut maxime iure nostrum earum fugiat magnam voluptates, placeat nam vitae praesentium quos, illum laudantium. Voluptates qui officiis ratione!</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit quisquam officia veritatis quasi maxime labore iusto sequi error dignissimos, dolores similique possimus culpa facere enim veniam distinctio repellendus in rem.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam quasi officiis quos sunt porro rem, accusantium vitae, laboriosam tenetur ducimus quo error, nihil, laborum non delectus eum. Porro reiciendis, sapiente!</p>
      </div>
    </div>
  </section>