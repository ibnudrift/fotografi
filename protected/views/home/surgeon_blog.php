<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>

      <div class="content-text insides_static">
        <h1 class="title_page">FOR PROFESSIONAL</h1>
        <div class="clear"></div>
        <h3 class="tagline"><?php echo $this->setting['blog_prof_title'] ?></h3>
        <div class="clear"></div>
        <div class="row">
          <div class="col-md-9 text-left">
            <div class="left_cont">
              <div class="mw906">
                <h4><?php echo nl2br($this->setting['blog_prof_subtitle']) ?></h4>
                <div class="clear"></div>
                <?php echo $this->setting['blog_prof_content'] ?>

                <div class="clear"></div>
              </div>
            </div>

          </div>
          <div class="col-md-3 text-left">
            <div class="right_cont">
              <div class="padding-left-25">
                  <span class="sub_page_title">FOR PROFESSIONAL</span>
              </div>
              <div class="clear"></div>
              <div class="right_sub_menu">
                <ul class="list-unstyled">
                  <li><a href="<?php echo CHtml::normalizeUrl(array('/home/whyClinic')); ?>"><?php echo $this->setting['why_clinic_title'] ?></a></li>
                  <li class="active"><a href="<?php echo CHtml::normalizeUrl(array('/home/surgeons_blog')); ?>"><?php echo $this->setting['blog_prof_title'] ?></a></li>
                  <li><a href="<?php echo CHtml::normalizeUrl(array('home/clinic_faq')); ?>"><?php echo $this->setting['clinic_faq_title'] ?></a></li>
                </ul>
                <div class="clear"></div>
              </div>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>