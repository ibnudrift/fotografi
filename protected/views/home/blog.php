<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-15"></div>
      
      <div class="clear"></div>
<section class="section_default back_grey mh850 blocks_news_home">
  <div class="prelatife container">
    <div class="tops margin-bottom-50">
      <h6>EXPLORE MORE</h6>
      <!-- <div class="clear"></div>
      <h5>BLOG</h5> -->
      <div class="clear height-30"></div>

      <div class="block_nav" style="max-width: 180px;">
        <ul class="list-inline">
          <li class="active"><a href="javascript:;" style="cursor:default;">BLOG</a></li>
        </ul>
        <div class="clear"></div>
      </div>

    </div>
    <div class="clear height-2"></div>
    <div class="middles">
      
      <!-- start list news -->
      <div class="listing_news_default">
        <div class="row">
          <div class="col-md-6 col-lg-6">
            <div class="items featured">
              <div class="picts"><a href="<?php echo CHtml::normalizeUrl(array('/home/blogDetail/')); ?>"><img src="<?php echo $this->assetBaseurl ?>news-1.jpg" alt="" class="img-responsive"></a></div>
              <div class="clear"></div>
              <article>
                <div class="title"><a href="<?php echo CHtml::normalizeUrl(array('/home/blogDetail/')); ?>">Which of your favourite Hollywood stars admitted to undergoing Plastic Surgery?</a></div>
                <div class="clear"></div>
                <span class="dates">15 June 2016</span>
              </article>
            </div>
          </div>
          <div class="col-md-6 col-lg-6">
            <div class="items featured">
              <div class="picts"><a href="<?php echo CHtml::normalizeUrl(array('/home/blogDetail/')); ?>"><img src="<?php echo $this->assetBaseurl ?>news-1.jpg" alt="" class="img-responsive"></a></div>
              <div class="clear"></div>
              <article>
                <div class="title"><a href="<?php echo CHtml::normalizeUrl(array('/home/blogDetail/')); ?>">Which of your favourite Hollywood stars admitted to undergoing Plastic Surgery?</a></div>
                <div class="clear"></div>
                <span class="dates">15 June 2016</span>
              </article>
            </div>
          </div>
        </div>
        <div class="clear height-15"></div>
        
        <?php for ($i=1; $i < 5; $i++) { ?>
          <div class="row">
              <div class="col-md-3">
                <div class="items">
                  <div class="picts"><img src="<?php echo $this->assetBaseurl ?>news-2.jpg" alt="" class="img-responsive"></div>
                  <div class="clear"></div>
                  <article>
                    <div class="title">Beauty Surgery trends in the early age of 20s? Good or Bad?</div>
                    <div class="clear"></div>
                    <span class="dates">11 June 2016</span>
                  </article>
                </div>
              </div>
              <div class="col-md-3">
                <div class="items">
                  <div class="picts"><img src="<?php echo $this->assetBaseurl ?>news-3.jpg" alt="" class="img-responsive"></div>
                  <div class="clear"></div>
                  <article>
                    <div class="title">When movie star Kiera Knightley shares her surgery wish!</div>
                    <div class="clear"></div>
                    <span class="dates">11 June 2016</span>
                  </article>
                </div>
              </div>
              <div class="col-md-3">
                <div class="items">
                  <div class="picts"><img src="<?php echo $this->assetBaseurl ?>news-4.jpg" alt="" class="img-responsive"></div>
                  <div class="clear"></div>
                  <article>
                    <div class="title">How to maintain your surgery result in the best way</div>
                    <div class="clear"></div>
                    <span class="dates">13 June 2016</span>
                  </article>
                </div>
              </div>
              <div class="col-md-3">
                <div class="items">
                  <div class="picts"><img src="<?php echo $this->assetBaseurl ?>news-5.jpg" alt="" class="img-responsive"></div>
                  <div class="clear"></div>
                  <article>
                    <div class="title">How to maintain your surgery result in the best way</div>
                    <div class="clear"></div>
                    <span class="dates">13 June 2016</span>
                  </article>
                </div>
              </div>
            </div>
          <?php } ?>


        <div class="clear"></div>
      </div>
      <!-- end list news -->

      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>