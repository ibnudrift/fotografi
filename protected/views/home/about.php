<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      
      <!-- <div class="pict_full illustration_picture"><img src="<?php echo $this->assetBaseurl ?>ill-about-1.jpg" alt="" class="img-responsive"></div>
      <div class="clear height-50"></div> -->

      <div class="content-text insides_static">
        <h1 class="title_page">ABOUT US</h1>
        <div class="clear"></div>
        <h3 class="tagline">Who is Trusted Surgeons</h3>
        <div class="clear"></div>
        <div class="row">
          <div class="col-md-9 text-left">
            <div class="left_cont">
              <div class="mw906">
                <h4>As you get started, it&#39;s completely normal to have a lot of questions or even feel overwhelmed.</h4>
                <div class="pict_full"><img src="<?php echo $this->assetBaseurl ?>about-1.jpg" alt="" class="img-responsive"></div>
                <div class="clear"></div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent iaculis ligula non libero tincidunt pellentesque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin consectetur massa nunc, sed vehicula leo vestibulum at. Mauris sit amet sapien leo. Nam volutpat quis nunc quis sollicitudin. Nam elementum tortor ante, vitae dignissim ipsum dignissim vel. Praesent sodales, dui volutpat commodo imperdiet, quam ipsum lacinia tellus, in lobortis nunc tellus quis felis. Mauris non neque vehicula, maximus nisl nec, tincidunt est. Ut venenatis turpis at iaculis sollicitudin. Fusce euismod fringilla egestas. Nunc tempus enim id massa egestas, at eleifend neque malesuada. Curabitur blandit, mauris quis efficitur dictum, risus tellus euismod nibh, rhoncus pellentesque odio elit a elit. Pellentesque vel ligula a justo dapibus fringilla. Ut eget nunc vel sem tincidunt commodo.</p>

                <p>Duis venenatis vitae elit in euismod. Nulla venenatis orci sed urna suscipit, vitae posuere sapien scelerisque. Donec in facilisis purus, sed mattis est. Suspendisse non pellentesque sapien, non tempor nibh. Donec mi felis, auctor sed mauris blandit, maximus luctus sem. Vestibulum condimentum purus vel rhoncus rhoncus. Sed vestibulum tortor at felis maximus, eu lobortis eros dapibus. Mauris rhoncus purus sit amet lectus scelerisque, id sodales lorem suscipit.</p>

                <p>Nulla eu nunc neque. Quisque eget ultrices massa, rutrum viverra turpis. Fusce auctor sapien sit amet dictum mollis. Maecenas et ex aliquam, sagittis quam non, molestie purus. Integer et turpis sed orci ultrices dapibus at vel nibh. Integer pharetra, ligula at malesuada tristique, velit neque congue nunc, mattis vestibulum ipsum est id lacus. Etiam auctor at ligula id euismod. Duis luctus tellus eget auctor lobortis. Quisque quis sagittis risus, eget laoreet tortor. Maecenas vel orci quis neque tincidunt facilisis. In ultrices erat sapien. Ut dignissim enim in augue ullamcorper, in commodo leo aliquam. Maecenas odio lorem, luctus eu tortor non, auctor dictum enim.</p>

                <p>Duis sem felis, imperdiet auctor gravida ac, faucibus sit amet lacus. Nunc id rutrum ex. Integer varius dolor volutpat quam tincidunt lobortis. Donec bibendum, neque et facilisis dignissim, tellus quam auctor ante, quis tincidunt risus libero et purus. Cras luctus eget augue a dapibus. Proin tempor sapien at lectus posuere, nec tristique dolor suscipit. Pellentesque hendrerit tellus nec vehicula pulvinar. Fusce efficitur nisl ex, at dignissim mi accumsan vel. Proin sed odio non nibh eleifend congue vel id risus. Etiam diam urna, tempor et elit non, posuere vehicula lacus. Nunc sed tristique massa, ac convallis augue. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In finibus lacinia ligula, eu vestibulum libero auctor vitae. Donec condimentum eleifend quam. Quisque non diam quis sapien scelerisque cursus.</p>

                <div class="clear"></div>
              </div>
            </div>

          </div>
          <div class="col-md-3 text-left">
            <div class="right_cont">
              <div class="padding-left-25">
                  <span class="sub_page_title">ABOUT US</span>
              </div>
              <div class="clear"></div>
              <div class="right_sub_menu">
                <ul class="list-unstyled">
                  <li class="active"><a href="#">Who is Trusted Surgeons</a></li>
                  <li><a href="#">Our Mission</a></li>
                  <li><a href="#">Why Choose Trusted Surgeons</a></li>
                  <li><a href="#">About Me</a></li>
                </ul>
                <div class="clear"></div>
              </div>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>