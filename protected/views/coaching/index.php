<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static coaching">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      <div class="height-5"></div>

      <div class="content-text insides_static text-center">
        <div class="mw903 tengah">
          <h1 class="title_page">COACHING</h1>
          <div class="clear"></div>
          <h3 class="tagline"><?php echo nl2br($this->setting['coaching_title']) ?></h3>
          <div class="clear"></div>
          <div class="pict_full">
            <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(903,1000, '/images/static/'.$this->setting['coaching_image'] , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive">
          </div>
          <?php echo $this->setting['coaching_content'] ?>

          <div class="clear"></div>
        </div>
        

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <!-- end middle content back pattern -->
    <div class="block_rate_services_coaching back-white">
      <div class="prelatife container">
        <div class="insides content-text insides_static text-center p2">
          <h1 class="title_page"><?php echo $this->setting['coaching_service_title'] ?></h1>
          <div class="clear height-15"></div>
          <?php echo $this->setting['coaching_service_content'] ?>
          <div class="clear height-25"></div>

          <div class="list_coaching_service_rate">
            <div class="row">
              <div class="col-md-6">
                <div class="items">
                  <div class="pict"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(617,206, '/images/static/'.$this->setting['coaching_service_1_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></div>
                  <div class="description">
                      <div class="hdescbottom">
                        <span>
                          <?php echo $this->setting['coaching_service_1_title'] ?> <br>
                          <b><?php echo $this->setting['coaching_service_1_rate'] ?></b>
                        </span>
                       <?php echo $this->setting['coaching_service_1_content'] ?>
                        </div>
                        <div class="clear height-0"></div>
                        <div class="text-center">
                          <a href="<?php echo CHtml::normalizeUrl(array('detail', 'id'=>1)); ?>" class="btn btn-default btn_custom_default_purple_b">BOOKING ONLINE</a>
                          <!-- <div class="clear height-15"></div>
                          <a href="<?php // echo CHtml::normalizeUrl(array('/contact/index')); ?>" class="btn btn-default btn_custom_default_purple_b">INQUIRE NOW</a> -->
                        </div>

                    <div class="clear"></div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="items">
                  <div class="pict"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(617,206, '/images/static/'.$this->setting['coaching_service_2_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></div>
                  <div class="description">
                      <div class="hdescbottom">
                        <span>
                          <?php echo $this->setting['coaching_service_2_title'] ?> <br>
                          <b><?php echo $this->setting['coaching_service_2_rate'] ?></b>
                        </span>
                       <?php echo $this->setting['coaching_service_2_content'] ?>

                        </div>
                        <div class="clear height-0"></div>
                        <div class="text-center">
                            <!-- <a href="<?php // echo CHtml::normalizeUrl(array('detail', 'id'=>2)); ?>" class="btn btn-default btn_custom_default_purple_b">BOOKING ONLINE</a>
                            <div class="clear height-15"></div> -->
                            <a href="<?php echo CHtml::normalizeUrl(array('/contact/index')); ?>" class="btn btn-default btn_custom_default_purple_b">INQUIRE NOW</a>
                        </div>

                    <div class="clear"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="clear"></div>
          </div>
          <!-- end list coaching service rate -->
          <div class="clear height-50"></div>
          <div class="height-10"></div>

          <div class="mw1021 tengah fz16_p">
              <?php echo $this->setting['coaching_footer1'] ?>

            <div class="clear"></div>
          </div>
          <div class="mw1021 tengah">
              <div class="blocks_purple_bottom_coaching_content">
                <?php echo $this->setting['coaching_footer2'] ?>
                <div class="clear"></div>
              </div>
          </div>




          <div class="clear"></div>
        </div>

        <div class="clear"></div>
      </div>
    </div>
    <!-- end back white bottom -->
    <div class="clear"></div>
  </div>
</section>