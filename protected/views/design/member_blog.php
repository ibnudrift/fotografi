<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="inside  s sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static page_member_s_content">
        <h1 class="title_page">MEMBER AREA</h1>
        <div class="clear height-50"></div> <div class="clear height-5"></div>
        <div class="clear"></div>

        <div class="row default box_outers_dashboardmember">
          <div class="col-md-3">
            <div class="lefts_cont_member">
              <ul class="list-unstyled">
                <li><a href="<?php echo CHtml::normalizeUrl(array('/design/memberE')); ?>">Manage Account</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/design/memberClinic')); ?>">Manage Clinic</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/design/memberReview')); ?>">Manage Reviews</a></li>
                <li class="active"><a href="<?php echo CHtml::normalizeUrl(array('/design/memberBlog')); ?>">Manage Messages</a></li>
              </ul>
              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
          <div class="col-md-9">
            <div class="rights_cont_member">
              
              <!-- start review cont -->
              <div class="list_box_default_member_white blogs">
                <?php for ($i=0; $i < 3; $i++) { ?>
                <div class="items prelatife">
                  <div class="padding">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="picture"><img src="http://placehold.it/195x135" alt="" class="img-responsive"></div>
                      </div>
                      <div class="col-md-9">
                        <h4 class="titles_s">Make Me a Yummy Mummy! - Castle Hill, AU</h4>
                          <div class="clear"></div>
                          <div class="blocks_bottom_d">
                            <div class="pt0">
                              <div class="row prelatife">
                                <div class="col-md-9">
                                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis id dui metus. In placerat arcu tortor, quis condimentum elit molestie ac. Sed dapibus accumsan arcu ac sodales.</p>
                                  <span class="dates">Posted time: 28/07/2015</span>
                                </div>
                                <div class="col-md-3 border-left h127">
                                  <div class="buttons_d padding-left-15">
                                    <a class="btn btn-default btn-link" href="#" role="button"><i class="fa fa-search"></i>&nbsp;&nbsp;Preview</a>
                                    <div class="clear height-3"></div>
                                    <a class="btn btn-default btn-link" href="#" role="button"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit</a>
                                    <div class="clear height-3"></div>
                                    <a class="btn btn-default btn-link" href="#" role="button"><i class="fa fa-times"></i>&nbsp;&nbsp;Delete</a>
                                  </div>
                                  <div class="clear"></div>
                                </div>
                              </div>

                              <div class="clear"></div>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                   
                </div>
                <?php } ?>

                <div class="clear"></div>
              </div>
              <!-- end review cont -->

              <div class="clear height-30"></div>
              <div class="box_form_reviewc">
                <h5 class="s_title">Add New Post</h5>
                <div class="clear height-20"></div>
                <div class="well" style="margin:0;">
                  <form action="#" class="form-horizontal">
                    <div class="form-group">
                      <label for="" class="control-label col-sm-2">Title</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control">
                      </div>
                    </div>

                    <div class="form-group m-0">
                      <div class="row default">
                        <div class="col-sm-12 col-md-12">
                          <textarea name="" cols="6" class="form-control redactor"></textarea>
                        </div>
                      </div>
                    </div>
                  </form>
                  <div class="clear"></div>
                </div>
                <div class="clear height-15"></div>
                <button type="submit" class="btn btn-default btn_purple_member defaults">SAVE / PUBLISH</button>

                <div class="clear"></div>
              </div>
              <!-- end box form_review -->

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
          <!-- end col md 12 -->
        </div>
        <div class="clear height-20"></div>

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>

<script src="//cdn.ckeditor.com/4.5.10/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'editor1' );
</script>