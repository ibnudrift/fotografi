<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="inside  s sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static page_member_s_content">
        <h1 class="title_page">MEMBER AREA</h1>
        <div class="clear height-50"></div> <div class="clear height-5"></div>
        <div class="clear"></div>

        <div class="row default box_outers_dashboardmember">
          <div class="col-md-3">
            <div class="lefts_cont_member">
              <ul class="list-unstyled">
                <li><a href="<?php echo CHtml::normalizeUrl(array('/design/memberE')); ?>">Manage Account</a></li>
                <li class="active"><a href="<?php echo CHtml::normalizeUrl(array('/design/memberClinic')); ?>">Manage Clinic</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/design/memberReview')); ?>">Manage Reviews</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/design/memberBlog')); ?>">Manage Messages</a></li>
              </ul>
              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
          <div class="col-md-9">
            <div class="rights_cont_member">
              
              <!-- Start box manages_clinic -->
              <div class="outers_p_manageclinic">
                <div class="tops">
                  <div class="picture_ill pict_full"><img src="<?php echo $this->assetBaseurl ?>picture1-clinic-top.jpg" alt="" class="img-responsive"></div>
                  <div class="back-white prelatife h55 fdesc_top">
                    <div class="insides prelatife">
                      <div class="cont_abs_tops">
                        <div class="row">
                          <div class="col-md-2">
                            <div class="pic_doctor">
                              <img src="<?php echo $this->assetBaseurl ?>exam_pict_doctor2.jpg" alt="">
                            </div>
                          </div>
                          <div class="col-md-10">
                            <div class="padding-left-25">
                              <form action="#" method="post">
                                <div class="form-group">
                                  <div class="mw395">
                                    <input type="text" class="form-control" placeholder="Doctor&rsquo;s Name">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="mw395">
                                    <input type="text" class="form-control" placeholder="Certification">
                                  </div>
                                </div>
                                <div class="clear height-20"></div>

                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <button class="btn_purple_member btn btn-default defaults">Change Profile Photo</button>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="fright">
                                        <button class="btn_purple_member btn btn-default defaults">Change Cover Photo</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </form>
                              <!-- end forms -->
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- end cont abs top -->
                      <div class="clear"></div>
                    </div>
                  </div>
                  <!-- end backwhite -->
                  <div class="clear"></div>
                </div>
                <!-- end top -->
                <div class="clear height-35"></div>

                <div class="middles">
                  <!-- start end box middles -->
                  <div class="box_clinic_tab_cont">
                    <div>
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#clinic_info" aria-controls="clinic_info" role="tab" data-toggle="tab">CLINIC CONTACT INFO</a></li>
                        <li role="presentation"><a href="#clinic_special" aria-controls="clinic_special" role="tab" data-toggle="tab">SELECT SPECIALISATION</a></li>
                        <li role="presentation"><a href="#about_us" aria-controls="about_us" role="tab" data-toggle="tab">ABOUT US</a></li>
                        <li role="presentation"><a href="#update_photo" aria-controls="update_photo" role="tab" data-toggle="tab">UPDATE PHOTO</a></li>
                        <li role="presentation"><a href="#update_video" aria-controls="update_video" role="tab" data-toggle="tab">UPDATE VIDEO</a></li>
                      </ul>

                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="clinic_info">
                            <?php echo $this->renderPartial('/design/_memberclinic_info', array()); ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="clinic_special">
                            <?php echo $this->renderPartial('/design/_member_special', array()); ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="about_us">
                            <form action="" method="post">
                              <div class="box_default prelatife">
                                <h3 class="sub_title">About Us</h3>
                                    <div class="clear"></div>
                                    <div class="row memberc">
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <textarea name="" id="" rows="5" class="form-control"></textarea>
                                        </div>
                                      </div>
                                    </div>
                              </div>

                              <div class="clear height-15"></div>
                              <div class="row">
                                <div class="col-md-12">
                                  <button class="btn_purple_member defaults btn btn-default">SUBMIT</button>
                                </div>
                              </div>
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="update_photo">
                            <?php echo $this->renderPartial('/design/_member_upphoto', array()); ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="update_video">
                             <form action="" method="post">
                              <div class="box_default prelatife">
                                <h3 class="sub_title">Video</h3>
                                    <div class="clear"></div>
                                    <div class="row memberc">
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <input type="text" class="form-control" placeholder="Video Link 1">
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <input type="text" class="form-control" placeholder="Video Link 2">
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <input type="text" class="form-control" placeholder="Video Link 3">
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <input type="text" class="form-control" placeholder="Video Link 4">
                                        </div>
                                      </div>
                                    </div>
                              </div>

                              <div class="clear height-15"></div>
                              <div class="row">
                                <div class="col-md-12">
                                  <button class="btn_purple_member defaults btn btn-default">SUBMIT</button>
                                </div>
                              </div>
                            </form>
                        </div>

                      </div>

                    </div>
                    <div class="clear"></div>
                  </div>
                  <!-- end box white tab content -->
                  <div class="clear"></div>
                </div>
                <!-- end middle -->

                <div class="clear"></div>
              </div>
              <!-- End box manages_clinic -->

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
          <!-- end col md 12 -->
        </div>
        <div class="clear height-20"></div>

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>

<script src="//cdn.ckeditor.com/4.5.10/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'editor1' );
</script>