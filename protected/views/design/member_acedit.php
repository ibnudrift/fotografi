<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static page_member_s_content">
        <h1 class="title_page">MEMBER AREA</h1>
        <div class="clear height-50"></div> <div class="clear height-5"></div>
        <div class="clear"></div>

        <div class="row default box_outers_dashboardmember">
          <div class="col-md-3">
            <div class="lefts_cont_member">
              <ul class="list-unstyled">
                <li class="active"><a href="<?php echo CHtml::normalizeUrl(array('/design/memberE')); ?>">Manage Account</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/design/memberClinic')); ?>">Manage Clinic</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/design/memberReview')); ?>">Manage Reviews</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/design/memberBlog')); ?>">Manage Messages</a></li>
              </ul>
              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
          <div class="col-md-9">
            <div class="rights_cont_member">
              <div class="row default">
                <div class="col-md-6">
                  <div class="box_default text-left">
                    <h3 class="sub_title">Change your password</h3>
                    <div class="clear"></div>
                    <form action="#" method="post">
                      <div class="form-group">
                        <input type="text" class="form-control" id="" placeholder="Current Password">
                      </div>
                      <div class="form-group">
                        <input type="text" class="form-control" id="" placeholder="New Password">
                      </div>
                      <div class="form-group">
                        <input type="text" class="form-control" id="" placeholder="Confirm New Password">
                      </div>
                      <div class="form-group">
                        <button type="reset" class="btn btn-default btn_purple_member">CANCEL</button>&nbsp;&nbsp;&nbsp;
                        <button type="submit" class="btn btn-default btn_purple_member">SAVE CHANGES</button>
                      </div>
                    </form>
                  </div>    
                </div>
              </div>

              <div class="clear height-50"></div>

              <div class="row">
                <div class="col-md-12">
                  <div class="box_default text-left">
                    <h3 class="sub_title">Contact Info</h3>
                    <div class="clear"></div>
                    <form action="#" method="post">
                      <div class="row default">
                        <div class="col-md-6">
                          <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="First Name">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="Last Name">
                          </div>
                        </div>
                      </div>

                      <div class="row default">
                        <div class="col-md-6">
                          <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="Email">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="Phone">
                          </div>
                        </div>
                      </div>

                      <div class="clear height-50"></div>
                      <h3 class="sub_title">Billing Address</h3>
                      <div class="clear"></div>
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Address line 1">
                      </div>
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Address line 2">
                      </div>
                      <div class="row default">
                        <div class="col-md-6">
                          <div class="form-group">
                            <input type="text" class="form-control" placeholder="Suburb">
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" placeholder="Postcode">
                          </div>
                          <div class="form-group">
                            <select name="#" id="" class="form-control">
                              <option value="">State</option>
                            </select>
                          </div>

                        </div>
                        <div class="col-md-6"></div>
                      </div>

                      <div class="clear height-50"></div>
                      <h3 class="sub_title">Delivery Address</h3>
                      <div class="clear"></div>
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Address line 1">
                      </div>
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Address line 2">
                      </div>
                      <div class="row default">
                        <div class="col-md-6">
                          <div class="form-group">
                            <input type="text" class="form-control" placeholder="Suburb">
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" placeholder="Postcode">
                          </div>
                          <div class="form-group">
                            <select name="#" id="" class="form-control">
                              <option value="">State</option>
                            </select>
                          </div>

                        </div>
                        <div class="col-md-6"></div>
                      </div>
                      <div class="form-group">
                        <button type="reset" class="btn btn-default btn_purple_member">CANCEL</button>&nbsp;&nbsp;&nbsp;
                        <button type="submit" class="btn btn-default btn_purple_member">SAVE CHANGES</button>
                      </div>

                      <div class="clear"></div>
                    </form>
                  </div>
                </div>
              </div>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
          <!-- end col md 12 -->
        </div>
        <div class="clear height-20"></div>

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>