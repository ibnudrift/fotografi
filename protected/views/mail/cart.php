<?php
$baseUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;
$url = Yii::app()->request->hostInfo;
?>

<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
    <tbody>
        <tr>
            <td align="center" valign="top">

                <table cellspacing="0" cellpadding="5" border="0" width="700" style="border:1px solid #373435">
                    <tbody>
                        <tr>
                            <td valign="top" style="background-color:#904099; text-align:center">
                            	<img src="<?php echo $baseUrl ?>/asset/images/logo_header_trusted_surgeons.jpg">
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table cellspacing="0" cellpadding="0" border="0" width="700" style="border:1px solid #666">
                    <tbody>
                        <tr style="margin:0;padding:0">
                            <td style="margin:0;padding:0">
                            </td>
                            <td bgcolor="#FFFFFF" style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0">

                                <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto;padding:30px 15px;border:1px solid #e7e7e7">
                                    <table bgcolor="transparent" style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0">
                                        <tbody>
                                            <tr style="margin:0;padding:0">
                                                <td style="margin:0;padding:0">
                                                    <h5 style="line-height:24px;color:#000;font-weight:900;font-size:17px;margin:0 0 20px;padding:0">Thanks, your order has been received:</h5>


                                                    <p style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0">To check the status of your order, you can visit the link below</p>
                                                    <div align="center" style="text-align:center;margin:20px;padding:0"><a target="_blank" style="margin:0;padding:7px 17px;color:#fff;text-decoration:none;display:inline-block;margin-bottom:0;vertical-align:middle;line-height:20px;font-size:13px;font-weight:600;text-align:center;white-space:nowrap;border-radius:2px;background-color:#777777;background-image:linear-gradient(top,#A2A2A2 0%,#575757 100%);border:1px solid #000000" href="<?php echo $url.CHtml::normalizeUrl(array('/member/order')); ?>">Check Order Status</a>
                                                    </div>

                                                    <p style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0">You can monitor the status of your order by accessing the Order Status page on your account <span class="lG">Trusted Surgeons</span>.</p>
                                                    <hr style="border-top-color:#d0d0d0;border-top-style:solid;border-bottom-color:#ffffff;border-bottom-style:solid;margin:20px 0;padding:0;border-width:3px 0 1px">
                                                    <h5 style="font-family:'HelveticaNeue-Light','Helvetica Neue Light','Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;line-height:1.1;color:#000;font-weight:900;font-size:17px;margin:0 0 20px;padding:0">List Order:</h5>

                                                    <div style="padding-top:20px;padding-bottom:20px;padding-right:20px;padding-left:20px;border-width:1px;border-style:dashed;border-color:#9D72C9;background-color:#F7F5FD;border-radius:5px;margin-bottom:20px">

                                                        <div style="margin:0;padding:0">
                                                            <?php $total = 0 ?>
                                                            <?php foreach ($order as $key => $value): ?>
	                                                            <ol style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0 0 0 21px">
	                                                                <li style="font-size:13px;margin:0 0 15px;padding:0">
	                                                                    <b style="margin:0;padding:0">
	                                                                        <?php echo $value->name ?>

	                                                                    </b>
	                                                                    <br style="margin:0;padding:0">Qty: <?php echo $value->qty ?> Item(s) (<?php echo Cart::money($subTotal = ($value->price)) ?>)</li>
	                                                            </ol>
	                                                            <?php $total = $total + ($subTotal*$value['qty']) ?>
                                                            <?php endforeach ?>

															<!-- Menampilkan pengiriman -->
                                                            <div style="font-size:13px;line-height:18px;margin:0 0 15px;padding:0">

                                                                <div style="margin:0 0 5px;padding:0"><b style="margin:0;padding:0">Delivery Address:</b>
                                                                </div><?php echo $model->shipping_first_name ?> <?php echo $model->shipping_last_name ?>
                                                                <br style="margin:0;padding:0">
                                                                <?php echo $model->shipping_address_1 ?>
                                                                <?php if ($model->shipping_address_2 != ''): ?>
                                                                <br><?php echo $model->shipping_address_2 ?>
                                                                <?php endif ?>
                                                                <br style="margin:0;padding:0">
                                                                <?php echo $model->shipping_city ?>, <?php echo $model->shipping_zone ?>, 
                                                                <?php echo $model->shipping_postcode ?>
                                                                <br style="margin:0;padding:0">Telp: <?php echo $model->phone ?>
                                                            </div>


                                                            <div style="margin:20px 0 0;padding:0">
                                                                <?php /*
                                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0">
                                                                    <tbody style="margin:0;padding:0">
                                                                        <tr style="margin:0;padding:0">
                                                                            <td valign="top" style="width:50%;font-weight:700;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0">Total Harga Produk:
                                                                            </td>
                                                                            <td align="right" valign="top" style="text-align:right;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0">Rp 700.000
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>

                                                                <div style="border-bottom-width:1px;border-bottom-color:#eee;border-bottom-style:solid;margin:0;padding:0">
                                                                </div>
                                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0">
                                                                    <tbody style="margin:0;padding:0">
                                                                        <tr style="margin:0;padding:0">
                                                                            <td valign="top" style="width:50%;font-weight:700;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0">Ongkos kirim:
                                                                            </td>
                                                                            <td align="right" valign="top" style="text-align:right;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0">Rp 17.000
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>

                                                                <div style="border-bottom-width:1px;border-bottom-color:#eee;border-bottom-style:solid;margin:0;padding:0">
                                                                </div>
                                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0">
                                                                    <tbody style="margin:0;padding:0">
                                                                        <tr style="margin:0;padding:0">
                                                                            <td valign="top" style="width:50%;font-weight:700;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0">Asuransi:
                                                                            </td>
                                                                            <td align="right" valign="top" style="text-align:right;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0">Rp 6.400
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                */ ?>
                                                                <div style="border-bottom-width:1px;border-bottom-color:#eee;border-bottom-style:solid;margin:0;padding:0">
                                                                </div>
                                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0">
                                                                    <tbody style="margin:0;padding:0">
                                                                        <tr style="margin:0;padding:0">
                                                                            <td valign="top" style="width:50%;font-weight:700;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0">Total:
                                                                            </td>
                                                                            <td align="right" valign="top" style="text-align:right;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0"><?php echo Cart::money($total, 0) ?>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <div style="border-bottom-width:1px;border-bottom-color:#eee;border-bottom-style:solid;margin:0;padding:0">
                                                                </div>
                                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0">
                                                                    <tbody style="margin:0;padding:0">
                                                                        <tr style="margin:0;padding:0">
                                                                            <td valign="top" style="width:50%;font-weight:700;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0">Delivery Price:
                                                                            </td>
                                                                            <td align="right" valign="top" style="text-align:right;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0">Free
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <div style="border-bottom-width:1px;border-bottom-color:#eee;border-bottom-style:solid;margin:0;padding:0">
                                                                </div>
                                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0">
                                                                    <tbody style="margin:0;padding:0">
                                                                        <tr style="margin:0;padding:0">
                                                                            <td valign="top" style="width:50%;font-weight:700;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0">Total Payment:
                                                                            </td>
                                                                            <td align="right" valign="top" style="text-align:right;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0"><?php echo Cart::money($total + $model->delivery_price, 0) ?>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <p style="margin-bottom:20px;font-weight:normal;font-size:14px;line-height:1.6;margin:40px 0 0 0;padding:10px 0 0 0;border-top:3px solid #d0d0d0"><small style="color:#999">Monitor the status of your order on the page <a target="_blank" style="margin:0;padding:0;color:#0f990f;text-decoration:none" href="<?php echo $url.CHtml::normalizeUrl(array('/member/order')); ?>">Order</a>. <br style="margin:0;padding:0">This email is automatically created. Please do not post a reply to this email.</small></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                            <td style="margin:0;padding:0">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table cellspacing="0" cellpadding="5" border="0" width="700" style="border:1px solid #373435">
                    <tbody>
                        <tr>
                            <td valign="middle" style="background-color:#3C2A5E">
                                <div style="text-align:center">
                                    <?php if ($this->setting['url_facebook']): ?>
                                    <a href="<?php echo $this->setting['url_facebook'] ?>"><img style="margin:0!important" src="<?php echo $baseUrl ?>/asset/images/ic-facebook.png"></a>
                                    <?php endif ?>
                                    <?php if ($this->setting['url_twitter']): ?>
                                    <a href="<?php echo $this->setting['url_twitter'] ?>"><img style="margin:0!important" src="<?php echo $baseUrl ?>/asset/images/ic-twitter.png"></a>
                                    <?php endif ?>
                                    <?php if ($this->setting['url_linkedin']): ?>
                                    <a href="<?php echo $this->setting['url_linkedin'] ?>"><img style="margin:0!important" src="<?php echo $baseUrl ?>/asset/images/ic-linkedin.png"></a>
                                    <?php endif ?>
                                    <?php if ($this->setting['url_instagram']): ?>
                                    <a href="<?php echo $this->setting['url_instagram'] ?>"><img style="margin:0!important" src="<?php echo $baseUrl ?>/asset/images/ic-instagram.png"></a>
                                    <?php endif ?>
                                    <?php if ($this->setting['url_pinterest']): ?>
                                    <a href="<?php echo $this->setting['url_pinterest'] ?>"><img style="margin:0!important" src="<?php echo $baseUrl ?>/asset/images/ic-pinterest.png"></a>
                                    <?php endif ?>

                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table cellspacing="0" cellpadding="0" border="0" width="700" style="border:1px solid #666">
                    <tbody>
                        <tr>
                            <td align="center" valign="top">

                                <table cellspacing="0" cellpadding="10" border="0" width="700">
                                    <tbody>
                                        <tr>
                                            <td valign="top">

                                                <table cellspacing="0" cellpadding="10" border="0" width="100%" style="color:#505050">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top" style="font-family:Verdana,helvetica,arial;font-size:12px">
                                                                    <b style="color: #f58634;">Customer Service :</b>
                                                                    <br>
                                                                    <table style="font-family:Verdana,helvetica,arial;font-size:12px">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="width:150px">CS For Patients</td>
                                                                                <td style="width:10px">:</td>
                                                                                <td><?php echo $this->setting['contact_phone_patiens'] ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width:150px">CS For Surgeons</td>
                                                                                <td style="width:10px">:</td>
                                                                                <td><?php echo $this->setting['contact_phone_surgeons'] ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Email For Patients</td>
                                                                                <td>:</td>
                                                                                <td><a target="_blank" href="mailto:<?php echo $this->setting['contact_email_patiens'] ?>"><?php echo $this->setting['contact_email_patiens'] ?></a></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Email For Surgeons</td>
                                                                                <td>:</td>
                                                                                <td><a target="_blank" href="mailto:<?php echo $this->setting['contact_email_surgeons'] ?>"><?php echo $this->setting['contact_email_surgeons'] ?></a></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Email</td>
                                                                                <td>:</td>
                                                                                <td><a target="_blank" href="mailto:<?php echo $this->setting['email'] ?>"><?php echo $this->setting['email'] ?></a></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <br>
                                                                    <table style="font-family:Verdana,helvetica,arial;font-size:12px">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="width:200px">
                                                                                	<a href="<?php echo $url ?>">
                                                                						<img src="<?php echo $baseUrl ?>/asset/images/logo_header_trusted_surgeons.jpg">
                                                                					</a>
                                                                                </td>
                                                                                <td style="text-align:right" width="100%">
                                                                                	<p>
																						<strong>TRUSTED SURGEONS</strong> <br>

																						<?php echo nl2br($this->setting['contact_address']) ?>
                                                                                	</p>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                            </td>
                        </tr>
                    </tbody>
                </table>


                <br>
            </td>
        </tr>
    </tbody>
</table>