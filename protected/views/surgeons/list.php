<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/asset/js/jquery-ui-1.12.1/jquery-ui.css">
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/asset/js/jquery-ui-1.12.1/jquery-ui.theme.min.css">
<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      
      
      <div class="content-text insides_static">
        <h1 class="title_page">SURGEONS</h1>
        <div class="clear"></div>
        <div class="clear height-30"></div>

        <!-- Start content surgeons -->
        <div class="insides_c_content_surgeons">
          
          <div class="tops">
            <div class="filter_surgeons_top">
              <form action="<?php echo CHtml::normalizeUrl(array('list')); ?>" class="form-inline">
                 <div class="form-group w25p">
                  <label class="sr-only" for="">Services</label>
<script src="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/jquery.easy-autocomplete.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.min.css">

                  <input type="input" name="specialisation" id="specialisation" value="<?php echo $_GET['specialisation'] ?>" class="form-control" id="" placeholder="Procedures">

<script type="text/javascript">
var options = {
    url: "<?php echo CHtml::normalizeUrl(array('/home/specialisationList')); ?>",
    list: {
        match: {
          enabled: true
        }
    },
    getValue: "name",

    template: {
      type: "description",
      fields: {
        description: "type"
      }
    }

};

$("#specialisation").easyAutocomplete(options);
</script>

<?php /*
                  <select name="specialisation" id="specialisation" class="form-control">
<?php 
$dataCategory = (PrdCategory::model()->categoryTree('spesialis', $this->languageID));
?>
                      <option value="">---- Select Services ----</option>
                      <?php foreach ($dataCategory as $key => $value): ?>
                        <?php if (count($value['children']) > 0): ?>
                        <optgroup label="<?php echo $value['title'] ?>">
                          <?php foreach ($value['children'] as $k => $v): ?>
                          <option value="<?php echo $v['id'] ?>"><?php echo $v['title'] ?></option>
                          <?php endforeach ?>
                        </optgroup>
                        <?php else: ?>
                        <option value="<?php echo $value['id'] ?>"><?php echo $value['title'] ?></option>
                        <?php endif ?>
                      <?php endforeach ?>
                  </select>
                  <script type="text/javascript">
                    $('#specialisation').val('<?php echo $_GET['specialisation'] ?>');
                  </script>
*/ ?>                  
                </div>
                 <div class="form-group w25p">
                  <label class="sr-only" for="">Doctor Name</label>
                  <input type="input" name="name" value="<?php echo $_GET['name'] ?>" class="form-control" id="" placeholder="Doctor Name">
                </div>
                 <div class="form-group w25p">
                  <label class="sr-only" for="">Nearest Location</label>
                  <input type="input" name="suburb" value="<?php echo $_GET['suburb'] ?>" class="form-control empty" id="suburb-auto" placeholder="&#xf041; Nearest Location">

<script type="text/javascript">
var options = {
    url: function(phrase) {
      return "<?php echo CHtml::normalizeUrl(array('/home/locationList')); ?>";
    },

    ajaxSettings: {
      dataType: "json",
      method: "POST",
      data: {
        dataType: "json"
      }
    },
    preparePostData: function(data) {
      data.phrase = $("#suburb-auto").val();
      return data;
    },
    // url: "<?php echo CHtml::normalizeUrl(array('/home/locationList')); ?>",
    // list: {
    //     match: {
    //       enabled: true
    //     }
    // },

    requestDelay: 400

};

$("#suburb-auto").easyAutocomplete(options);
</script>

                </div>
                <div class="form-group w25p">
                  <button type="submit" class="btn btn-default btns_submit_filter">
                    <i class="fa fa-search"></i> SEARCH
                  </button>
                </div>
              </form>
              <div class="clear"></div>
            </div>
          </div>
          <!-- end tops -->

<script>
  // $( function() {
  //   $( "#suburb-auto" ).autocomplete({
  //     source: "<?php echo CHtml::normalizeUrl(array('/home/suburb')); ?>"
  //   });
  // } );
</script>
          <div class="middles">

            <div class="box_sorting text-right">
              <span class="d-inline">Sort by:</span> 
              &nbsp;&nbsp;
              <a href="<?php echo CHtml::normalizeUrl(array('list', 'order'=>'alphabetic')); ?>" <?php if ($_GET['order'] == '' OR $_GET['order'] == 'alphabetic'): ?>class="active"<?php endif ?>>Alphabetical order</a>&nbsp;&nbsp;
              <a href="<?php echo CHtml::normalizeUrl(array('list', 'order'=>'most_review')); ?>" <?php if ($_GET['order'] == 'most_review'): ?>class="active"<?php endif ?>>Most reviewed</a>&nbsp;&nbsp;
              <a href="<?php echo CHtml::normalizeUrl(array('list', 'order'=>'date_join')); ?>" <?php if ($_GET['order'] == 'date_join'): ?>class="active"<?php endif ?>>Date joined</a>
            </div>
            <div class="clear height-30"></div>
            
            <div class="box_listing_surgeons2">
              <div class="row">
                <div class="col-md-12">
                  <?php foreach ($dataDoctor->getData() as $key => $value): ?>
                  <div class="items">
                    <div class="picture doctor_p">
                      <a href="<?php echo CHtml::normalizeUrl(array('detail', 'id'=>$value->id)); ?>">
                        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(127,127, '/images/doctor/'.$value->photo , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                      </a>
                    </div>
                    <div class="ml102">
                      <div class="desc">
                        <div class="doctor_info">
                          <div class="row">
                            <div class="col-md-10">
                              <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <a href="<?php echo CHtml::normalizeUrl(array('detail', 'id'=>$value->id)); ?>">
                                    <span class="names"><?php echo $value->name ?></span>
                                    </a>
                                    <p><?php echo $value->certification ?> <br>
                                        <?php if ($value->n_review > 0): ?>
                                            <?php echo DoctorReviewCategory::model()->createStar($value->rating) ?> &nbsp; <?php echo $value->n_review ?> reviews <br>
                                        <?php endif ?>
                                        <?php echo $value->clinics[0]->address_1 ?> <?php echo $value->clinics[0]->address_2 ?> <br>
                                        <?php echo $value->clinics[0]->suburb ?> <?php echo $value->clinics[0]->state ?> <br>
                                        <?php if ($value->clinics[0]->phone != ''): ?>
                                                Phone: <?php echo $value->clinics[0]->phone ?>
                                        <?php endif ?>
                                    </p>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                  <div class="treatment-area">    
<?php
$dataCategory = (PrdCategory::model()->categoryTree('spesialis', $this->languageID));
$dataSpec = array();
foreach ($value->spec as $v) {
  $dataSpec[$v->specialitation_id] = 1;
}
?>
                                  <?php foreach ($dataCategory as $v): ?>
                                  <?php
                                  $statusSpec = 0;
                                  if (count($v['children']) > 0) {
                                    foreach ($v['children'] as $ke => $val) {
                                      if (array_key_exists($val['id'], $dataSpec)) {
                                        $statusSpec = 1;
                                      }
                                    }
                                  }
                                  ?>
                                  <?php switch ($v['title']) {
                                    case 'Face':
                                      ?>
                                      <div class="body-area face">
                                        <div class="icon"></div>
                                        Face
                                      </div>
                                      <?php
                                      break;
                                    
                                    case 'Breast':
                                      ?>
                                      <div class="body-area breasts">
                                        <div class="icon"></div>
                                        Breasts
                                      </div>
                                      <?php
                                      break;
                                    
                                    case 'Body':
                                      ?>
                                      <div class="body-area body">
                                        <div class="icon"></div>
                                        Body
                                      </div>
                                      <?php
                                      break;
                                    
                                    case 'Intimates':
                                      ?>
                                      <div class="body-area other">
                                        <div class="icon"></div>
                                        Intimates
                                      </div>
                                      <?php
                                      break;
                                    
                                    default:

                                      break;
                                  } ?>
                                        <!-- <div class="clear height-10"></div> -->
                                  <?php endforeach ?>
                                      <div class="clear"></div>
                                  </div>
                                  <!-- end icon treatment -->
                                </div>
                              </div>
                              <!-- end info -->
                            </div>
                            <div class="col-md-2">
                              <div class="fright padding-top-5">
                              <?php if ($value->member_id == 0): ?>
                                <a href="<?php echo CHtml::normalizeUrl(array('claim', 'id'=>$value->id)); ?>" class="back_purple_defaults_tl d-inline">CLAIM THIS CLINIC</a>
                                <div class="clear height-10"></div>
                              <?php else: ?>
                                <a href="<?php echo CHtml::normalizeUrl(array('/contact/index', 'id'=>$value->id)); ?>" class="back_purple_defaults_tl d-inline">CONTACT CLINIC</a>
                                <div class="clear height-10"></div>
                              <?php endif ?>
                                <a href="<?php echo CHtml::normalizeUrl(array('/coaching/index')); ?>" class="back_purple_defaults_tl d-inline">GET COACHING SERVICE</a>
                                <div class="clear height-15"></div>
                                <div class="social-media">
                                  <?php if ($value->social_facebook != ''): ?>
                                  <a href="<?php echo $value->social_facebook ?>" class="facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                                  <?php endif ?>
                                  <?php if ($value->social_twitter != ''): ?>
                                  <a href="<?php echo $value->social_twitter ?>" class="twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                                  <?php endif ?>
                                  <?php if ($value->social_instagram != ''): ?>
                                  <a href="<?php echo $value->social_instagram ?>" class="instagram" target="_blank"><i class="fa fa-instagram"></i></a>
                                  <?php endif ?>
                                </div>
                                <div class="clear"></div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- end description -->
                    </div>
                    <div class="clear"></div>
                  </div>
                  <?php endforeach ?>
                </div>
              </div>
              <!-- end row -->
                <?php $this->widget('CLinkPager', array(
                    'pages' => $dataDoctor->getPagination(),
                    'header' => '',
                    'htmlOptions' => array('class' => 'pagination'),
                )) ?>

              <div class="clear"></div>
            </div>
            <!-- end box listing surgeons1 -->

            <div class="clear"></div>
          </div>
          <!-- end middles -->
          <div class="clear"></div>
        </div>
        <!-- End content surgeons -->
        

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>