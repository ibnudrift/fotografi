<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">

    <div class="pict_full illustration_picture prelatife mxh480">
      <img src="<?php echo $this->assetBaseurl ?>ill-surgeons-1.jpg" alt="" class="img-responsive">
    </div>
    <div class="prelatife middle-bottom_surgeonsindex">
      <div class="clear height-50"></div>
      <div class="prelatife container">
        <div class="content-text insides_static">
          <h1 class="title_page shadow">SURGEONS</h1>
          <div class="clear"></div>
          <h3 class="tagline shadow">Choose a Surgeon You Can Trust</h3>
          <div class="clear"></div>

          <!-- Start content surgeons -->
          <div class="insides_c_content_surgeons">
            
            <div class="tops">
              <div class="filter_surgeons_top">
                <form action="<?php echo CHtml::normalizeUrl(array('list')); ?>" class="form-inline">
                   <div class="form-group w25p">
                    <label class="sr-only" for="">Services</label>
<script src="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/jquery.easy-autocomplete.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.min.css">
                    <input type="input" name="specialisation" id="specialisation" value="<?php echo $_GET['specialisation'] ?>" class="form-control" id="" placeholder="Procedures">
<script type="text/javascript">
var options = {
    url: "<?php echo CHtml::normalizeUrl(array('/home/specialisationList')); ?>",
    list: {
        match: {
          enabled: true
        }
    },
    getValue: "name",

    template: {
      type: "description",
      fields: {
        description: "type"
      }
    }

};

$("#specialisation").easyAutocomplete(options);
</script>
                    <?php /*
                    <select name="specialisation" id="specialisation" class="form-control">
<?php 
$dataCategory = (PrdCategory::model()->categoryTree('spesialis', $this->languageID));
?>
                      <option value="">---- Select Services ----</option>
                      <?php foreach ($dataCategory as $key => $value): ?>
                        <?php if (count($value['children']) > 0): ?>
                        <optgroup label="<?php echo $value['title'] ?>">
                          <?php foreach ($value['children'] as $k => $v): ?>
                          <option value="<?php echo $v['id'] ?>"><?php echo $v['title'] ?></option>
                          <?php endforeach ?>
                        </optgroup>
                        <?php else: ?>
                        <option value="<?php echo $value['id'] ?>"><?php echo $value['title'] ?></option>
                        <?php endif ?>
                      <?php endforeach ?>
                    </select>
                    <script type="text/javascript">
                      $('#specialisation').val('<?php echo $_GET['specialisation'] ?>');
                    </script>
                    */ ?>
                  </div>
                   <div class="form-group w25p">
                    <label class="sr-only" for="">Doctor Name</label>
                    <input type="input" name="name" value="<?php echo $_GET['name'] ?>" class="form-control" id="" placeholder="Doctor Name">
                  </div>
                   <div class="form-group w25p">
                    <label class="sr-only" for="">Nearest Location</label>
                    <input type="input" name="suburb" value="<?php echo $_GET['suburb'] ?>" class="form-control empty" id="suburb-auto" placeholder="&#xf041; Nearest Location">
<script type="text/javascript">
var options = {
    url: function(phrase) {
      return "<?php echo CHtml::normalizeUrl(array('/home/locationList')); ?>";
    },

    ajaxSettings: {
      dataType: "json",
      method: "POST",
      data: {
        dataType: "json"
      }
    },
    preparePostData: function(data) {
      data.phrase = $("#suburb-auto").val();
      return data;
    },
    // url: "<?php echo CHtml::normalizeUrl(array('/home/locationList')); ?>",
    // list: {
    //     match: {
    //       enabled: true
    //     }
    // },

    requestDelay: 400

};

$("#suburb-auto").easyAutocomplete(options);
</script>
                  </div>
                  <div class="form-group w25p">
                    <button type="submit" class="btn btn-default btns_submit_filter">
                      <i class="fa fa-search"></i> SEARCH
                    </button>
                  </div>
                </form>
                <div class="clear"></div>
              </div>

              <div class="clear height-15"></div>
            </div>
            <!-- end tops -->

<script>
  // $( function() {
  //   $( "#suburb-auto" ).autocomplete({
  //     source: "<?php echo CHtml::normalizeUrl(array('/home/suburb')); ?>"
  //   });
  // } );
</script>

            <div class="middles">

              <div class="box_sorting text-left">
                <span class="d-inline">Sort by:</span> 
                &nbsp;&nbsp;
                <a href="<?php echo CHtml::normalizeUrl(array('list', 'order'=>'alphabetic')); ?>" <?php if ($_GET['order'] == '' OR $_GET['order'] == 'alphabetic'): ?>class="active"<?php endif ?>>Alphabetical order</a>&nbsp;&nbsp;
                <a href="<?php echo CHtml::normalizeUrl(array('list', 'order'=>'most_review')); ?>" <?php if ($_GET['order'] == 'most_review'): ?>class="active"<?php endif ?>>Most reviewed</a>&nbsp;&nbsp;
                <a href="<?php echo CHtml::normalizeUrl(array('list', 'order'=>'date_join')); ?>" <?php if ($_GET['order'] == 'date_join'): ?>class="active"<?php endif ?>>Date joined</a>
              </div>
              <div class="clear height-30"></div>
              
              <div class="box_listing_surgeons1">
                <div class="row">
<?php
$dataFeatured = DoctorFeatured::model()->findAll('type = :type', array(':type'=>'Surgeons'));
?>

                  <?php foreach ($dataFeatured as $v): ?>
<?php
$value = Doctor::model()->with(array('specialication', 'clinics', 'clinic'))->findByPk($v->doctor_id);
?>                    
                  
                  <div class="col-md-4">
                    <div class="items">
                      <div class="picture">
                        <a href="<?php echo CHtml::normalizeUrl(array('detail', 'id'=>$value->id)); ?>">
                        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(404,179, '/images/doctor/'.$value->cover , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                        </a>
                      </div>
                      <div class="desc">
                        <div class="doctor_p d-inline padding-right-15">
                          <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(82,82, '/images/doctor/'.$value->photo , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                        </div>
                        <div class="doctor_info d-inline">
                          <a href="<?php echo CHtml::normalizeUrl(array('detail', 'id'=>$value->id)); ?>">
                          <span class="names"><?php echo $value->name ?></span>
                          </a>
                          <p><?php echo $value->certification ?> <br>
                          <?php if ($value->n_review > 0): ?>
                              <?php echo DoctorReviewCategory::model()->createStar($value->rating) ?> &nbsp; <?php echo $value->n_review ?> reviews <br>
                          <?php endif ?>
                             <?php echo $value->clinics[0]->suburb ?></p>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>
                  <?php endforeach ?>
                  <?php /*
                  <div class="col-md-4">
                    <div class="items">
                      <div class="picture">
                        <a href="<?php echo CHtml::normalizeUrl(array('detail')); ?>">
                        <img src="<?php echo $this->assetBaseurl ?>ex-item-surgeons2.jpg" alt="" class="img-responsive">
                        </a>
                      </div>
                      <div class="desc">
                        <div class="doctor_p d-inline padding-right-15">
                          <img src="<?php echo $this->assetBaseurl ?>pict-doctor-surgeons2.png" alt="" class="img-responsive">
                        </div>
                        <div class="doctor_info d-inline">
                          <a href="<?php echo CHtml::normalizeUrl(array('detail')); ?>">
                          <span class="names">Alexander Phoon</span>
                          </a>
                          <p>MBBS, FRACS <br>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i> &nbsp; 50 reviews <br>
                             Wooloomooloo, NSW 2022</p>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="items">
                      <div class="picture">
                        <a href="<?php echo CHtml::normalizeUrl(array('detail')); ?>">
                        <img src="<?php echo $this->assetBaseurl ?>ex-item-surgeons3.jpg" alt="" class="img-responsive">
                        </a>
                      </div>
                      <div class="desc">
                        <div class="doctor_p d-inline padding-right-15">
                          <img src="<?php echo $this->assetBaseurl ?>pict-doctor-surgeons3.png" alt="" class="img-responsive">
                        </div>
                        <div class="doctor_info d-inline">
                          <a href="<?php echo CHtml::normalizeUrl(array('detail')); ?>">
                          <span class="names">Alexander Phoon</span>
                          </a>
                          <p>MBBS, FRACS <br>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i> &nbsp; 50 reviews <br>
                             Wooloomooloo, NSW 2022</p>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="items">
                      <div class="picture">
                        <a href="<?php echo CHtml::normalizeUrl(array('detail')); ?>">
                        <img src="<?php echo $this->assetBaseurl ?>ex-item-surgeons4.jpg" alt="" class="img-responsive">
                        </a>
                      </div>
                      <div class="desc">
                        <div class="doctor_p d-inline padding-right-15">
                          <img src="<?php echo $this->assetBaseurl ?>pict-doctor-surgeons.png" alt="" class="img-responsive">
                        </div>
                        <div class="doctor_info d-inline">
                          <a href="<?php echo CHtml::normalizeUrl(array('detail')); ?>">
                          <span class="names">Alexander Phoon</span>
                          </a>
                          <p>MBBS, FRACS <br>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i> &nbsp; 50 reviews <br>
                             Wooloomooloo, NSW 2022</p>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="items">
                      <div class="picture">
                        <a href="<?php echo CHtml::normalizeUrl(array('detail')); ?>">
                        <img src="<?php echo $this->assetBaseurl ?>ex-item-surgeons5.jpg" alt="" class="img-responsive">
                        </a>
                      </div>
                      <div class="desc">
                        <div class="doctor_p d-inline padding-right-15">
                          <img src="<?php echo $this->assetBaseurl ?>pict-doctor-surgeons2.png" alt="" class="img-responsive">
                        </div>
                        <div class="doctor_info d-inline">
                          <a href="<?php echo CHtml::normalizeUrl(array('detail')); ?>">
                          <span class="names">Alexander Phoon</span>
                          </a>
                          <p>MBBS, FRACS <br>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i> &nbsp; 50 reviews <br>
                             Wooloomooloo, NSW 2022</p>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="items">
                      <div class="picture">
                        <a href="<?php echo CHtml::normalizeUrl(array('detail')); ?>">
                        <img src="<?php echo $this->assetBaseurl ?>ex-item-surgeons6.jpg" alt="" class="img-responsive">
                        </a>
                      </div>
                      <div class="desc">
                        <div class="doctor_p d-inline padding-right-15">
                          <img src="<?php echo $this->assetBaseurl ?>pict-doctor-surgeons3.png" alt="" class="img-responsive">
                        </div>
                        <div class="doctor_info d-inline">
                          <a href="<?php echo CHtml::normalizeUrl(array('detail')); ?>">
                          <span class="names">Alexander Phoon</span>
                          </a>
                          <p>MBBS, FRACS <br>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i> &nbsp; 50 reviews <br>
                             Wooloomooloo, NSW 2022</p>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>
                  */ ?>

                </div>
                <!-- end row -->

                <div class="clear"></div>
              </div>
              <!-- end box listing -->
<?php
$criteria = new CDbCriteria;
$criteria->order = 't.date_input DESC';
$criteria->group = 't.id';
$dataFeatured = new CActiveDataProvider('DoctorBlog', array(
  'criteria'=>$criteria,
    'pagination'=>array(
        'pageSize'=>3,
    ),
));
?>
<?php if ($dataFeatured->getTotalItemCount() > 0): ?>
              <div class="clear height-40"></div>
              <div class="lines-grey"></div>
              <div class="clear height-50"></div>
              <h2 class="title_page">SURGEONS BLOGS</h2>
              <div class="clear"></div>

              <div class="clear height-45"></div>
              
              <div class="blocks_outer_list_blog pinside">
                <div class="row">
                  <?php foreach ($dataFeatured->getData() as $key => $value): ?>
                  <div class="col-md-4">
                    <div class="items prelatife">
                      <div class="pict prelatife"><a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(405,247, '/images/blog/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></a></div>
                      <div class="block_purple desription">
                        <span class="title">
                          <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id)); ?>">
                          <?php echo $value->title ?>
                          </a>
                        </span>
                        <span class="date">
                        <?php if ($value->doctor_id == 0): ?>
                            <?php if ($value->member_id == 0): //admin ?>
                                Admin
                            <?php else: // pasien?>  
                                Patient
                            <?php endif ?>  
                        <?php else: // doctor?>
                            <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail', 'id'=>$value->doctor_id)); ?>">
                            <?php
                            echo(Doctor::model()->findByPk($value->doctor_id)->name);
                            ?>
                            </a>
                            -
                        <?php endif ?>
                        <?php echo date('d F Y', strtotime($value->date_input)) ?>
                        </span>
                      </div>
                    </div>
                  </div>
                  <?php endforeach ?>
                </div>
                <div class="clear"></div>
              </div>
              <!-- end list blogs surgeons -->
<?php endif ?>
              <div class="clear"></div>
            </div>
            <!-- end middles -->
            <div class="clear"></div>
          </div>
          <!-- End content surgeons -->
          

          <div class="clear"></div>
        </div>

        <div class="clear height-50"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear height-25"></div>
  </div>
</section>