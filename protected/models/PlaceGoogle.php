<?php

/**
 * This is the model class for table "gal_gallery_image".
 *
 * The followings are the available columns in table 'gal_gallery_image':
 * @property integer $id
 * @property string $image
 */
class PlaceGoogle
{
	public function placeAutocomplete($phrase = "")
	{
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=".$phrase."&types=geocode&language=en&key=AIzaSyCnVYV9PU2hDS4GMEJ_TZ2Hy-zy1iXfQX0");
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec ($ch);
		curl_close ($ch);

		$data = json_decode($server_output);

		$dataArray = array();
		foreach ($data->predictions as $key => $value) {
			$dataArray[] = $value->description;
		}
		return $dataArray;
	}
	public function getLatLong($phrase='')
	{
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($phrase)."&key=AIzaSyCnVYV9PU2hDS4GMEJ_TZ2Hy-zy1iXfQX0");
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec ($ch);
		curl_close ($ch);
		$data = json_decode($server_output);
		if ($data->status == 'OK') {
			return array(
				'lat'=>$data->results[0]->geometry->location->lat,
				'lng'=>$data->results[0]->geometry->location->lng
			);
		} else {
			return null;
		}
		
	}
}