<?php

/**
 * This is the model class for table "doctor".
 *
 * The followings are the available columns in table 'doctor':
 * @property integer $id
 * @property integer $member_id
 * @property string $name
 * @property string $certification
 * @property string $photo
 * @property string $cover
 * @property string $social_facebook
 * @property string $social_twitter
 * @property string $social_instagram
 * @property string $about
 */
class Doctor extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Doctor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'doctor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// array('member_id, name, certification, photo, cover, social_facebook, social_twitter, social_instagram, about', 'required'),
			array('member_id', 'numerical', 'integerOnly'=>true),
			array('name, certification, photo, cover, social_facebook, social_twitter, social_instagram', 'length', 'max'=>200),
			array('member_id, name, certification, photo, cover, social_facebook, social_twitter, social_instagram, about', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, member_id, name, certification, photo, cover, social_facebook, social_twitter, social_instagram, about', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clinics'=>array(self::HAS_MANY, 'DoctorClinic', 'doctor_id'),
			'spec'=>array(self::HAS_MANY, 'DoctorSpecialication', 'doctor_id'),
			'specialication'=>array(self::HAS_ONE, 'DoctorSpecialication', 'doctor_id'),
			'clinic'=>array(self::HAS_ONE, 'DoctorClinic', 'doctor_id'),
			'videos'=>array(self::HAS_MANY, 'DoctorVideo', 'doctor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'member_id' => 'Member',
			'name' => 'Name',
			'certification' => 'Certification',
			'photo' => 'Photo',
			'cover' => 'Cover',
			'social_facebook' => 'Social Facebook',
			'social_twitter' => 'Social Twitter',
			'social_instagram' => 'Social Instagram',
			'about' => 'About',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('member_id',$this->member_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('certification',$this->certification,true);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('cover',$this->cover,true);
		$criteria->compare('social_facebook',$this->social_facebook,true);
		$criteria->compare('social_twitter',$this->social_twitter,true);
		$criteria->compare('social_instagram',$this->social_instagram,true);
		$criteria->compare('about',$this->about,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}