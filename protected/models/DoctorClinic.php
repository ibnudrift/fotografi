<?php

/**
 * This is the model class for table "doctor_clinic".
 *
 * The followings are the available columns in table 'doctor_clinic':
 * @property integer $id
 * @property integer $doctor_id
 * @property string $name
 * @property string $phone
 * @property string $website
 * @property string $fax
 * @property string $address_1
 * @property string $address_2
 * @property string $suburb
 * @property string $postcode
 * @property string $state
 */
class DoctorClinic extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DoctorClinic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'doctor_clinic';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, phone, website, fax, address_1, suburb, postcode, state', 'required'),
			array('doctor_id', 'numerical', 'integerOnly'=>true),
			array('name, phone, website, fax, address_1, address_2, suburb, state', 'length', 'max'=>100),
			array('postcode', 'length', 'max'=>20),
			array('latitude, longitude', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, doctor_id, name, phone, website, fax, address_1, address_2, suburb, postcode, state', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'doctor_id' => 'Doctor',
			'name' => 'Name',
			'phone' => 'Phone',
			'website' => 'Website',
			'fax' => 'Fax',
			'address_1' => 'Address 1',
			'address_2' => 'Address 2',
			'suburb' => 'Suburb',
			'postcode' => 'Postcode',
			'state' => 'State',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('doctor_id',$this->doctor_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('address_1',$this->address_1,true);
		$criteria->compare('address_2',$this->address_2,true);
		$criteria->compare('suburb',$this->suburb,true);
		$criteria->compare('postcode',$this->postcode,true);
		$criteria->compare('state',$this->state,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}