<?php
$this->breadcrumbs=array(
	'List Procedure'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-weixin',
	'title'=>'List Procedure',
	'subtitle'=>'Add List Procedure',
);

$this->menu=array(
	array('label'=>'List Procedure', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>