<?php
$this->breadcrumbs=array(
	'Doctor',
);

$this->pageHeader=array(
	'icon'=>'fa fa-group',
	'title'=>'Doctor',
	'subtitle'=>'Doctor',
);

$this->menu=array(
	array('label'=>'Back', 'icon'=>'arrow-left','url'=>array('/admin/customer/index')),
	array('label'=>'Clinic', 'icon'=>'glass','url'=>array('index', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'Specialisation', 'icon'=>'heart','url'=>array('specialisation', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'About Us', 'icon'=>'star','url'=>array('about', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'Update Photo', 'icon'=>'camera','url'=>array('photo', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'Update Video', 'icon'=>'film','url'=>array('video', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>

<div class="row-fluid">
	<div class="span8">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'cs-customer-form',
    // 'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); ?>
		<div class="widget">
		<h4 class="widgettitle">Profile</h4>
		<div class="widgetcontent">

			<?php echo $form->textFieldRow($model,'name',array('class'=>'span12', 'placeholder'=>'Doctor`s Name')); ?>
			<?php echo $form->textFieldRow($model,'certification',array('class'=>'span12', 'placeholder'=>'Certification')); ?>

			<div class="row-fluid">
				<div class="span6">
					<?php echo $form->fileFieldRow($model,'photo',array(
					'hint'=>'<b>Note:</b> Image size is 127 x 127px. Larger image will be automatically cropped.', 'style'=>"width: 100%")); ?>
					<?php if ($model->scenario == 'update'): ?>
					<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(127,127, '/images/doctor/'.$model->photo , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
					<?php endif; ?>
				</div>
				<div class="span6">
					<?php echo $form->fileFieldRow($model,'cover',array(
					'hint'=>'<b>Note:</b> Image size is 1600 x 530px. Larger image will be automatically cropped.', 'style'=>"width: 100%")); ?>
					<?php if ($model->scenario == 'update'): ?>
					<img style="width: 100%;" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1600,530, '/images/doctor/'.$model->cover , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
					<?php endif; ?>
				</div>
			</div>

			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Simpan dan Tambahkan' : 'Simpan',
				'htmlOptions'=>array('class'=>'btn-large'),
			)); ?>

		</div>
		</div>

		<div class="alert">
		  <button type="button" class="close" data-dismiss="alert">×</button>
		  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
		</div>
<?php $this->endWidget(); ?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'cs-customer-form',
    // 'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); ?>
		<div class="widget">
		<h4 class="widgettitle">Clinic Contact Information</h4>
		<div class="widgetcontent">
<?php
$criteria = new CDbCriteria;
$criteria->addCondition('doctor_id = :doctor_id');
$criteria->params[':doctor_id'] = $model->id;
$dataClinic = new CActiveDataProvider('DoctorClinic', array(
	'criteria'=>$criteria,
));
?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'promotion-grid',
	'dataProvider'=>$dataClinic,
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		'name',
		'phone',
		'website',
		'suburb',
		// array(
		// 	'name'=>'last_update_by',
		// ),
		// array(
		// 	'name'=>'active',
		// 	'filter'=>array(
		// 		'0'=>'Non Active',
		// 		'1'=>'Active',
		// 	),
		// 	'type'=>'raw',
		// 	'value'=>'($data->active == "1") ? "Show" : "Hide"',
		// ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} {delete}',
			'deleteButtonUrl'=>'CHtml::normalizeUrl(array("index", "member_id"=>"'.$member_id.'", "doctor_id"=>"'.(($_GET['doctor_id'] == '') ? 0 : $_GET['doctor_id']).'", "delete"=>$data->id))',
			'updateButtonUrl'=>'CHtml::normalizeUrl(array("index", "member_id"=>"'.$member_id.'", "doctor_id"=>"'.(($_GET['doctor_id'] == '') ? 0 : $_GET['doctor_id']).'", "update"=>$data->id))',
		),
	),
)); ?>
			<?php echo $form->errorSummary($modelClinic); ?>
			<div class="row-fluid">
				<div class="span6">
					<?php echo $form->textFieldRow($modelClinic,'name',array('class'=>'span12', 'placeholder'=>'Clinic`s Name')); ?>
					<?php echo $form->textFieldRow($modelClinic,'phone',array('class'=>'span12', 'placeholder'=>'Phone')); ?>
				</div>
				<div class="span6">
					<?php echo $form->textFieldRow($modelClinic,'website',array('class'=>'span12', 'placeholder'=>'Clinic`s Name')); ?>
					<?php echo $form->textFieldRow($modelClinic,'fax',array('class'=>'span12', 'placeholder'=>'Phone')); ?>
				</div>
			</div>

			<?php echo $form->textFieldRow($modelClinic,'address_1',array('class'=>'span12', 'placeholder'=>'Address line 1')); ?>
			<?php echo $form->textFieldRow($modelClinic,'address_2',array('class'=>'span12', 'placeholder'=>'Address line 2')); ?>

			<div class="row-fluid">
				<div class="span4">
					<?php echo $form->textFieldRow($modelClinic,'suburb',array('class'=>'span12', 'placeholder'=>'Suburb')); ?>
				</div>
				<div class="span4">
					<?php echo $form->textFieldRow($modelClinic,'postcode',array('class'=>'span12', 'placeholder'=>'Postcode')); ?>
				</div>
				<div class="span4">
					<?php echo $form->textFieldRow($modelClinic,'state',array('class'=>'span12', 'placeholder'=>'State')); ?>
				</div>
			</div>
			
					<?php echo $form->hiddenField($modelClinic,'latitude',array('class'=>'span12', 'placeholder'=>'Latitude')); ?>
					<?php echo $form->hiddenField($modelClinic,'longitude',array('class'=>'span12', 'placeholder'=>'Longitude')); ?>

<!-- <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js"></script> -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnVYV9PU2hDS4GMEJ_TZ2Hy-zy1iXfQX0&callback=initialize"></script>
<div id="map_canvas" style="width:100%; height:400px;"></div>
<div class="divider10"></div>
<div class="divider10"></div>
<script type="text/javascript">
var map;
function initialize() {
  <?php if ($modelClinic->scenario == 'update'): ?>
  var myLatlng = new google.maps.LatLng(<?php echo $modelClinic->latitude ?>,<?php echo $modelClinic->longitude ?>);
  <?php else: ?>
  var myLatlng = new google.maps.LatLng(-24.90636703004104,133.63494856249997);
  <?php endif ?>


  var myOptions = {
     zoom: 4,
     center: myLatlng,
     mapTypeId: google.maps.MapTypeId.ROADMAP
     }
  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions); 

  var marker = new google.maps.Marker({
    draggable: true,
    position: myLatlng, 
    map: map,
  title: "Your location"
  });

  google.maps.event.addListener(marker, 'dragend', function (event) {
      document.getElementById("DoctorClinic_latitude").value = this.getPosition().lat();
      document.getElementById("DoctorClinic_longitude").value = this.getPosition().lng();
  });

  // if (navigator.geolocation) {
  //   navigator.geolocation.getCurrentPosition(function(position) {
  //     var pos = {
  //       lat: position.coords.latitude,
  //       lng: position.coords.longitude
  //     };

  //     map.setCenter(pos);
  //     marker.setMap(null);

  //     // map = new google.maps.Map(document.getElementById("map_canvas"), myOptions); 

  //     var marker = new google.maps.Marker({
  //       draggable: true,
  //       position: pos, 
  //       map: map,
  //       title: "Your location"
  //     });

  //     marker.setMap(map);

  //     google.maps.event.addListener(marker, 'dragend', function (event) {
  //         // document.getElementById("DoctorClinic_latitude").value = this.getPosition().lat();
  //         // document.getElementById("DoctorClinic_longitude").value = this.getPosition().lng();
  //     });
      

  //   }, function() {
  //     handleLocationError(true, infoWindow, map.getCenter());
  //   });
  // } else {
  //   // Browser doesn't support Geolocation
  //   handleLocationError(false, infoWindow, map.getCenter());
  // }

}

$(document).load(function() {
  initialize()
})
</script> 

			<?php if ($modelClinic->scenario == 'update'): ?>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				// 'buttonType'=>'submit',
				// 'type'=>'info',
				'url'=>CHtml::normalizeUrl(array('index', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
				'label'=>'Back',
				'htmlOptions'=>array('class'=>'btn-large'),
			)); ?>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>'Update Clinic',
				'htmlOptions'=>array('class'=>'btn-large'),
			)); ?>
			<?php else: ?>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>'Add Clinic',
				'htmlOptions'=>array('class'=>'btn-large'),
			)); ?>
			<?php endif ?>

		</div>
		</div>

<?php $this->endWidget(); ?>


<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'cs-customer-form',
    // 'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); ?>
		<div class="widget">
		<h4 class="widgettitle">Social Links</h4>
		<div class="widgetcontent">

			<?php echo $form->textFieldRow($model,'social_facebook',array('class'=>'span12', 'placeholder'=>'Facebook')); ?>
			<?php echo $form->textFieldRow($model,'social_twitter',array('class'=>'span12', 'placeholder'=>'Twitter')); ?>
			<?php echo $form->textFieldRow($model,'social_instagram',array('class'=>'span12', 'placeholder'=>'Instagram')); ?>

			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Simpan dan Tambahkan' : 'Simpan',
				'htmlOptions'=>array('class'=>'btn-large'),
			)); ?>

		</div>
		</div>

		<div class="alert">
		  <button type="button" class="close" data-dismiss="alert">×</button>
		  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
		</div>
<?php $this->endWidget(); ?>
	</div>
	<div class="span4">
		<div class="widgetbox block-rightcontent">                        
		</div>
	</div>
</div>


