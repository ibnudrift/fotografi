<?php
$this->breadcrumbs=array(
	' Member/Doctor'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-group',
	'title'=>'Member/Doctor',
	'subtitle'=>'Add Member/Doctor',
);

$this->menu=array(
	array('label'=>'List Member/Doctor', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>