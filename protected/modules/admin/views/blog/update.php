<?php
$this->breadcrumbs=array(
	'Blog'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-comments-o',
	'title'=>'Blog',
	'subtitle'=>'Data Blog',
);

$this->menu=array(
	array('label'=>'List Blog', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Blog', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Blog', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<div class="row-fluid">
	<div class="span8">
		<h1>Edit Blog</h1>
		<?php echo $this->renderPartial('_form',array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>
	</div>
	<!-- <div class="span4">
		<?php // $this->renderPartial('/pages/page_menu') ?>
	</div> -->
</div>
