<?php
$this->breadcrumbs=array(
	'Blog',
);

$this->pageHeader=array(
	'icon'=>'fa fa-comments-o',
	'title'=>'Blog',
	'subtitle'=>'Blog Data',
);

$this->menu=array(
	array('label'=>'Add Blog', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<div class="row-fluid">
	<div class="span9">
<h1>Blog</h1>
		<?php $this->widget('bootstrap.widgets.TbGridView',array(
			'id'=>'promotion-grid',
			'dataProvider'=>$model->search($this->languageID),
			// 'filter'=>$model,
			'enableSorting'=>false,
			'summaryText'=>false,
			'type'=>'bordered',
			'columns'=>array(
				array(
		            'name'=>'title',
		        ),    
				array(
		            'name'=>'date_input',
		        ),    
				array(
		            'name'=>'date_input',
		        ),    
				// array(
		  //           'name'=>'writer_name',
		  //       ),    
				// array(
				// 	'name'=>'date_input',
				// 	'filter'=>false,
				// ),
				// array(
				// 	'name'=>'date_update',
				// 	'filter'=>false,
				// ),
				// array(
				// 	'name'=>'insert_by',
				// ),
				// array(
				// 	'name'=>'last_update_by',
				// ),
				// array(
				// 	'name'=>'active',
				// 	'filter'=>array(
				// 		'0'=>'Non Active',
				// 		'1'=>'Active',
				// 	),
				// 	'type'=>'raw',
				// 	'value'=>'($data->active == "1") ? "Show" : "Hide"',
				// ),
				array(
					'class'=>'bootstrap.widgets.TbButtonColumn',
					'template'=>'{update} {delete}'
				),
			),
		)); ?>
	</div>
	<!-- <div class="span4">
		<?php // $this->renderPartial('/pages/page_menu') ?>
	</div> -->
</div>
		