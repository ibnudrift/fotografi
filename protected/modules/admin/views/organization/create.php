<?php
$this->breadcrumbs=array(
	'Organization'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-weixin',
	'title'=>'Organization',
	'subtitle'=>'Add Organization',
);

$this->menu=array(
	array('label'=>'List Organization', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>