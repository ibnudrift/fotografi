<?php
$this->breadcrumbs=array(
	'Organization'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-weixin',
	'title'=>'Organization',
	'subtitle'=>'Edit Organization',
);

$this->menu=array(
	array('label'=>'List Organization', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Organization', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Organization', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>