<div class="widgetbox block-rightcontent">                        
    <div class="headtitle">
        <h4 class="widgettitle">Setting List</h4>
    </div>
    <div class="widgetcontent">
        <ul class="userlist">
                <li> Who is Trusted Surgeons <a href="<?php echo CHtml::normalizeUrl(array('/admin/about/index')); ?>" class="editright"> <span class="fa fa-pencil"></span> &nbsp; Edit</a></li>
                <li> Our Mission <a href="<?php echo CHtml::normalizeUrl(array('/admin/about/mission')); ?>" class="editright"> <span class="fa fa-pencil"></span> &nbsp; Edit</a></li>
                <li> Why Choose Trusted Surgeons <a href="<?php echo CHtml::normalizeUrl(array('/admin/about/why')); ?>" class="editright"> <span class="fa fa-pencil"></span> &nbsp; Edit</a></li>
                <li> About Me <a href="<?php echo CHtml::normalizeUrl(array('/admin/about/me')); ?>" class="editright"> <span class="fa fa-pencil"></span> &nbsp; Edit</a></li>
        </ul>
    </div><!--widgetcontent-->
</div>