<?php
$this->breadcrumbs=array(
	'Manage Featured Doctors',
);
$this->pageHeader=array(
	'icon'=>'fa fa-life-ring',
	'title'=>'Manage Featured Doctors',
	'subtitle'=>'Manage Featured Doctors',
);

$this->menu=array(
	// array('label'=>'Add Manage Featured Doctors', 'icon'=>'icon-plus-sign','url'=>array('create')),
);
?>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'input-product-form',
    // 'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<div class="row-fluid">
	<div class="span12">
<?php foreach ($dataType as $k => $v): ?>
<?php
$dataFeatured = DoctorFeatured::model()->findAll('type = :type', array(':type'=>$v->type));
?>		
		<div class="widgetbox block-rightcontent">                        
		    <div class="headtitle">
		        <h4 class="widgettitle"><?php echo $v->type ?></h4>
		    </div>
		    <div class="widgetcontent">
                <table class="table table-bordered responsive table-slim">
                	<thead>
                        <tr>
                            <th>Profile</th>
                            <th>Action</th>
                        </tr>
                	</thead>
                    <tbody>
                    	<?php foreach ($dataFeatured as $key => $value): ?>
                        <tr>
                            <td>
                            	<select type="text" name="DoctorFeatured[doctor_id][<?php echo $value->id ?>]" id="DoctorFeatured_doctor_id_<?php echo $value->id ?>" class="input-block-level">
                            	<option value="">Choose Featured Surgeons</option>
								<?php foreach ($model as $val): ?>
									<option value="<?php echo $val->id ?>"><?php echo $val->name ?> | <?php echo $val->certification ?></option>
								<?php endforeach ?>
                            	</select>
                            	<script type="text/javascript">
                            	jQuery('#DoctorFeatured_doctor_id_<?php echo $value->id ?>').val('<?php echo $value->doctor_id ?>');
                            	</script>
                            </td>
                            <td><button type="submit" class="btn btn-primary">Submit</button></td>
                        </tr>
                    	<?php endforeach ?>
                    </tbody>
                </table>

		    </div><!--widgetcontent-->
		</div>
		<div class="divider15"></div>
<?php endforeach ?>
	</div>
</div>
<?php $this->endWidget(); ?>