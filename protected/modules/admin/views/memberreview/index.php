<?php
$this->breadcrumbs=array(
	'Manage Reviews',
);
$this->pageHeader=array(
	'icon'=>'fa fa-life-ring',
	'title'=>'Manage Reviews',
	'subtitle'=>'Manage Reviews Data',
);

$this->menu=array(
	array('label'=>'Add Manage Reviews', 'icon'=>'icon-plus-sign','url'=>array('create')),
);
?>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<div class="row-fluid">
	<div class="span12">
		<div class="widgetbox block-rightcontent">                        
		    <div class="headtitle">
		        <h4 class="widgettitle">Manage Reviews Data</h4>
		    </div>
		    <div class="widgetcontent">
		    	
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	<div class="row-fluid">
		<div class="span2">
			<?php echo $form->textFieldRow($model,'search_name',array('class'=>'span12','maxlength'=>200, 'placeholder'=>'Search Doctor Name')); ?>
		</div>
		<div class="span2">
			<?php echo $form->textFieldRow($model,'search_date_from',array('class'=>'span12 datepick','maxlength'=>200, 'placeholder'=>'Search Date From')); ?>
		</div>
		<div class="span2">
			<?php echo $form->textFieldRow($model,'search_date_to',array('class'=>'span12 datepick','maxlength'=>200, 'placeholder'=>'Search Date To')); ?>
		</div>
		<div class="span2">
			<?php echo $form->dropDownListRow($model,'search_status',array(
				'1'=>'Managed',
				'2'=>'Unclaimed',
			),array('class'=>'span12','maxlength'=>200, 'empty'=>'All Status')); ?>
		</div>
		<div class="span2">
			<?php echo $form->dropDownListRow($model,'search_review_status',array(
				'1'=>'Pending',
				'2'=>'Approved',
				'3'=>'Featured',
			),array('class'=>'span12','maxlength'=>200, 'empty'=>'All Status')); ?>
		</div>
	</div>

	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>'Search',
	)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		// 'buttonType'=>'button',
		'type'=>'primary',
		'label'=>'Reset',
		'url'=>Yii::app()->createUrl($this->route),
	)); ?>

<?php $this->endWidget(); ?>

				<hr>
				<?php
				$data = $dataReview->getData();
				?>
				<?php $this->widget('CLinkPager', array(
				    'pages' => $dataReview->getPagination(),
				)) ?>
				<hr>
				<?php foreach ($data as $key => $value): ?>
				<div class="row-fluid">
					<div class="span12">
<?php
$criteria = new CDbCriteria;
$criteria->select = 'SUM(`value`)/COUNT(`value`) as `value`';
$criteria->addCondition('review_id = :review_id');
$criteria->params[':review_id'] = $value->id;
$criteria->group = 'review_id';
$dataReviewValue = DoctorCategoryReview::model()->find($criteria);

?>
						<h3 class="title-product"><?php echo DoctorReviewCategory::model()->createStar($dataReviewValue->value) ?>
                      &nbsp; Post By: <?php echo $value->name ?> <?php if ($value->address != ''): ?>, <?php echo $value->address ?><?php endif ?>
	                    <?php if ($value->status == 0): ?>
	                      (<span style="color: red">Pending</span>)
	                    <?php else: ?>
	                      <?php if ($value->featured == 1): ?>
	                        (<span style="color: green">Approved</span> | <span style="color: blue">Featured</span>)
	                      <?php else: ?>
	                        (<span style="color: green">Approved</span>)
	                      <?php endif ?>
	                    <?php endif ?>
                  		</h3>
	                    <h4>Profile Name: <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail', 'id'=>$value->doctor_id)); ?>"><?php echo $value->doctor->name ?></a>
							<?php if ($value->doctor->member_id == 0): ?>
								<span style="color: green">(Unclaimed)</span>
							<?php else: ?>
								<span style="color: red">(Managed)</span>
								
							<?php endif ?>
	                     | <?php echo date('d F Y H:i', strtotime($value->date_input)); ?></h4>
						<div class="row-fluid">
							<div class="span12">
								<p><?php echo strip_tags($value->comment) ?></p>
								<a data-id="<?php echo $value->status ?>" href="<?php echo CHtml::normalizeUrl(array('setStatus', 'id'=>$value->id, 'type'=>'status')); ?>" class="btn btn-inverse btn-hide-show"><i class="fa fa-eye"></i> Approved</a>
                            	<a data-id="<?php echo $value->featured ?>" href="<?php echo CHtml::normalizeUrl(array('setStatus', 'id'=>$value->id, 'type'=>'featured')); ?>" class="btn btn-small btn-featured"><i class="fa fa-star"></i> Featured</a>
                            	<a data-id="<?php echo $value->id ?>" href="<?php echo CHtml::normalizeUrl(array('delete', 'id'=>$value->id)); ?>" class="btn btn-small btn-primary btn-terbaru delete-product"><i class="fa fa-fa-trash-o"></i> Delete</a>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<?php endforeach ?>
				<?php $this->widget('CLinkPager', array(
				    'pages' => $dataReview->getPagination(),
				)) ?>

		    </div><!--widgetcontent-->
		</div>
	</div>
	<script type="text/javascript">
		jQuery(function ( $ ) {
			$('.btn-hide-show').setStatusAjax({
				content: '<i class="fa fa-eye"></i> Approved',
				contentOK: '<i class="fa fa-eye-slash"></i> Hidden',
				class: 'btn-ok',
				classOK: 'btn-inverse',
			})
			$('.btn-featured').setStatusAjax({
				contentOK: '<i class="fa fa-star"></i> Delete Featured',
				content: '<i class="fa fa-star-o"></i> Featured',
				class: 'btn-ok',
				classOK: 'btn-primary',
			})
			// $('.delete-product').deleteAjax({
			// })
		})
	</script>
	<?php /*
	<div class="span4">
		<?php $this->renderPartial('product_category', array(
			'categoryModel'=>$categoryModel,
			'categoryModelDesc'=>$categoryModelDesc,
			'nestedCategory'=>$nestedCategory,
		)) ?>
	</div>
	*/ ?>
</div>
