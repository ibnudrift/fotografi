<div class="widgetbox block-rightcontent">                        
    <div class="headtitle">
        <h4 class="widgettitle">Start Journey</h4>
    </div>
    <div class="widgetcontent">
        <ul class="userlist">
                <li> Step 1 <a href="<?php echo CHtml::normalizeUrl(array('/admin/journey/index')); ?>" class="editright"> <span class="fa fa-pencil"></span> &nbsp; Edit</a></li>
                <!-- <li> Step 2 <a href="<?php echo CHtml::normalizeUrl(array('/admin/journey/step2')); ?>" class="editright"> <span class="fa fa-pencil"></span> &nbsp; Edit</a></li> -->
                <li> Step 2 <a href="<?php echo CHtml::normalizeUrl(array('/admin/journey/step3')); ?>" class="editright"> <span class="fa fa-pencil"></span> &nbsp; Edit</a></li>
                <li> Step 3 <a href="<?php echo CHtml::normalizeUrl(array('/admin/journey/step4')); ?>" class="editright"> <span class="fa fa-pencil"></span> &nbsp; Edit</a></li>
                <li> Step 4 <a href="<?php echo CHtml::normalizeUrl(array('/admin/journey/step5')); ?>" class="editright"> <span class="fa fa-pencil"></span> &nbsp; Edit</a></li>
                <!-- <li> Step 6 <a href="<?php echo CHtml::normalizeUrl(array('/admin/journey/step6')); ?>" class="editright"> <span class="fa fa-pencil"></span> &nbsp; Edit</a></li> -->
                <li> Step 5 <a href="<?php echo CHtml::normalizeUrl(array('/admin/journey/step7')); ?>" class="editright"> <span class="fa fa-pencil"></span> &nbsp; Edit</a></li>
                <!-- <li> Step 8 <a href="<?php echo CHtml::normalizeUrl(array('/admin/journey/step8')); ?>" class="editright"> <span class="fa fa-pencil"></span> &nbsp; Edit</a></li> -->
                <!-- <li> Step 9 <a href="<?php echo CHtml::normalizeUrl(array('/admin/journey/step9')); ?>" class="editright"> <span class="fa fa-pencil"></span> &nbsp; Edit</a></li> -->
                <li> Step 6 <a href="<?php echo CHtml::normalizeUrl(array('/admin/journey/step10')); ?>" class="editright"> <span class="fa fa-pencil"></span> &nbsp; Edit</a></li>
        </ul>
    </div><!--widgetcontent-->
</div>