<?php
$this->breadcrumbs=array(
	'Projects'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-camera',
	'title'=>'Projects',
	'subtitle'=>'Projects',
);

$this->menu=array(
	array('label'=>'List Projects', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelDesc'=>$modelDesc, 'modelCategory'=>$modelCategory, 'modelImage'=>$modelImage)); ?>
