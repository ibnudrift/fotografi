<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'promo-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<style type="text/css">
.ui-datepicker-month, .ui-datepicker-year{
	width: auto;
}
</style>
<?php echo $form->errorSummary($model); ?>

<div class="widget">
<h4 class="widgettitle">Data Promo</h4>
<div class="widgetcontent">

	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'kode',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->dropDownListRow($model, 'type_potongan', array(
		'1'=>'Percent',
		'0'=>'Nominal',
	)); ?>

	<?php echo $form->textFieldRow($model,'potongan',array('class'=>'span5','maxlength'=>100)); ?>
	
	<?php echo $form->textFieldRow($model,'aktif_sampai',array('class'=>'span5 datepicker','maxlength'=>100)); ?>
	
	<?php echo $form->dropDownListRow($model, 'aktif', array(
		'1'=>'Active',
		'0'=>'Non Active',
	)); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>$model->isNewRecord ? 'Create' : 'Save',
	)); ?>

</div>
</div>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
jQuery('.datepicker').datepicker({
	'showAnim':'fold',
	'dateFormat': 'yy-mm-dd',
	'changeMonth': true,
	'changeYear': true,
	// 'showOn':'button',
	// 'buttonImage':'/surabaya/asset/images/icon-calender.png',
	// 'buttonImageOnly':true
});
</script>
