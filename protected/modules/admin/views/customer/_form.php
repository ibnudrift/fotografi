<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'cs-customer-form',
    // 'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="row-fluid">
	<div class="span8">
		<div class="widget">
		<h4 class="widgettitle">Data Patients/Doctors</h4>
		<div class="widgetcontent">
			<div class="row-fluid">
				<div class="span4">
					<?php echo $form->textFieldRow($model,'email',array('class'=>'span12')); ?>
					<?php if ($model->scenario == 'update' OR $model->scenario == 'changePass'): ?>
						<?php echo $form->passwordFieldRow($model,'pass',array('class'=>'span12')); ?>
						<?php echo $form->passwordFieldRow($model,'pass2',array('class'=>'span12')); ?>
					<?php endif ?>
				</div>
				<div class="span4">
					<?php echo $form->textFieldRow($model,'first_name',array('class'=>'span12')); ?>
					<?php echo $form->textFieldRow($model,'last_name',array('class'=>'span12')); ?>
					<?php echo $form->textFieldRow($model,'phone',array('class'=>'span12')); ?>

				</div>
				<div class="span4">
		        	<?php echo $form->dropDownListRow($model, 'aktif', array(
		        		'1'=>'Active',
		        		'0'=>'Non Active',
		        	), array('class'=>'span12')); ?>

		        	<?php echo $form->dropDownListRow($model, 'type', array(
		        		'0'=>'Member',
		        		'1'=>'Doctor',
		        	), array('class'=>'span12')); ?>
					<?php // echo $form->textFieldRow($model,'city',array('class'=>'span12')); ?>
					<?php // echo $form->textFieldRow($model,'province',array('class'=>'span12')); ?>
					<?php // echo $form->textFieldRow($model,'postcode',array('class'=>'span12')); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6">
					<h3>Billing Address</h3>
					<?php echo $form->textFieldRow($model,'billing_address_line_1',array('class'=>'span12')); ?>
					<?php echo $form->textFieldRow($model,'billing_address_line_2',array('class'=>'span12')); ?>
					<?php echo $form->textFieldRow($model,'billing_suburb',array('class'=>'span12')); ?>
					<?php echo $form->textFieldRow($model,'billing_state',array('class'=>'span12')); ?>
					<?php echo $form->textFieldRow($model,'billing_postcode',array('class'=>'span12')); ?>
				</div>
				<div class="span6">
					<h3>Delivery Address</h3>
					<?php echo $form->textFieldRow($model,'delivery_address_line_1',array('class'=>'span12')); ?>
					<?php echo $form->textFieldRow($model,'delivery_address_line_2',array('class'=>'span12')); ?>
					<?php echo $form->textFieldRow($model,'delivery_suburb',array('class'=>'span12')); ?>
					<?php echo $form->textFieldRow($model,'delivery_state',array('class'=>'span12')); ?>
					<?php echo $form->textFieldRow($model,'delivery_postcode',array('class'=>'span12')); ?>
				</div>
			</div>


		</div>
		</div>

		<div class="alert">
		  <button type="button" class="close" data-dismiss="alert">×</button>
		  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
		</div>
		
	</div>
	<div class="span4">
		<div class="widgetbox block-rightcontent">                        
		    <div class="headtitle">
		        <h4 class="widgettitle">Action</h4>
		    </div>
		    <div class="widgetcontent">
				<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'submit',
					'type'=>'primary',
					'label'=>$model->isNewRecord ? 'Save and Add' : 'Save',
					'htmlOptions'=>array('class'=>'btn-large'),
				)); ?>
				<?php $this->widget('bootstrap.widgets.TbButton', array(
					// 'buttonType'=>'submit',
					// 'type'=>'info',
					'url'=>CHtml::normalizeUrl(array('index')),
					'label'=>'Cancel',
					'htmlOptions'=>array('class'=>'btn-large'),
				)); ?>
		    </div>
		</div>
	</div>
</div>


<?php $this->endWidget(); ?>
