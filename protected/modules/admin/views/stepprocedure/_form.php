<div class="row-fluid">
	<div class="span8">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'faq-form',
    // 'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="widget">
<h4 class="widgettitle">Data List Procedure</h4>
<div class="widgetcontent">

		<?php Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget'); ?>
		<?php $this->widget('ImperaviRedactorWidget', array(
		    'selector' => '.redactor',
		    'options' => array(
		        'imageUpload'=> $this->createUrl('/admin/setting/imgUpload', array('type'=>'image')),
		        'clipboardUploadUrl'=> $this->createUrl('/admin/setting/imgUpload', array('type'=>'clip')),
		    ),
		    'plugins' => array(
		        'clips' => array(
		        ),
		    ),
		)); ?>

		<div class="row-fluid">
			<div class="span4">
				<?php echo $form->fileFieldRow($model,'image',array(
				'hint'=>'<b>Note:</b> Image size is 270 x 270px. Larger image will be automatically cropped.', 'style'=>"width: 100%")); ?>
				<?php if ($model->scenario == 'update'): ?>
				<img style="width: 100%;" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(270,270, '/images/stepprocedure/'.$model->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
				<?php endif; ?>
			</div>
			<div class="span8">
				<?php echo $form->textAreaRow($model,'title',array('class'=>'span12')); ?>

				<?php // echo $form->textAreaRow($model,'content',array('rows'=>6, 'cols'=>50, 'class'=>'span5 redactor')); ?>
				<!-- <div class="divider10"></div> -->
				<div class="row-fluid">
					<div class="span6">
						<?php echo $form->dropDownListRow($model,'category_id',array(
							'1'=>'Face',
							'2'=>'Breast',
							'3'=>'Body',
						),array('class'=>'span12')); ?>
					</div>
					<div class="span6">
						<?php echo  $form->textFieldRow($model,'url',array('class'=>'span12')); ?>
					</div>
				</div>

				<?php // echo $form->textFieldRow($model,'url_text',array('class'=>'span12')); ?>
			</div>
		</div>

		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Add' : 'Save',
		)); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			// 'buttonType'=>'submit',
			// 'type'=>'info',
			'url'=>CHtml::normalizeUrl(array('index')),
			'label'=>'Cancel',
		)); ?>
</div>
</div>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>
<script type="text/javascript">
if (typeof RedactorPlugins === 'undefined') var RedactorPlugins = {};

RedactorPlugins.advanced = {
    init: function()
    {
        alert(1);
    }
}
jQuery(function( $ ) {
	$('.multilang').multiLang({
	});
})

</script>
<?php $this->endWidget(); ?>
</div>
	<div class="span4">
		<?php $this->renderPartial('/journey/_page_menu') ?>
	</div>
</div>