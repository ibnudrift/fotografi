<?php

class MemberclaimController extends ControllerAdmin
{
	public $layout = '//layoutsAdmin/column2';

	public function actionIndex()
	{
		// https://www.youtube.com/watch?v=mC2q2SGy2e0
		// $data = file_get_contents("http://youtube.com/get_video_info?video_id=".'mC2q2SGy2e0');
		// parse_str($data, $output);
		$model = new DoctorClaim;

		$criteria = new CDbCriteria;
		$criteria->with = array(
			'doctor',
			'member',
		);
		

		$criteria->order = 't.date_input DESC';

		if ($_GET['DoctorClaim']) {
			$model->attributes = $_GET['DoctorClaim'];
			if ($model->search_name != '') {
				$criteria->addCondition('doctor.name LIKE :name OR member.first_name LIKE :name OR member.last_name LIKE :name');
				$criteria->params[':name'] = '%'.$model->search_name.'%';
			}
			if ($model->search_date_from != '' AND $model->search_date_to == '') {
				$criteria->addCondition('t.date_input > :date_from');
				$criteria->params[':date_from'] = date('Y-m-d H:i:s',strtotime($model->search_date_from.' 00:00:01'));
			}
			if ($model->search_date_to != '' AND $model->search_date_from == '') {
				$criteria->addCondition('t.date_input < :date_to');
				$criteria->params[':date_to'] = date('Y-m-d H:i:s',strtotime($model->search_date_to.' 23:59:59'));
			}
			if ($model->search_date_from != '' AND $model->search_date_to != '') {
				$criteria->addCondition('t.date_input > :date_from AND t.date_input < :date_to');
				$criteria->params[':date_from'] = date('Y-m-d H:i:s',strtotime($model->search_date_from.' 00:00:01'));
				$criteria->params[':date_to'] = date('Y-m-d H:i:s',strtotime($model->search_date_to.' 23:59:59'));
			}
			if ($model->status != '') {
				$criteria->compare('status',$model->status);
			}
		}

		$dataReview = new CActiveDataProvider('DoctorClaim', array(
		  'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>10,
		    ),
		));

		$this->render('index', array(
			'model'=>$model,
			'dataReview'=>$dataReview,
		));	
	}

	public function actionSetStatus($id, $type)
	{
		// we only allow deletion via POST request
		$model = DoctorClaim::model()->findByPk($id);

		$modelDoctor = Doctor::model()->findByPk($model->doctor_id);
		$modelMember = MeMember::model()->findByPk($model->member_id);

		$modelDoctor2 = Doctor::model()->find('member_id = :member_id', array(':member_id'=>$model->member_id));
		$modelDoctor2->status = 0;
		$modelDoctor2->save();

		$modelDoctor->member_id = $modelMember->id;
		$modelDoctor->save(false);

		$modelMember->type = 1;
		$modelMember->save(false);

		$model->{$type} = ($model->{$type}-1)*-1;
		$model->save();
		Yii::app()->user->setFlash('success',$modelDoctor->name.' have claimed by '.$modelMember->first_name);
		$this->redirect(array('index'));
	}

}