<?php

class MemberclinicController extends ControllerAdmin
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layoutsAdmin/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
			array('admin.filter.AuthFilter'),
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			(!Yii::app()->user->isGuest)?
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete','index','view','create','update'),
				'users'=>array(Yii::app()->user->name),
			):array(),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex($member_id = 0)
	{
		if ($member_id > 0) {
			$model = Doctor::model()->find('member_id = :member_id', array(':member_id'=>$member_id));
			if ($model == null) {
				$model = new Doctor;
				$model->member_id = $member_id;
				$model->photo = 'exam_pict_doctor3.jpg';
				$model->cover = 'picts_detail_psurgeons.jpg';
				$model->save(false);
			}
		}else{
			$model = Doctor::model()->findByPk($_GET['doctor_id']);
		}
		
		if ($_GET['update']) {
		$modelClinic = DoctorClinic::model()->findByPk($_GET['update']);
		}else{
		$modelClinic = new DoctorClinic;
		}

		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		if ($_POST['Doctor']) {
			$photo = $model->photo;//mengamankan nama file
			$cover = $model->cover;//mengamankan nama file
			$model->attributes = $_POST['Doctor'];
			$model->photo = $photo;//mengembalikan nama file
			$model->cover = $cover;//mengembalikan nama file

			$photo = CUploadedFile::getInstance($model,'photo');
			if ($photo->name != '') {
				$model->photo = substr(md5(time()),0,5).'-'.$photo->name;
			}
			$cover = CUploadedFile::getInstance($model,'cover');
			if ($cover->name != '') {
				$model->cover = substr(md5(time()),0,5).'-'.$cover->name;
			}

			if ($photo->name != '') {
				$photo->saveAs(Yii::getPathOfAlias('webroot').'/images/doctor/'.$model->photo);
			}

			if ($cover->name != '') {
				$cover->saveAs(Yii::getPathOfAlias('webroot').'/images/doctor/'.$model->cover);
			}

			$model->save();
			$this->refresh();
		}

		if ($_POST['DoctorClinic']) {
			$modelClinic->attributes=$_POST['DoctorClinic'];
			if($modelClinic->validate()){
				$modelClinic->doctor_id = $model->id;
				$modelClinic->save();
				$this->redirect(array('index', 'member_id'=>$member_id, 'doctor_id'=>$model->id));
			}
		}

		$this->render('index', array(
			'model'=>$model,
			'member_id'=>$member_id,
			'modelClinic'=>$modelClinic,
		));	
	}

	public function actionSpecialisation($member_id = 0)
	{
		if ($member_id > 0) {
			$model = Doctor::model()->find('member_id = :member_id', array(':member_id'=>$member_id));
		}else{
			$model = Doctor::model()->findByPk($_GET['doctor_id']);
		}

		if ($_POST['Specialisation']) {
			DoctorSpecialication::model()->deleteAll('doctor_id = :id', array(':id'=>$model->id));
			foreach ($_POST['Specialisation'] as $key => $value) {
				if ($value == 1) {
					$modelSpesialis = new DoctorSpecialication;
					$modelSpesialis->doctor_id = $model->id;
					$modelSpesialis->specialitation_id = $key;
					$modelSpesialis->save();
				}
			}
			$this->refresh();
		}

		$this->render('specialisation', array(
			'model'=>$model,
			'member_id'=>$member_id,
			// 'model2'=>$model2,
		));	
	}

	public function actionAbout($member_id = 0)
	{
		if ($member_id > 0) {
			$model = Doctor::model()->find('member_id = :member_id', array(':member_id'=>$member_id));
		}else{
			$model = Doctor::model()->findByPk($_GET['doctor_id']);
		}

		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		if ($_POST['Doctor']) {
			$model->attributes = $_POST['Doctor'];
			$model->save();
			$this->refresh();
		}

		$this->render('about', array(
			'model'=>$model,
			'member_id'=>$member_id,
			// 'model2'=>$model2,
		));	
	}

	public function actionPhoto($member_id = 0)
	{
		if ($member_id > 0) {
			$model = Doctor::model()->find('member_id = :member_id', array(':member_id'=>$member_id));
		}else{
			$model = Doctor::model()->findByPk($_GET['doctor_id']);
		}

		if ($_GET['update']) {
		$modelPhoto = DoctorPhoto::model()->findByPk($_GET['update']);
		}else{
		$modelPhoto = new DoctorPhoto;
		}

		if ($_POST['DoctorPhoto']) {
			$modelPhoto->attributes=$_POST['DoctorPhoto'];

			$image = CUploadedFile::getInstance($modelPhoto,'image');
			if ($image->name != '') {
				$modelPhoto->image = substr(md5(time()),0,5).'-'.$image->name;
			}

			if($modelPhoto->validate()){
				if ($image->name != '') {
					$image->saveAs(Yii::getPathOfAlias('webroot').'/images/doctor_photo/'.$modelPhoto->image);
				}
				$modelPhoto->doctor_id = $model->id;
				$modelPhoto->save();
				$this->redirect(array('photo', 'member_id'=>$member_id, 'doctor_id'=>$model->id));
			}
		}

		$this->render('photo', array(
			'model'=>$model,
			'member_id'=>$member_id,
			'modelPhoto'=>$modelPhoto,
			// 'model2'=>$model2,
		));	
	}
	public function actionVideo($member_id = 0)
	{
		if ($member_id > 0) {
			$model = Doctor::model()->find('member_id = :member_id', array(':member_id'=>$member_id));
		}else{
			$model = Doctor::model()->findByPk($_GET['doctor_id']);
		}

		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		$modelVideo = array();
		$modelVideo = DoctorVideo::model()->findAll('doctor_id = :id ORDER BY id', array(':id'=>$model->id));

		if ($_POST['DoctorVideo']) {
			DoctorVideo::model()->deleteAll('doctor_id = :id', array(':id'=>$model->id));
			if (count($_POST['DoctorVideo']['url']) > 0) {
				foreach ($_POST['DoctorVideo']['url'] as $key => $value) {
					$modelVideo[$key] = new DoctorVideo;
					if ($value != '') {
						$modelVideo[$key]->id_str = $_POST['DoctorVideo']['id_str'][$key];
						$modelVideo[$key]->doctor_id = $model->id;
						$modelVideo[$key]->url = $value;
						parse_str( parse_url( $value, PHP_URL_QUERY ), $dataUrl );
						$modelVideo[$key]->code = $dataUrl['v'];
						$dataVideo = @file_get_contents("http://youtube.com/get_video_info?video_id=".$dataUrl['v']);
						parse_str($dataVideo, $dataYoutube);
						$modelVideo[$key]->title = $dataYoutube['title'];
						$modelVideo[$key]->save(false);
						$modelVideo[$key]->id_str = $modelVideo[$key]->id;
						$modelVideo[$key]->save(false);
					}
					
				}
			}
			$this->refresh();
		}
		$this->render('video', array(
			'model'=>$model,
			'member_id'=>$member_id,
			'modelVideo'=>$modelVideo,
		));	
	}
}
